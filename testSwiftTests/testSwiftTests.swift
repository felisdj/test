//
//  testSwiftTests.swift
//  testSwiftTests
//
//  Created by JJsobtid on 4/10/2560 BE.
//  Copyright © 2560 JJsobtid. All rights reserved.
//

import XCTest
import FirebaseCore

@testable import testSwift

class testSwiftTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
		let testVM = TestViewModel(withModel: TestModel(p: "test", v: 0))
		XCTAssert(testVM.getModel(index: 0).value.value == 0, "TestModelValue")
		testVM.clickOn(index: 0)
		XCTAssert(testVM.getModel(index: 0).value.value == 1, "TestModelValue")
    }
	
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
