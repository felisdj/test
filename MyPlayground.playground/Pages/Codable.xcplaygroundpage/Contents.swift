import UIKit

print("test")

let dateData = "2019-11-21T00:00:00"
let today = Date()

let dateFormatter = DateFormatter()
dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
//dateFormatter.locale = Locale(identifier: "en_US") // set locale to reliable US_POSIX
let date = dateFormatter.date(from:dateData)!


let dateFormatterThai = DateFormatter()
dateFormatterThai.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"

print(date)
print(dateFormatterThai.string(from: today))

let calendar = Calendar.current
let component = calendar.dateComponents([.year, .month, .day], from: date)

print(component)

var comp = calendar.dateComponents([.year, .month, .day], from: Date())
let range = calendar.range(of: .day, in: .month, for: Date())!
let numDays = range.count

print(comp)

while(true) {
	if comp.year! >= 2020 {
		break
	}
	for i in comp.month!...12 {
		if i == 1 {
			break
		}
		let thisMonth = calendar.date(from: comp)!
		let range = calendar.range(of: .day, in: .month, for: thisMonth)!
		comp.month! += 1
		for j in comp.day!...range.count {
			let thisDate = calendar.date(from: comp)!
 			print("\(i) in \(comp.year!) :: \(j),, \(dateFormatter.string(from: thisDate))")
			comp.day! += 1
		}
		comp.day = 1
	}
	comp.year! += 1
	comp.month = 1
}

let now = Date()
let dateFormatter2 = DateFormatter()
dateFormatter2.dateFormat = "MMM\nEEE\nd"
dateFormatter2.locale = Locale(identifier: "th_TH")
let nameOfMonth = dateFormatter2.string(from: date)
print(nameOfMonth)

let startDateStr = "2019-11"
let endDateStr = "2021-11"

let rangeDateFormat = DateFormatter()
rangeDateFormat.dateFormat = "yyyy-MM"
rangeDateFormat.locale = Locale(identifier: "th_TH")

let start = rangeDateFormat.date(from: startDateStr)!
let end = rangeDateFormat.date(from: endDateStr)!

let diffDay = calendar.dateComponents([.day], from: start, to: end)
let diffMonth = calendar.dateComponents([.month], from: start, to: end)
print(diffDay.day, diffMonth.month)
