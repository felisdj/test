import UIKit
import Hero
import PlaygroundSupport
import RxSwift
import RxCocoa
import Alamofire
import SnapKit
import Foundation

var str = "Hello, playground"

var input = Observable<String>.from(["Hello", "mid", "World"])

input.map({ (str) -> String in
	return "Hello, \(str)"
}).subscribe { (event) in
	switch event {
	case .next:
		print(event.element ?? "")
	case .completed:
		print("completed")
	default:
		print("err")
	}
}

struct Person {
	let name: String
	let address: String
	let age: Int
	let income: Double
	let cars: [String]
}

let peopleArray = [ Person(name:"Santosh", address: "Pune, India", age:34, income: 100000.0, cars:["i20","Swift VXI"]),
					Person(name: "John", address:"New York, US", age: 23, income: 150000.0, cars:["Crita", "Swift VXI"]),
					Person(name:"Amit", address:"Nagpure, India", age:17, income: 200000.0, cars:Array())]

let cars = peopleArray.flatMap({ $0.cars })
print(cars)

let allCarText = cars.reduce("") { (result, nowCar) -> String in
	return result + ", " + nowCar
}
print(allCarText)
let allIncome = peopleArray.map { (person) -> Double in
	return person.income
}

print(allIncome.reduce(0, +))

let flatmapArray2 = peopleArray.map({$0.cars}).reduce([], +)
print("flatmapArray2 : \(flatmapArray2)")

var friends = ["Deapak", "Alex", "ravi", "Deapak"]

Dictionary(zip(friends, repeatElement(1, count: friends.count)), uniquingKeysWith: +)

let people = ["Tom": 24, "Alex": 23, "Rex": 21, "Ravi": 43]
let middleAgePeople = Dictionary(grouping: people.keys) {
	$0.count
}
print(middleAgePeople)
people["Test", default: 30]

var countFriend = [String: Int]()
for friend in friends {
	countFriend[friend, default: 0] += 1
}
countFriend

var tupleWithDupKeys = [("oné", 1), ("one", 2), ("two", 2), ("three", 3), ("five", 5)]

let dictionaryWithTuple = Dictionary(tupleWithDupKeys) { (first, second) in
	first
}
dictionaryWithTuple

friends.capacity
friends.reserveCapacity(20)
friends.capacity
let firstrv = friends.first!.reversed()
print(String(firstrv))

struct Book {
	var title = ""
	let price : Float
}
let titleKeyPath = \Book.title
let mathsBook = Book(title: "Algebra", price: 10.50)
mathsBook[keyPath: titleKeyPath]

var a = 0
var b = 1
(b, a) = (a, b)
a
b

func executeProcedure(for description: String, procedure: () -> Void) {
	print("Procedure executed for:", description)
	procedure()
}

enum Event<Element> {
	case next(Element)
	case error(Swift.Error)
	case complete
}

executeProcedure(for: "of") {
	let disposeBag = DisposeBag()
	let observable = Observable.of(10, 20, 30)
	observable.subscribe(onNext: { (no) in
		print(no)
	}, onCompleted: {
		print("complete")
	}, onDisposed: {
		print("dispose")
	}).disposed(by: disposeBag)
}

let pubSubject = PublishSubject<String>()
pubSubject.subscribe(onNext: { (str) in
	print(str)
}, onCompleted: {
	print("Complete")
}) {
	print("dispose")
}

pubSubject.on(.next("First Event"))
pubSubject.onNext("Second Event")

print("\n\n")

executeProcedure(for: "ReplaySubject") {
	let disposeBag = DisposeBag()
	let repSubject = ReplaySubject<String>.create(bufferSize: 3)
	repSubject.onNext("first")
	repSubject.onNext("second")
	let initialSubs = repSubject.subscribe(onNext: {
		print("Line number \(#line) and value is", $0)
	})
	initialSubs.disposed(by: disposeBag)
	repSubject.onNext("third")
	repSubject.onNext("final")
	repSubject.subscribe(onNext: {
		print("Hello", $0)
	}).disposed(by: disposeBag)
	repSubject.onNext("EIEI")
}

executeProcedure(for: "BehaviorSubject") {
	let disposeBag = DisposeBag()
	let behSubject = BehaviorSubject(value: "Test")
	let initialSubs = behSubject.subscribe(onNext: {
		print("Line number \(#line) and value is", $0)
	})
	initialSubs.disposed(by: disposeBag)
	behSubject.onNext("second")
	behSubject.subscribe(onNext: {
		print("Hello", $0)
	}).disposed(by: disposeBag)
}

executeProcedure(for: "PublishSubject") {
	enum CustomError: Error {
		case defaultError
	}
	let pubSubject = PublishSubject<String>()
	let disposeBag = DisposeBag()
	pubSubject.subscribe({
		print($0)
	}).disposed(by: disposeBag)
	pubSubject.on(.next("First"))
	pubSubject.onNext("Second")
	let newSub = pubSubject.subscribe(onNext: {
		print($0)
	})
	pubSubject.onNext("I am NEW")
	newSub.dispose()
	pubSubject.onNext("Fourth")
}

executeProcedure(for: "flatMap and flatMapLatest") {
	struct Player {
		let score: Variable<Int>
	}
	
	let disposeBag = DisposeBag()
	let alex = Player(score: Variable(10))
	let alot = Player(score: Variable(20))
	
	let currentPlayer = Variable(alex)
	let currentLatestPlayer = Variable(alex)
	currentPlayer.asObservable().flatMap({ $0.score.asObservable() }).subscribe(onNext: {
		print("flatMap :", $0)
	}).disposed(by: disposeBag)
	currentLatestPlayer.asObservable().flatMapLatest({ $0.score.asObservable() }).subscribe(onNext: {
		print("flatMapLatest :", $0)
	}).disposed(by: disposeBag)
	
	alex.score.value = 30
	
	currentPlayer.value = alot
	currentLatestPlayer.value = alot
	
	alot.score.value = 50
	alex.score.value = 40
}

let arrIntt: [Int] = [1, 2, 3, 23, 5]

let va = arrIntt.reduce(1) {
	print($0)
	return $0+$1
}
//var primes = Array<Bool>(repeating: true, count: 100001)
//
//for i in 1..<primes.count {
//	if i == 1 {
//		primes[i] = false
//	} else {
//		if primes[i] {
//			var noStart = i*2
//			while noStart < primes.count {
//				if primes[noStart] {
//					primes[noStart] = false
//				}
//				noStart += i
//			}
//		}
//	}
//}

public extension Int {
	func isPrime() -> Bool {
		guard self > 1 else { return false }
		
		for index in 2 ..< self {
			if self % index == 0 {
				return false
			}
		}
		
		return true
	}
}

executeProcedure(for: "filter") {
	
	let disposeBag = DisposeBag()
	
	let integers = Observable.generate(initialState: 1, condition: { $0 < 101 }, iterate: { $0 + 1 }).filter {
		$0.isPrime()
	}.toArray()
	
	integers.subscribe(onNext: {
		print($0, terminator: " ")
	}).disposed(by: disposeBag)
	
}

executeProcedure(for: "merge") {
	let disposeBag = DisposeBag()
	
	let pubSub1 = PublishSubject<String>()
	let pubSub2 = PublishSubject<String>()
	let pubSub3 = PublishSubject<String>()
	
	Observable.of(pubSub1, pubSub2, pubSub3).merge().subscribe(onNext: {
		print($0)
	}).disposed(by: disposeBag)
	
	pubSub1.onNext("test1")
	pubSub3.onNext("test3")
	pubSub2.onNext("test2")
}

executeProcedure(for: "zip") {
	let disposeBag = DisposeBag()
	
	let intPubSub1 = PublishSubject<Int>()
	let intPubSub2 = PublishSubject<Int>()
	let strPubSub1 = PublishSubject<String>()
	let strPubSub2 = PublishSubject<String>()
	
	Observable.zip(intPubSub1, intPubSub2, strPubSub1, strPubSub2) { intSub1, intSub2, strSub1, strSub2 in
		"\(intSub1) : \(strSub1) AND \(intSub2) : \(strSub2)"
	}.subscribe(onNext: {
		print($0)
	}).disposed(by: disposeBag)
	
	intPubSub1.onNext(0)
	intPubSub2.onNext(1)
	strPubSub1.onNext("test 0")
	strPubSub2.onNext("test 1")
	
	strPubSub2.onNext("test 3")
	intPubSub1.onNext(0)
	intPubSub2.onNext(1)
	strPubSub1.onNext("test 0")
	
	strPubSub2.onNext("test 2")
}


print(INT_MAX)

enum Test: String {
	case hello
	case test
	case world
}

print(Test.test.rawValue)

let testSwitches: [(String?, String?, String?)] = [("",nil,nil),
												   ("","",nil),
												   ("",nil,""),
												   ("","",""),
												   (nil,nil,nil),
												   (nil,"",nil),
												   (nil,nil,""),
												   (nil,"","")]
for test in testSwitches {
	var height: CGFloat = 0
	switch test {
	case (.some(_), nil, nil):
		print("100")
	case (.some(_), .some(_), nil):
		print("110")
	case (.some(_), nil, .some(_)):
		print("101")
	case (.some(_), .some(_), .some(_)):
		print("111")
	case (nil, nil, nil):
		print("000")
	case (nil, .some(_), nil):
		print("010")
	case (nil, nil, .some(_)):
		print("001")
	case (nil, .some(_), .some(_)):
		print("011")
	}
}

var examJSON = """
{	"name" : "hello",
	"age" : nil,
	"authors" : [ {
		"name" : "name1"
		}, {
		"name" : "name2"
		}
	]
}
"""

class testJsonDecode: Codable {
	var name: String
	var age: Int
	var authors: [[String: String]]
	init(name: String, age: Int, authors: [[String: String]]) {
		self.name = ""
		self.age = 0
		self.authors = []
	}
}

let testNamePath = \testJsonDecode.name
let json = examJSON.data(using: .utf8)

let testModel = try? JSONDecoder().decode(testJsonDecode.self, from: json!)

print(testModel!)
