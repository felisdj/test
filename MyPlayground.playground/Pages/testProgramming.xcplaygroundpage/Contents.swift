
import Foundation
import UIKit

var str = "Hello, playground"

var row = 3
var col = 5
var text = [ ["d", "a", "m", "i", "r"],
			 ["m", "a", "r", "k", "o"],
			 ["d", "a", "r", "k", "o"] ]

var rad = 180
var caseR = 0
var loopRow = 0
switch rad {
case 0:
	caseR = 0
	loopRow = row
case 45:
	caseR = 1
	loopRow = row+col
case 90:
	caseR = 2
	loopRow = col
case 135:
	caseR = 3
	loopRow = row+col
case 180:
	caseR = 4
	loopRow = row
case 225:
	caseR = 5
	loopRow = row+col
case 270:
	caseR = 6
	loopRow = col
case 315:
	caseR = 7
	loopRow = row+col
default:
	caseR = 0
	loopRow = row
}

for i in 0..<loopRow {
	if caseR == 0 {
		print(text[i].joined())
	} else if caseR == 4 {
		print(text.reversed()[i].reversed().joined())
	} else if caseR == 2 {
		var line = ""
		for j in 0..<row {
			line += text[j][i]
		}
		print(line)
	} else if caseR == 6 {
		var line = ""
		for j in 0..<row {
			line += text[j][loopRow-1-i]
		}
		print(line)
	} else if caseR == 1 {
//		for j in 0..<loopRow {
//
//		}
		
	}
}

enum TestString: String {
	case none
	case test
	case link
	
	static func typeFromText(_ str: String) -> TestString {
		if str == TestString.none.rawValue {
			return .none
		} else if str == TestString.test.rawValue {
			return .test
		} else {
			return .none
		}
	}
}

let testCase = TestString.none

print(TestString(rawValue: "non") ?? .none)

for name in UIFont.familyNames {
	print(name)
	if let nameString = name as? String
	{
		print(UIFont.fontNames(forFamilyName: nameString))
	}
}

let sayHello: () -> Void = {
	print("Hello")
}

sayHello()

// Closure take one parameter and return 1 parameter
let value: (Int) -> Int = { (value1) in
	return value1*2
}

print(value(5))

class TestClosure {
	
	var name: String
	
	init() {
		name = "TEST"
	}
	
	func process(name: String, _ changeName: (String)->()) -> TestClosure {
		self.name = name
		changeName(name)
		return self
	}
	func validation(_ valid: (Bool)->()) {
		valid(name.lowercased() == name)
	}
}

var test = TestClosure()
test.process(name: "Test") { (name) in
	print(name)
}.validation { (valid) in
	print("name is\(valid ? "" : "n't") lowercase")
}

class CaptureList: NSObject {
	let digit = 5
	typealias onCompletionHandler = (Int) -> Void
	
	override init() {
		super.init()
		
		makeSquareOfValue { [digit] squareDigit in
			print("Square of \(digit) is \(squareDigit)")
		}
	}
	
	func makeSquareOfValue(onCompletion: onCompletionHandler) {
		let squareDigit = digit * digit
		onCompletion(squareDigit)
	}
}

CaptureList()

struct Blog: Codable {
	let title: String
	let homepageURL: String
	let articles: [Article]
	
	enum CodingKeys: String, CodingKey {
		case title
		case homepageURL = "home_page_url"
		case articles = "items"
	}
	
}
struct Article: Codable {
	var id: String
	var url: String?
	var title: [String:String]
	var exps: [Experience]
	
	enum CodingKeys: String, CodingKey {
		case id
		case url
		case title
		case exps = "experiences"
	}
}
struct Experience: Codable {
	let id: String
	let name: String
}

let json: [String:Any] = [
	"id": "test",
    "url": "url",
	"title":["titleTest":"true"],
	"experiences": [
		[
		"id": "exp_id",
		"name": "exp_name"]
	]
]

let jsonData = try! JSONSerialization.data(withJSONObject: json, options: [])
var decoder = try! JSONDecoder().decode(Article.self, from: jsonData)

print(decoder.url)

let jsonEncoder = JSONEncoder()

// object -> dictionary
let dictionary = try JSONSerialization.jsonObject(with: jsonEncoder.encode(decoder)) as? [String: Any] ?? [:]

// object -> json string
let jData = try jsonEncoder.encode(decoder)
let jsonFinal = String(data: jData, encoding: String.Encoding.utf8)

print((dictionary["experiences"] as! [[String:Any]])[0]["id"] ?? "")

var hasher = Hasher()

hasher.combine("0")
print(hasher.finalize())

class ProductFactory: NSObject, NSCopying {
	var items = 8
	
	func copy(with zone: NSZone? = nil) -> Any {
		let factory = ProductFactory()
		factory.items = items
		return factory
	}
}

struct Product {
	let identifier: Int
	var name: String
	private var _factory = ProductFactory()
	
	init(identifier: Int, name: String) {
		self.identifier = identifier
		self.name = name
	}
	
	var getFactory: ProductFactory {
		return _factory
	}
	
	var setFactory: ProductFactory {
		mutating get {
			if !isKnownUniquelyReferenced(&_factory) {
				_factory = _factory.copy() as! ProductFactory
			}
			return _factory
		}
		
	}
}


let productA = Product(identifier: 234, name: "electronics")

var productB = productA

productB.setFactory.items = 4
productB.name = "newNameB"

print(productA.name)

print("product A items \(productA.getFactory.items)")
print("product B items \(productB.getFactory.items)")

class View<T>: NSObject {
	var dataSource: T!
	
	init(with data: T) {
		super.init()
		dataSource = data
	}
	
	func logData() {
		if dataSource is String {
			print("Object is String")
		} else if dataSource is Product {
			print("Object is Product")
		} else {
			print("Unknown")
		}
	}
}

let objectPro = View<Product>(with: productA)
objectPro.logData()

let objectStr = View<String>(with: "Test string")
objectStr.logData()

let objectOther = View<ProductFactory>(with: ProductFactory())
objectOther.logData()

enum TestInt: Int {
    case primary, secondary, third
}

print(TestInt.primary.rawValue)

func hex(fromColor color: UIColor) -> String {
    var fRed : CGFloat = 0
    var fGreen : CGFloat = 0
    var fBlue : CGFloat = 0
    var fAlpha: CGFloat = 0
    if color.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
        let iRed = Int(fRed * 255.0)
        let iGreen = Int(fGreen * 255.0)
        let iBlue = Int(fBlue * 255.0)
//            let iAlpha = Int(fAlpha * 255.0)
        let hexValue = String(format:"#%02X%02X%02X", iRed, iGreen, iBlue)
        
        return hexValue
    }
    return "#ffffff"
}

hex(fromColor: .white)
hex(fromColor: .red)
hex(fromColor: .blue)
hex(fromColor: .green)
hex(fromColor: .yellow)
