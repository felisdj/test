//: [Previous](@previous)

import Foundation
import RxSwift
import RxCocoa

var str = "Hello, playground"

//: [Next](@next)

func executeProcedure(for description: String, procedure: () -> Void) {
	print("Procedure executed for:", description)
	procedure()
}

executeProcedure(for: "scan and buffer") {
	let disposeBag = DisposeBag()
	
	let gameScore = PublishSubject<Int>()
	
	gameScore.buffer(timeSpan: 0.0, count: 3, scheduler: MainScheduler.instance).map {
		print($0, "--> ", terminator:  "")
		return $0.reduce(0, +)
		}.scan(501, accumulator: -).map { max($0, 0) }.subscribe(onNext: { print($0) }).disposed(by: disposeBag)
	
	gameScore.onNext(20)
	gameScore.onNext(31)
	gameScore.onNext(50)
	
	gameScore.onNext(23)
	gameScore.onNext(4)
	gameScore.onNext(20)
}

struct AModel {
    var numbers: [BModel]
}
class AViewModel: NSObject {
    var model: AModel!
    
    override init() {
        super.init()
        model = AModel(numbers: [BModel(number: 0), BModel(number: 1), BModel(number: 2), BModel(number: 3)])
    }
    func getBModel(index: Int) -> BModel {
        return model.numbers[index]
    }
    func setnumber(_ no: Int, at: Int) {
        model.numbers[at].number = no
    }
}
class A {
    var viewModel: AViewModel
    init() {
        viewModel = AViewModel()
    }
}

struct BModel {
    var number: Int
}
class BViewModel: NSObject {
    var model: BModel!
    
    init(number: BModel) {
        super.init()
        model = number
    }
    func setModelNumber(_ no: Int) {
        model.number = no
    }
}
class B {
    var viewModel: BViewModel!
}

var a = A()
var b = B()

var bm = a.viewModel.getBModel(index: 1)
bm.number = 3
a.viewModel.setnumber(4, at: 1)
a.viewModel.model.numbers[1].number
b.viewModel = BViewModel(number: a.viewModel.getBModel(index: 0))
b.viewModel.model.number
b.viewModel.setModelNumber(1)
b.viewModel.model.number
a.viewModel.getBModel(index: 0).number
print("test")
