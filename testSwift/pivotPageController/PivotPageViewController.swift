//
//  PivotPageViewController.swift
//  testSwift
//
//  Created by JJsobtid on 12/1/2561 BE.
//  Copyright © 2561 JJsobtid. All rights reserved.
//

import UIKit

@objc protocol PivotPageDelegate: class {
	var titleForPivotPage: String? { get }
	
	@objc optional func rootPivotPageDidShow()
	@objc optional func rootPivotPageWillHide()
	
}

class PivotPage: UIViewController {

	weak var pivotPageController: PivotPageViewController!
    
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		if pivotPageController == nil {
			pivotPageController = (navigationController?.viewControllers.first as? PivotPage)?.pivotPageController
		}
		
		refreshContentView()
	}
	
	fileprivate func refreshContentView() {
		pivotPageController.pagesCollectionView.isScrollEnabled = pivotPageShouldHandleNavigation()
		pivotPageController.pagesCollectionView.collectionViewLayout.invalidateLayout()
		
	}
	
	func shouldShowPivotMenu() -> Bool {
		
		return true
		
	}
	func shouldShowFooterView() -> Bool {
		
		return true
		
	}
	func pivotPageShouldHandleNavigation() -> Bool {
		
		return true
		
	}
	
	// MARK: - MIPivotRootPage
	
	var imageForPivotPage: UIImage? {
		
		return nil
		
	}
}

protocol PivotPageControllerDelegate: class {
	func PivotPageControllerWillShow(pivotPageController: PivotPageViewController)
}

class PivotPageViewController: UIViewController {
	
	typealias setUpClosure = (PivotPageViewController) -> ()
	
	var menuView: UIView!
	var menuCollectionView: UICollectionView!
	var pagesCollectionView: UICollectionView!
	
	var delegate: PivotPageControllerDelegate?
	
	private var setupClosure: setUpClosure?
	override var prefersStatusBarHidden: Bool { return true }
	
	fileprivate let pivotPageCellBaseIdentifier = PageCollectionViewCell.cellIdentifier
	var selectedIndex: Int = 0
	var rootPages: [PivotPageDelegate]!
	var pagesNumber: Int {
		return rootPages?.count ?? 0
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		setupCollectionView()
		
		setupClosure?(self)
		
		// Do any additional setup after loading the view.
	}
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		delegate?.PivotPageControllerWillShow(pivotPageController: self)
		
	}
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		refreshCollectionsLayout()
		
	}
	override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
		super.viewWillTransition(to: size, with: coordinator)
		
		coordinator.animate(
			alongsideTransition: { _ in
				
				self.refreshCollectionsLayout()
				
			}, completion: { _ in
				self.scrollToPage(atIndex: self.selectedIndex, animated: false)
			}
		)
		
	}
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	public static func get(rootPages: [PivotPageDelegate], setupClosure: setUpClosure?) -> PivotPageViewController {
		
		let pivotPageController = PivotPageViewController()
		
		pivotPageController.addRootPages(rootPages)
		pivotPageController.setupClosure = setupClosure
		
		return pivotPageController
		
	}
	
	// MARK: -SET UP
	private func setupCollectionView() {
		view.backgroundColor = UIColor.brandcoloralpha
		
		menuView = UIView(frame: CGRect())
		
		view.addSubview(menuView)
		menuView.snp.makeConstraints { (make) in
			make.leading.equalTo(view.snp.leading)
			make.trailing.equalTo(view.snp.trailing)
			make.top.equalTo(0)
			make.height.equalTo(44)
		}
		let collectionLayout = UICollectionViewLayout()
		
		menuCollectionView = UICollectionView(frame: CGRect(), collectionViewLayout: collectionLayout)
		menuCollectionView.register(MenuPageCollectionViewCell.self, forCellWithReuseIdentifier: MenuPageCollectionViewCell.cellIdentifier)
		
		let collectionLayout2 = UICollectionViewLayout()
		pagesCollectionView = UICollectionView(frame: CGRect(), collectionViewLayout: collectionLayout2)
		for i in 0...(rootPages.count-1) {
			pagesCollectionView.register(PageCollectionViewCell.self, forCellWithReuseIdentifier: PageCollectionViewCell.cellIdentifier + String(i))
		}
		
		menuView.addSubview(menuCollectionView)
		menuCollectionView.snp.makeConstraints { (make) in
			make.edges.equalTo(menuView.snp.edges)
		}
		view.addSubview(pagesCollectionView)
		pagesCollectionView.snp.makeConstraints { (make) in
			make.leading.equalTo(view.snp.leading)
			make.trailing.equalTo(view.snp.trailing)
			make.top.equalTo(menuCollectionView.snp.bottom)
			make.bottom.equalTo(view.snp.bottom)
		}
	}
	
	private func addRootPages(_ rootPages: [PivotPageDelegate]) {
		func setParentForNavigationController(_ navigationController: UINavigationController) {
			guard let rootViewController = navigationController.viewControllers.first as? PivotPage else { return }
			
			rootViewController.pivotPageController = self
		}
		
		func setParentForRootPivotPage(_ pivotPageDelegate: PivotPageDelegate) {
			guard let pivotPage = pivotPageDelegate as? PivotPage else { return }
			pivotPage.pivotPageController = self
		}
		
		func setParentForTabBarController(_ tabbarController: UITabBarController) {
			guard let tabBarChildControllers = tabbarController.viewControllers else { return }
			
			for childController in tabBarChildControllers {
				
				if let navController = childController as? UINavigationController {
					setParentForNavigationController(navController)
				} else if let childController = childController as? PivotPage {
					childController.pivotPageController = self
				}
				
			}
		}
		
		self.rootPages = rootPages
		
		for pivotRootPage in rootPages {
			
			if let tabBarController = pivotRootPage as? UITabBarController {
				
				setParentForTabBarController(tabBarController)
				
			} else if let navigationController = pivotRootPage as? UINavigationController {
				
				setParentForNavigationController(navigationController)
				
			} else {
				
				setParentForRootPivotPage(pivotRootPage)
				
			}
			
		}
		
	}
	
	// MARK: - Refresh
	
	fileprivate func refreshCollectionsLayout() {
		
		menuCollectionView.collectionViewLayout.invalidateLayout()
		pagesCollectionView.collectionViewLayout.invalidateLayout()
		
		view.layoutIfNeeded()
		
	}
	
	// MARK: - Shortcuts
	
	fileprivate func pivotPage(atIndex index: Int) -> PivotPageDelegate {
		
		return rootPages[max(0, min(pagesNumber, index))]
		
	}
	fileprivate func menuString(atIndex index: Int) -> String? {
		
		return pivotPage(atIndex: index).titleForPivotPage
		
	}

	fileprivate func viewController(atIndex index: Int) -> UIViewController? {
		
		return pivotPage(atIndex: index) as? UIViewController
		
	}
	
	// MARK: - Menu stuff
	
	func updateMenu(forAnimationProgress animationProgress: Float) {
		
		for i in 0...pagesNumber {
			
			guard let cell = menuCollectionView.cellForItem(at: IndexPath(item: i, section: 0)) as? MenuPageCollectionViewCell else { return }
			
			let cellAnimationProgress = 1 - abs(animationProgress - Float(i))
			
			cell.updateUI(forProgress: cellAnimationProgress, selected: false)
			
		}
		
	}
	func scrollToPage(atIndex index: Int, animated: Bool = true) {
		
		pagesCollectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .left, animated: animated)
		
	}
}

extension PivotPageViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return pagesNumber
	}
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		switch collectionView {
			
		case menuCollectionView:
			
			guard
				let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MenuPageCollectionViewCell.cellIdentifier, for: indexPath) as? MenuPageCollectionViewCell
				else { return UICollectionViewCell() }
			
			cell.set(title: menuString(atIndex: indexPath.item)!, selected: selectedIndex == indexPath.item)
//			cell.configure(
//				image: menuImage(atIndex: indexPath.item),
//				badgeValue: badgeValue(atIndex: indexPath.item),
//				badgeConfig: nil,
//				shouldHideBadgeOnPageFocus: shouldHideBadgeOnPageFocus(atIndex: indexPath.item),
//				selected: selectedIndex == indexPath.item
//			)
			
			return cell
			
		case pagesCollectionView:
			
			guard
				let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PageCollectionViewCell.cellIdentifier + String(indexPath.item), for: indexPath) as? PageCollectionViewCell,
				let viewController = viewController(atIndex: indexPath.item)
				else { return UICollectionViewCell() }
			
			cell.set(viewController: viewController)
//			cell.configure(viewController: viewController)
			
			return cell
			
		default:
			
			return UICollectionViewCell()
			
		}
		
	}
	
	// UICollectionViewDelegateFlowLayout
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		
		switch collectionView {
			
		case menuCollectionView:
			
			return CGSize(width: collectionView.frame.height, height: collectionView.frame.height)
			
		case pagesCollectionView:
			
			return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
			
		default:
			
			return CGSize.zero
			
		}
		
	}
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		
		return 0
		
	}
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
		
		return 0
		
	}
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		
		switch collectionView {
			
		case menuCollectionView:
			
			let cellCount = CGFloat(collectionView.numberOfItems(inSection: section))
			let collectionViewWidth = collectionView.bounds.size.width
			
			let totalCellWidth = cellCount * collectionView.frame.height
			let totalCellSpacing = cellCount - 1
			
			let totalCellsWidth = totalCellWidth + totalCellSpacing
			
			let edgeInsets = (collectionViewWidth - totalCellsWidth) / 2.0
			
			return edgeInsets > 0 ? UIEdgeInsets(top: 0, left: edgeInsets, bottom: 0, right: edgeInsets) : UIEdgeInsets.zero
			
		default:
			
			return UIEdgeInsets.zero
			
		}
	}
	
	// UICollectionViewDelegate
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		
		let oldSelectedIndex = selectedIndex
		selectedIndex = indexPath.item
		
		pivotPage(atIndex: oldSelectedIndex).rootPivotPageWillHide?()
		pivotPage(atIndex: selectedIndex).rootPivotPageDidShow?()
		
		scrollToPage(atIndex: self.selectedIndex)
		
	}
}

// MARK: - UIScrollViewDelegate

extension PivotPageViewController: UIScrollViewDelegate {
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		
		updateMenu(forAnimationProgress: Float(scrollView.contentOffset.x/scrollView.frame.width))
		
	}
	
	func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
		
		pivotPage(atIndex: Int(floor(targetContentOffset.pointee.x/scrollView.frame.width))).rootPivotPageDidShow?()
		
	}
	
	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		
		selectedIndex = Int(floor(scrollView.contentOffset.x/scrollView.frame.width))
		
	}
	
	func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
		
		pivotPage(atIndex: selectedIndex).rootPivotPageWillHide?()
		
	}
	
}

