//
//  PageCollectionViewCell.swift
//  testSwift
//
//  Created by JJsobtid on 12/1/2561 BE.
//  Copyright © 2561 JJsobtid. All rights reserved.
//

import UIKit

class PageCollectionViewCell: UICollectionViewCell {
    static let cellIdentifier = "PageCollectionViewCell"
	
	func set(viewController: UIViewController) {
		addSubview(viewController.view)
		
		viewController.view.snp.makeConstraints { (make) in
			make.edges.equalTo(self.snp.edges)
		}
	}
}
