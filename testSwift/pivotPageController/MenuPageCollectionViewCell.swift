//
//  MenuPageCollectionViewCell.swift
//  testSwift
//
//  Created by JJsobtid on 12/1/2561 BE.
//  Copyright © 2561 JJsobtid. All rights reserved.
//

import UIKit

class MenuPageCollectionViewCell: UICollectionViewCell {
    static let cellIdentifier = "MenuPageCollectionViewCell"
	
	var pageLabel: UILabel!
	var selectedBar: UIView!
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		pageLabel = UILabel()
//		pageLabel.font = .kanitReg14
		pageLabel.textColor = .guestcolor
		
		selectedBar = UIView()
		selectedBar.backgroundColor = .white
		
		addSubview(pageLabel)
		pageLabel.snp.makeConstraints { (make) in
			make.edges.equalTo(snp.edges)
		}
		
		addSubview(selectedBar)
		selectedBar.snp.makeConstraints { (make) in
			make.leading.equalTo(snp.leading)
			make.trailing.equalTo(snp.trailing)
			make.bottom.equalTo(snp.bottom)
			make.height.equalTo(3)
		}
		
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func set(title: String, selected: Bool) {
		
		pageLabel.text = title
		
		updateUI(forProgress: selected ? 1 : 0, selected: selected)
	}
	
	func updateUI(forProgress animationProgress: Float, selected: Bool) {
		let animationProgress = CGFloat(max(0, animationProgress))
		
		if selected {
			pageLabel.textColor = .brandcolor1
		} else {
			pageLabel.textColor = .guestcolor
		}
	}
	
}
