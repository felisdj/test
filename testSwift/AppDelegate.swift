//
//  AppDelegate.swift
//  testSwift
//
//  Created by JJsobtid on 4/10/2560 BE.
//  Copyright © 2560 JJsobtid. All rights reserved.
//

import UIKit
import Firebase
import GoogleMaps
import Hero
import LineSDK
import RxSwift
import RxCocoa

//@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?
	var tabbar: TabBarViewController?
	var biometricAuth = BiometricAuth.shared
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		
		GMSServices.provideAPIKey("AIzaSyD5O2xlfhf94JySL1iF0nc8mSBrsBahYP0")
		LoginManager.shared.setup(channelID: "1653671842", universalLinkURL: nil) // LINE
		FirebaseApp.configure()
		
		if let gai = GAI.sharedInstance() {
//			assert(false, "Google Analytics not configured correctly")
		
			gai.tracker(withTrackingId: "141221287")
			gai.trackUncaughtExceptions = true
			
			gai.logger.logLevel = .verbose
		}
//		let filePath = ConfigurationManager.shared.getFirebaseConfigurePath()
//		if let options = FirebaseOptions(contentsOfFile: filePath) {
//			FirebaseApp.configure(options: options)
//		} else {
//		}
		// Override point for customization after application launch.
//		if !(self.window != nil) {
			self.window = UIWindow(frame: UIScreen.main.bounds)
			self.tabbar = TabBarViewController()
			self.window?.rootViewController = self.tabbar
			self.window?.makeKeyAndVisible()

		return true
	}

	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	}

	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
	}

	func applicationDidBecomeActive(_ application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	}

	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	}
	
	func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
		return LoginManager.shared.application(app, open: url)
	}
	
}

