//
//  TabBarViewController.swift
//  testSwift
//
//  Created by JJsobtid on 4/10/2560 BE.
//  Copyright © 2560 JJsobtid. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
		let configure = ConfigurationManager.shared
		print(configure.getBackendUrl())
		
		let v1: FirstViewController = FirstViewController()
		let v2: TestViewController = TestViewController()
		
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		let controller = storyboard.instantiateViewController(withIdentifier: "CalendarViewController")
		
		let v3:CalendarViewController = controller as! CalendarViewController
		
		v1.str = "View 1"
//		v2.str = "View 2"
		
		let v4 = ChartViewController()
		let v5 = GoogleMapViewController()
		
		let nav_1:UINavigationController = UINavigationController(rootViewController: v1)
		nav_1.tabBarItem.title = "view1"
		nav_1.tabBarItem.image = UIImage(named: "Image")
		nav_1.tabBarItem.badgeValue = "N"
		nav_1.tabBarItem.setBadgeTextAttributes([.font : UIFont.systemFont(ofSize: 12)], for: .normal)
		
		let nav_2:UINavigationController = UINavigationController(rootViewController: v2)
		nav_2.tabBarItem.title = "view2"
		nav_2.tabBarItem.image = UIImage(named: "Image")
		
		let nav_3:UINavigationController = UINavigationController(rootViewController: v3)
		nav_3.tabBarItem.title = "Calendar"
		nav_3.tabBarItem.image = UIImage(named: "Image")
		
		let nav_4:UINavigationController = UINavigationController(rootViewController: v4)
		nav_4.tabBarItem.title = "Calendar"
		nav_4.tabBarItem.image = UIImage(named: "Image")
		
		let nav_5:UINavigationController = UINavigationController(rootViewController: v5)
		nav_5.tabBarItem.title = "Maps"
		nav_5.tabBarItem.image = UIImage(named: "Image")
		
		let listVC = ListViewController()
		let nav_6:UINavigationController = UINavigationController(rootViewController: listVC)
		nav_6.tabBarItem.title = "List"
		nav_6.tabBarItem.image = UIImage(named: "list")
		
		self.setViewControllers([nav_1, nav_2, nav_3, nav_4, nav_6], animated: true)
		self.delegate = self as? UITabBarControllerDelegate
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
