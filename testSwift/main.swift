//
//  main.swift
//  testSwift
//
//  Created by JJsobtid on 27/3/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

let isRunningTests = NSClassFromString("XCTestCase") != nil
let appDelegateClass = isRunningTests ? NSStringFromClass(FakeAppDelegate.self) : NSStringFromClass(AppDelegate.self)
_ = UIApplicationMain(
	CommandLine.argc, CommandLine.unsafeArgv,
	nil, appDelegateClass
)
//let args = UnsafeMutableRawPointer(CommandLine.unsafeArgv).bindMemory(to: UnsafeMutablePointer<Int8>.self, capacity: Int(CommandLine.argc))
//UIApplicationMain(CommandLine.argc, args, nil, appDelegateClass)
