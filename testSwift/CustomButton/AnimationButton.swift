//
//  AnimationButton.swift
//  testSwift
//
//  Created by JJsobtid on 22/1/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

class AnimationButton: UIButton, AnimationLayerDelegate {

	var animationLayer: AnimationLayer!
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		super.touchesBegan(touches, with: event)
		
		if let touch = touches.first {
			let point = touch.location(in: self)
//			print("touch at : \(point)")
			let inkLayer = AnimationLayer.init()
			inkLayer.maxRippleRadius = 0
			inkLayer.frame = bounds
			inkLayer.opacity = 0
			inkLayer.animationDelegate = self
			layer.addSublayer(inkLayer)
			inkLayer.startInkAtPoint(point, animation: true)
			animationLayer = inkLayer
		}
	}
	
	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		if let touch = touches.first {
			let point = touch.location(in: self)
			animationLayer.endInkAtPoint(point, animation: true)			
		}
	}

}
