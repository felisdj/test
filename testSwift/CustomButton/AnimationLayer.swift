//
//  AnimationLayer.swift
//  testSwift
//
//  Created by JJsobtid on 21/1/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

@objc protocol AnimationLayerDelegate: NSObjectProtocol {
	@objc optional func inkLayerAnimationDidStart(inkLayer: AnimationLayer)
	@objc optional func inkLayerAnimationDidEnd(inkLayer: AnimationLayer)
}

//static inline CGFloat MDCHypot(CGFloat x, CGFloat y) {
//	#if CGFLOAT_IS_DOUBLE
//	return hypot(x, y);
//	#else
//	return hypotf(x, y);
//	#endif
//}
func animationHypot(x: CGFloat, y: CGFloat) -> CGFloat {
	#if CGFLOAT_IS_DOUBLE
	return hypot(x, y)
	#else
	return CGFloat(hypotf(Float(x), Float(y)))
	#endif
}
class AnimationLayer: CAShapeLayer {

	
	let animationLayerCommonDuration: CGFloat = 0.083
	let animationLayerEndFadeOutDuration: CGFloat = 0.15
	let animationLayerStartScalePositionDuration: CGFloat = 0.5
	let animationLayerStartFadeHalfDuration: CGFloat = 0.167
	let animationLayerStartFadeHalfBeginTimeFadeOutDuration: CGFloat = 0.25
	
	let animationLayerScaleStartMin: CGFloat = 0.2
	let animationLayerScaleStartMax: CGFloat = 0.6
	let animationLayerDivisor: CGFloat = 300
	
	let opacityString = "opacity"
	let positionString = "position"
	let scaleString = "transform.scale"
	
	weak var animationDelegate: AnimationLayerDelegate?
	var startAnimationActive: Bool!
	
	var endAnimationDelay: CGFloat!
	var finalRadius: CGFloat!
	var initialRadius: CGFloat!
	var maxRippleRadius: CGFloat!
	
	var inkColor: UIColor!
	
	func startAnimationAtPoint(_ point: CGPoint) {
		startInkAtPoint(point, animation: true)
	}
	func startInkAtPoint(_ point: CGPoint, animation: Bool) {
		var radius = finalRadius ?? 0
		if maxRippleRadius > 0 {
			radius = maxRippleRadius
		}
		let ovalRect = CGRect(x: bounds.width / 2 - radius, y: bounds.height / 2 - radius, width: radius * 2, height: radius * 2)
		let circlePath = UIBezierPath(ovalIn: ovalRect)
		path = circlePath.cgPath
		fillColor = inkColor.cgColor
		if !animation {
			opacity = 1
			position = CGPoint(x: bounds.width / 2, y: bounds.height / 2)
		} else {
			opacity = 0
			position = point
			startAnimationActive = true
			
			let materialTimingFunction = CAMediaTimingFunction(controlPoints: 0.4, 0, 0.2, 1)
			
			var scaleStart = min(bounds.width, bounds.height) / animationLayerDivisor
			if scaleStart < animationLayerScaleStartMin {
				scaleStart = animationLayerScaleStartMin
			} else if scaleStart > animationLayerScaleStartMax {
				scaleStart = animationLayerScaleStartMax
			}
			
			let scaleAnimate = CABasicAnimation()
			scaleAnimate.keyPath = scaleString
			scaleAnimate.fromValue = scaleStart
			scaleAnimate.toValue = 1.0
			scaleAnimate.duration = CFTimeInterval(animationLayerStartScalePositionDuration)
			scaleAnimate.beginTime = CFTimeInterval(animationLayerCommonDuration)
			scaleAnimate.timingFunction = materialTimingFunction
			scaleAnimate.fillMode = CAMediaTimingFillMode.forwards
			scaleAnimate.isRemovedOnCompletion = false
			
			let centerPath = UIBezierPath()
			let startPoint = point
			let endPoint = CGPoint(x: bounds.width / 2, y: bounds.height / 2)
			centerPath.move(to: startPoint)
			centerPath.addLine(to: endPoint)
			centerPath.close()
			
			let positionAnimate = CAKeyframeAnimation()
			positionAnimate.keyPath = positionString
			positionAnimate.path = centerPath.cgPath
			positionAnimate.keyTimes = [0, 1]
			positionAnimate.values = [0, 1]
			positionAnimate.duration = CFTimeInterval(animationLayerStartScalePositionDuration)
			positionAnimate.beginTime = CFTimeInterval(animationLayerCommonDuration)
			positionAnimate.fillMode = CAMediaTimingFillMode.forwards
			positionAnimate.isRemovedOnCompletion = false
			
			let fadeInAnimate = CABasicAnimation()
			fadeInAnimate.keyPath = opacityString
			fadeInAnimate.fromValue = 0
			fadeInAnimate.toValue = 1
			fadeInAnimate.duration = CFTimeInterval(animationLayerCommonDuration)
			fadeInAnimate.beginTime = CFTimeInterval(animationLayerCommonDuration)
			fadeInAnimate.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
			fadeInAnimate.fillMode = CAMediaTimingFillMode.forwards
			fadeInAnimate.isRemovedOnCompletion = false
			
			CATransaction.begin()
			let animateGroup = CAAnimationGroup()
			animateGroup.animations = [scaleAnimate, positionAnimate, fadeInAnimate]
			animateGroup.duration = CFTimeInterval(animationLayerStartScalePositionDuration)
			animateGroup.fillMode = CAMediaTimingFillMode.forwards
			animateGroup.isRemovedOnCompletion = false
			CATransaction.setCompletionBlock { [weak self] in
				self?.startAnimationActive = false
			}
			add(animateGroup, forKey: nil)
			CATransaction.commit()
		}
		animationDelegate?.inkLayerAnimationDidStart?(inkLayer: self)
		
	}
	func changeAnimationAtPoint(_ point: CGPoint) {
		var animationDelay: CGFloat = 0
		if startAnimationActive != nil {
			animationDelay = animationLayerStartFadeHalfBeginTimeFadeOutDuration + animationLayerStartFadeHalfDuration
		}
		
		let viewContainsPoint = bounds.contains(point) ? true : false
		let currentOpacity = presentation()?.opacity
		var updateOpacity: CGFloat = 0
		if viewContainsPoint {
			updateOpacity = 1
		}
		
		let changeAnimate = CABasicAnimation()
		changeAnimate.keyPath = opacityString
		changeAnimate.fromValue = currentOpacity
		changeAnimate.toValue = updateOpacity
		changeAnimate.duration = CFTimeInterval(animationLayerCommonDuration)
		changeAnimate.beginTime = convertTime(CACurrentMediaTime() + Double(animationDelay), from: nil)
		changeAnimate.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
		changeAnimate.fillMode = CAMediaTimingFillMode.forwards
		changeAnimate.isRemovedOnCompletion = false
		add(changeAnimate, forKey: nil)
	}
	func endAnimationAtPoint(_ point: CGPoint) {
		endInkAtPoint(point, animation: true)
	}
	func endInkAtPoint(_ point: CGPoint, animation: Bool) {
		if startAnimationActive {
			endAnimationDelay = animationLayerStartFadeHalfBeginTimeFadeOutDuration
		}
		var opacity: CGFloat = 1
		if !bounds.contains(point) {
			opacity = 0
		}
		
		if !animation {
			opacity = 0
			animationDelegate?.inkLayerAnimationDidEnd?(inkLayer: self)
			removeFromSuperlayer()
		} else {
			CATransaction.begin()
			let fadeOutAnimate = CABasicAnimation()
			fadeOutAnimate.keyPath = opacityString
			fadeOutAnimate.fromValue = opacity
			fadeOutAnimate.toValue = 0
			fadeOutAnimate.duration = CFTimeInterval(animationLayerEndFadeOutDuration)
			fadeOutAnimate.beginTime = convertTime(CACurrentMediaTime() + Double(endAnimationDelay), from: nil)
			fadeOutAnimate.fillMode = CAMediaTimingFillMode.forwards
			fadeOutAnimate.isRemovedOnCompletion = false
			CATransaction.setCompletionBlock { [weak self] in
				if let weakSelf = self {
					weakSelf.animationDelegate?.inkLayerAnimationDidEnd?(inkLayer: weakSelf)					
				}
				self?.removeFromSuperlayer()
			}
			add(fadeOutAnimate, forKey: nil)
			CATransaction.commit()
			
		}
	}
	
	override init() {
		super.init()
		endAnimationDelay = animationLayerEndFadeOutDuration
		inkColor = UIColor(white: 0, alpha: 0.08)
	}
	
	override init(layer: Any) {
		super.init(layer: layer)
		endAnimationDelay = 0
		finalRadius = 0
		initialRadius = 0
		inkColor = UIColor(white: 0, alpha: 0.08)
		startAnimationActive = false
		if let layer = layer as? AnimationLayer {
			endAnimationDelay = layer.endAnimationDelay
			finalRadius = layer.finalRadius
			initialRadius = layer.initialRadius
			maxRippleRadius = layer.maxRippleRadius
			inkColor = layer.inkColor
			startAnimationActive = false
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
		inkColor = UIColor(white: 0, alpha: 0.08)
//		fatalError("init(coder:) has not been implemented")
	}
	
	override func setNeedsLayout() {
		super.setNeedsLayout()
		setRadiiWithRect(bounds)
	}
	
	fileprivate func setRadiiWithRect(_ rect: CGRect) {
		initialRadius = animationHypot(x: rect.height, y: rect.width / 2 * 0.6)
		finalRadius = animationHypot(x: rect.height, y: rect.width / 2 + 10)
	}
}
