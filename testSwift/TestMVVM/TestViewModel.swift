//
//  TestViewModel.swift
//  testSwift
//
//  Created by JJsobtid on 26/7/2561 BE.
//  Copyright © 2561 JJsobtid. All rights reserved.
//

import UIKit
import AVFoundation
import RxCocoa
import RxSwift
import Firebase

class TestViewModel: NSObject {
	
	var testModelList: [TestModel]
	var userList: Variable<[TestModel]> = Variable([])
	
	var dataSource: Observable<[TestModel]>!
	var isLoading: Variable<Bool> = Variable(false)
	
	override init() {
		testModelList = []
		super.init()
		
	}
	init(reloadTap: Driver<Void>, disposeBag: DisposeBag) {
		testModelList = []
		super.init()
		reload()
		dataSource = userList.asObservable()
		reloadTap.drive(onNext: { [unowned self] _ in
			self.userList.value = []
			self.reload()
		}, onCompleted: nil).disposed(by: disposeBag)
	}
	
	init(withModel model: TestModel) {
		testModelList = [model]
	}
	
	func reload() {
		isLoading.value = true
		let defaultStore = Firestore.firestore()
		defaultStore.collection("users").getDocuments { (querySnapshot, err) in
			if let error = err {
				print(error)
//				observer.onError(error)
			}
			self.isLoading.value = false
			if ((querySnapshot?.documents) != nil) {
				self.userList.value = querySnapshot!.documents.map({ (document) -> TestModel in
					return TestModel(document: document.data())
				})
			}
		}
	}
	
	func loadDataTest() -> Observable<[TestModel]>{
//		var ref: DocumentReference? = nil
		
		return Observable.create({ (observer) -> Disposable in
			let defaultStore = Firestore.firestore()
			defaultStore.collection("users").getDocuments { (querySnapshot, err) in
				if let error = err {
					print(error)
					observer.onError(error)
				}
				if ((querySnapshot?.documents) != nil) {
					self.userList.value = querySnapshot!.documents.map({ (document) -> TestModel in
						return TestModel(document: document.data())
					})
					observer.onNext(self.userList.value)
				}
				observer.onCompleted()
			}
			return Disposables.create {
				
			}
		})
//		for i in 0...4 {
//			testModelList.append(TestModel(p: "Index: \(i)", v: i))
//
//		}
	}
	
	func clickOn(index: Int) {
//		testModelList[index].updateValue()
		speckOn(index: index)
	}
	func speckOn(index: Int) {
		let utterance = AVSpeechUtterance(string: "プレス位置\(index)")
//		utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
        if Bool.random() {
            utterance.voice = AVSpeechSynthesisVoice(language: "ja-JP")
        } else {
            utterance.voice = AVSpeechSynthesisVoice(identifier: "com.apple.ttsbundle.siri_male_ja-JP_compact")
        }
		let synth = AVSpeechSynthesizer()
		synth.speak(utterance)
	}
	
	func numberOfList() -> Int {
		return testModelList.count
	}
	func getModel(index: Int) -> TestModel {
		return testModelList[index]
	}
}
