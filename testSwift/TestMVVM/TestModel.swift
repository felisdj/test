//
//  TestModel.swift
//  testSwift
//
//  Created by JJsobtid on 26/7/2561 BE.
//  Copyright © 2561 JJsobtid. All rights reserved.
//

import UIKit

class TestModel: NSObject {
	
	var email, username: String!
	var parameter: String
	var value: Dynamic<Int>
	var timer: Timer!
	
	init(document: [String: Any]) {
		email = document["email"] as? String ?? ""
		username = document["username"] as? String ?? ""
		parameter = ""
		value = Dynamic(0)
		super.init()
	}
	
	init(p: String, v: Int) {
		parameter = p
		value = Dynamic(v)
		super.init()
		timer = Timer(timeInterval: 1.0, target: self, selector: #selector(updateValue), userInfo: nil, repeats: true)
		RunLoop.main.add(timer, forMode: .common)
	}
	
	@objc func updateValue() {
		value.value += 1
	}
	
	func onClick() {
		value.value = 0
	}
	func getTitle() -> String {
		return parameter
	}
	func getValue(_ returnValue: @escaping (Int) -> ()) {
		value.bindAndFire { (val) in
			returnValue(val)
		}
//		return value.value
	}
}
