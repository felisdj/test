//
//  TestViewController.swift
//  testSwift
//
//  Created by JJsobtid on 26/7/2561 BE.
//  Copyright © 2561 JJsobtid. All rights reserved.
//

import UIKit
import TagListView
import RxSwift
import RxDataSources
import Spruce

struct SectionOfCustomData: SectionModelType {
	var header: String
	var items: [Item]
	
	typealias Item = TestModel
	
	init(original: SectionOfCustomData, items: [Item]) {
		self = original
		self.items = items
	}
}

class TestViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, TagListViewDelegate {

	let animations: [StockAnimation] = [.slide(.left, .severely), .fadeIn]
	var sortFunction: SortFunction? = LinearSortFunction(direction: .topToBottom, interObjectDelay: 0.05)
	var animationView: UIView?
	var timer: Timer?
	
	var tableView: UITableView!
	var tableViewModel: TestViewModel!
	var tagListView: TagListView!
	var dataLoading: UIActivityIndicatorView!
	var nowCell: UITableViewCell!
	var nowIndexPath: IndexPath!
	
	var refreshButton: UIButton!
	
	let disposeBag = DisposeBag()
//	override var preferredStatusBarStyle: UIStatusBarStyle {
//		return .lightContent
//	}
	fileprivate func loadTableData() {
		let dataSources = RxTableViewSectionedReloadDataSource<SectionOfCustomData>(configureCell: {
			dataSources, tableView, indexPath, item in
			let cell = tableView.dequeueReusableCell(withIdentifier: "reuse", for: indexPath)
			cell.textLabel?.text = item.email ?? ""
			return cell
		})
		
		tableViewModel = TestViewModel(reloadTap: refreshButton.rx.tap.asDriver(), disposeBag: disposeBag)
		tableViewModel.dataSource.bind(to: tableView.rx.items(cellIdentifier: "reuse", cellType: UITableViewCell.self)) {
			row, user, cell in
			cell.textLabel?.text = user.username ?? ""
			cell.detailTextLabel?.text = user.email ?? ""
			print(user.username)
		}.disposed(by: disposeBag)
		tableViewModel.isLoading.asObservable().bind { (isLoading) in
			if isLoading {
				self.dataLoading.startAnimating()
			} else {
				self.callAnimation()
				self.dataLoading.stopAnimating()
			}
		}.disposed(by: disposeBag)
		
        Observable
        .zip(tableView.rx.itemSelected, tableView.rx.modelSelected(TestModel.self))
        .bind { [unowned self] indexPath, user in
            print(user.email!)
            self.tableViewModel.clickOn(index: indexPath.item)
            if let selectedRowIndexPath = self.tableView.indexPathForSelectedRow {
                self.tableView.deselectRow(at: selectedRowIndexPath, animated: true)
            }
        }
        .disposed(by: disposeBag)
//		tableView.rx.modelSelected(TestModel.self).subscribe(onNext: {
//			user in
//			print(user.email!)
//            self.tableViewModel.clickOn(index: Int.random(in: 0...10))
//			if let selectedRowIndexPath = self.tableView.indexPathForSelectedRow {
//				self.tableView.deselectRow(at: selectedRowIndexPath, animated: true)
//			}
//		}).disposed(by: disposeBag)
		
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()

		navigationController?.setNavigationBarHidden(true, animated: false)
		
		prepareAnimation()
		
		tableView = UITableView(frame: CGRect(), style: .plain)
		tableView.register(UITableViewCell.self, forCellReuseIdentifier: "reuse")
		
//		tableView.delegate = self
//		tableView.dataSource = self
		tableView.estimatedRowHeight = 60
		tableView.rowHeight = UITableView.automaticDimension
		tableView.tableFooterView = UIView()
		
		animationView = tableView
		view.addSubview(tableView)
		
		tableView.snp.makeConstraints { (make) in
			make.edges.equalTo(view.snp.edges)
		}
		
		refreshButton = UIButton(type: .system)
		refreshButton.setTitle("Refresh", for: .normal)

		view.addSubview(refreshButton)
		refreshButton.snp.makeConstraints { (make) in
			make.trailing.equalTo(view.snp.trailing).offset(-8)
			make.top.equalTo(view.snp.top).offset(44 + 8)
			make.size.equalTo(CGSize(width: 60, height: 20))
		}
		
		dataLoading = UIActivityIndicatorView(style: .gray)
		view.addSubview(dataLoading)
		dataLoading.stopAnimating()
		dataLoading.snp.makeConstraints { (make) in
			make.center.equalTo(tableView.snp.center)
		}
		
		loadTableData()
        // Do any additional setup after loading the view.
    }

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return tableViewModel.numberOfList() + 1
	}
//	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//		return 60
//	}
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		var cell = tableView.dequeueReusableCell(withIdentifier: "reuse")
	
		if cell == nil {
			cell = UITableViewCell(style: .subtitle, reuseIdentifier: "reuse")
		}

		if indexPath.item == tableViewModel.numberOfList() {
			if tagListView == nil {
				tagListView = TagListView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 16))
				tagListView.delegate = self
				tagListView.addTag("addTag...")
			}
			cell?.addSubview(tagListView)
			nowCell = cell
			nowIndexPath = indexPath
			tagListView.snp.remakeConstraints { (make) in
				make.top.equalTo(cell!.snp.top)
				make.leading.equalTo(cell!.snp.leading)
				make.height.equalTo(tagListView.intrinsicContentSize.height)
				make.trailing.equalTo(cell!.snp.trailing)
			}
		} else {
			if tagListView != nil {
				tagListView.removeFromSuperview()
			}
			cell!.textLabel?.text = tableViewModel.getModel(index: indexPath.item).getTitle()
			tableViewModel.getModel(index: indexPath.item).getValue { (val) in
				cell!.detailTextLabel?.text = "onClick: \(val)"
			}
		}
		
		return cell!
	}
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if indexPath.item == tableViewModel.numberOfList() {
			
		} else {
			tableViewModel.clickOn(index: indexPath.item)
			
		}
		tableView.deselectRow(at: indexPath, animated: true)
		//		tableView.reloadRows(at: [indexPath], with: .automatic)
	}
	
	func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
		if title != "addTag..." {
			sender.removeTag(title)
		} else {
			sender.removeTagView(tagView)
			sender.addTag("test\(sender.tagViews.count)")
			sender.addTag("addTag...")
			print("sender height: \(sender.intrinsicContentSize)")
			sender.snp.remakeConstraints { (make) in
				make.top.equalTo(nowCell.snp.top)
				make.leading.equalTo(nowCell.snp.leading)
				make.height.equalTo(tagListView.intrinsicContentSize.height)
				make.trailing.equalTo(nowCell.snp.trailing)
			}
			tableView.reloadRows(at: [nowIndexPath], with: .automatic)
//			sender.frame = CGRect(origin: sender.frame.origin, size: sender.intrinsicContentSize)
		}
	}
	
	func prepareAnimation() {
		animationView?.spruce.prepare(with: animations)
		
		timer?.invalidate()
		timer = Timer.scheduledTimer(timeInterval: 0, target: self, selector: #selector(callAnimation), userInfo: nil, repeats: false)
	}
	
	@objc func callAnimation() {
		guard let sortFunction = sortFunction else {
			return
		}
		let animation = SpringAnimation(duration: 0.7)
		DispatchQueue.main.async {
			self.animationView?.spruce.animate(self.animations, animationType: animation, sortFunction: sortFunction)
		}
	}
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
