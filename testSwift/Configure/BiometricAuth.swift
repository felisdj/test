//
//  BiometricAuth.swift
//  testSwift
//
//  Created by Jackthip Pureesatian on 29/6/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import Foundation
import LocalAuthentication

class BiometricAuth: NSObject {
	
	enum BioMetricSupported: String {
		case touchId = "Touch ID"
		case faceId = "Face ID"
		case none = "none"
	}
	
	class var shared: BiometricAuth {
		struct Static {
			static let instance: BiometricAuth = BiometricAuth()
		}
		return Static.instance
	}
	
	var type: BioMetricSupported!
	
	override init() {
		super.init()
		type = supportedBiometricType()
	}
	
	func supportedBiometricType () -> BioMetricSupported {
		let context = LAContext()
		//		context.touchIDAuthenticationAllowableReuseDuration = 10;
		var error: NSError?
		
		if context.canEvaluatePolicy(
			LAPolicy.deviceOwnerAuthenticationWithBiometrics,
			error: &error) {
			
			if #available(iOS 11.0, *) {
				if (context.biometryType == LABiometryType.faceID) {
					return BioMetricSupported.faceId
				} else if context.biometryType == LABiometryType.touchID {
					return BioMetricSupported.touchId
				}
			} else {
				return BioMetricSupported.touchId
			}
		}
		return BioMetricSupported.none
	}
	
}
