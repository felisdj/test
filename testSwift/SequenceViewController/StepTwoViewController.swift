//
//  StepTwoViewController.swift
//  testSwift
//
//  Created by JJsobtid on 19/10/2561 BE.
//  Copyright © 2561 JJsobtid. All rights reserved.
//

import UIKit
import Hero

class StepTwoViewController: UIViewController {

	var panGR: UIScreenEdgePanGestureRecognizer!
	var startPoint: CGFloat = 0.0
	var startPan: Bool = true
	
	var nextButton, backButton: UIButton!
	
	var barNow, bar1, bar2: UIView!
	
	init(withIds ids: [String]? = nil) {
		super.init(nibName: nil, bundle: nil)
		
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		view.backgroundColor = .white
		
		navigationController?.hero.navigationAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
		
		panGR = UIScreenEdgePanGestureRecognizer(target: self,
									   action: #selector(handlePan(gestureRecognizer:)))
		panGR.edges = .left
		view.addGestureRecognizer(panGR)
		
		nextButton = UIButton(type: .system)
		nextButton.setTitle("Next", for: .normal)
		backButton = UIButton(type: .system)
		backButton.setTitle("Back", for: .normal)
		
		nextButton.addTarget(self, action: #selector(navigationButton(_:)), for: .touchUpInside)
		backButton.addTarget(self, action: #selector(navigationButton(_:)), for: .touchUpInside)
		
		view.addSubview(backButton)
		view.addSubview(nextButton)
		
		nextButton.snp.makeConstraints { (make) in
			make.top.equalTo(100)
			make.size.equalTo(CGSize(width: 100, height: 20))
			make.trailing.equalTo(view.snp.trailing).offset(-20)
		}
		backButton.snp.makeConstraints { (make) in
			make.top.equalTo(nextButton.snp.top)
			make.size.equalTo(nextButton.snp.size)
			make.leading.equalTo(view.snp.leading).offset(20)
		}
		
		barNow = UIView()
		barNow.backgroundColor = .green
		barNow.layer.cornerRadius = 8
		barNow.hero.id = "BarOne"
		
		bar1 = UIView()
		bar1.backgroundColor = .green
		bar1.alpha = 0.5
		bar1.layer.cornerRadius = 8
		bar1.hero.id = "BarNow"
		
		bar2 = UIView()
		bar2.backgroundColor = .green
		bar2.alpha = 0.5
		bar2.layer.cornerRadius = 8
		bar2.hero.id = "BarTwo"
		
		view.addSubview(bar1)
		view.addSubview(bar2)
		view.addSubview(barNow)
		
		bar1.snp.makeConstraints { (make) in
			make.height.equalTo(16)
			make.trailing.equalTo(barNow.snp.leading).offset(-8)
			make.width.equalTo(22)
			make.bottom.equalTo(view.snp.bottom).offset(-100)
		}
		bar2.snp.makeConstraints { (make) in
			make.height.equalTo(16)
			make.leading.equalTo(barNow.snp.trailing).offset(8)
			make.width.equalTo(22)
			make.bottom.equalTo(view.snp.bottom).offset(-100)
		}
		barNow.snp.makeConstraints { (make) in
			make.height.equalTo(16)
			make.centerX.equalTo(view.snp.centerX)
			make.width.equalTo(46)
			make.bottom.equalTo(view.snp.bottom).offset(-100)
		}
		
		view.layoutIfNeeded()
		
		let circleLoadView = BorderCircleButton(frame: CGRect(x: view.frame.width - 120, y: view.frame.height - 220, width: 100, height: 100))
		view.addSubview(circleLoadView)
		// Do any additional setup after loading the view.
	}
	
	@objc func navigationButton(_ sender: UIButton) {
		if sender == nextButton {
			let vc = StepThreeViewController()
			vc.navigationController?.hero.isEnabled = true
			
			navigationController?.hero.isEnabled = true
			navigationController?.pushViewController(vc, animated: true)
		} else if sender == backButton {
			navigationController?.popViewController(animated: true)
		}
	}
	@objc func handlePan(gestureRecognizer:UIScreenEdgePanGestureRecognizer) {
		let percent = gestureRecognizer.translation(in: gestureRecognizer.view!).x / gestureRecognizer.view!.bounds.size.width
		
		if gestureRecognizer.state == .began {
			navigationController?.popViewController(animated: true)
		} else if gestureRecognizer.state == .changed {
			Hero.shared.update(percent)
		} else if gestureRecognizer.state == .ended {
			if percent > 0.5 && gestureRecognizer.state != .cancelled {
				Hero.shared.finish()
			} else {
				Hero.shared.cancel()
			}
		}
	}

}
