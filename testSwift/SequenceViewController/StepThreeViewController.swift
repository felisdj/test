
//
//  StepThreeViewController.swift
//  testSwift
//
//  Created by JJsobtid on 19/10/2561 BE.
//  Copyright © 2561 JJsobtid. All rights reserved.
//

import UIKit
import Hero

class StepThreeViewController: UIViewController {

	var panGR: UIPanGestureRecognizer!
	var startPoint: CGFloat = 0.0
	var startPan: Bool = true
	
	var nextButton, backButton: UIButton!
	
	var barNow, bar1, bar2: UIView!
	
	init(withIds ids: [String]? = nil) {
		super.init(nibName: nil, bundle: nil)
		
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		view.backgroundColor = .white
		
		navigationController?.hero.navigationAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
		
		panGR = UIPanGestureRecognizer(target: self,
									   action: #selector(handlePan(gestureRecognizer:)))
		view.addGestureRecognizer(panGR)
		
		nextButton = UIButton(type: .system)
		nextButton.setTitle("Next", for: .normal)
		backButton = UIButton(type: .system)
		backButton.setTitle("Back", for: .normal)
		
		nextButton.addTarget(self, action: #selector(navigationButton(_:)), for: .touchUpInside)
		backButton.addTarget(self, action: #selector(navigationButton(_:)), for: .touchUpInside)
		
		view.addSubview(backButton)
		view.addSubview(nextButton)
		
		nextButton.snp.makeConstraints { (make) in
			make.top.equalTo(100)
			make.size.equalTo(CGSize(width: 100, height: 20))
			make.trailing.equalTo(view.snp.trailing).offset(-20)
		}
		backButton.snp.makeConstraints { (make) in
			make.top.equalTo(nextButton.snp.top)
			make.size.equalTo(nextButton.snp.size)
			make.leading.equalTo(view.snp.leading).offset(20)
		}
		
		barNow = UIView()
		barNow.backgroundColor = .green
		barNow.layer.cornerRadius = 8
		barNow.hero.id = "BarTwo"
		
		bar1 = UIView()
		bar1.backgroundColor = .green
		bar1.alpha = 0.5
		bar1.layer.cornerRadius = 8
		bar1.hero.id = "BarNow"
		
		bar2 = UIView()
		bar2.backgroundColor = .green
		bar2.alpha = 0.5
		bar2.layer.cornerRadius = 8
		bar2.hero.id = "BarOne"
		
		view.addSubview(bar1)
		view.addSubview(bar2)
		view.addSubview(barNow)
		
		bar1.snp.makeConstraints { (make) in
			make.height.equalTo(16)
			make.trailing.equalTo(bar2.snp.leading).offset(-8)
			make.width.equalTo(22)
			make.bottom.equalTo(view.snp.bottom).offset(-100)
		}
		bar2.snp.makeConstraints { (make) in
			make.height.equalTo(16)
			make.trailing.equalTo(barNow.snp.leading).offset(-8)
			make.width.equalTo(22)
			make.bottom.equalTo(view.snp.bottom).offset(-100)
		}
		barNow.snp.makeConstraints { (make) in
			make.height.equalTo(16)
			make.trailing.equalTo(view.snp.centerX).offset(53)
			make.width.equalTo(46)
			make.bottom.equalTo(view.snp.bottom).offset(-100)
		}
		
		view.layoutIfNeeded()
		// Do any additional setup after loading the view.
	}
	
	@objc func navigationButton(_ sender: UIButton) {
		if sender == nextButton {
			let vc = StepTwoViewController()
			vc.navigationController?.hero.isEnabled = true
			
			navigationController?.hero.isEnabled = true
			navigationController?.pushViewController(vc, animated: true)
		} else if sender == backButton {
			navigationController?.popViewController(animated: true)
		}
	}
	@objc func handlePan(gestureRecognizer:UIPanGestureRecognizer) {
		let translation = panGR.translation(in: view)
		if startPan {
			startPoint = panGR.location(in: view).x
			//			print(startPoint)
			startPan = false
		}
		if startPoint <= 35.0 {
			switch panGR.state {
			case .began:
				//				if panGR.location(in: view).x > 20.0 {
				navigationController?.popViewController(animated: true)
				//					dismiss(animated: true, completion: nil)
			//				}
			case .changed:
				Hero.shared.update(translation.x / view.bounds.width)
			default:
				let velocity = panGR.velocity(in: view)
				if ((translation.x + velocity.x) / view.bounds.width) > 0.5 {
					Hero.shared.finish()
					//					startPan = true
				} else {
					Hero.shared.cancel()
					startPan = true
				}
			}
		} else {
			startPan = true
		}
	}

}
