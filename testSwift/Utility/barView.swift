//
//  barView.swift
//  testSwift
//
//  Created by JJsobtid on 11/1/2561 BE.
//  Copyright © 2561 JJsobtid. All rights reserved.
//

import UIKit

class barView: UIView {

	var bar, percentBar: CAShapeLayer!
	var barGradient: CAGradientLayer!
	var percentLabel: UILabel!
	let fromColor = [UIColor.weakness_d, UIColor.weakness_c, UIColor.weakness_b, UIColor.weakness_a]
	let toColor = [UIColor.weakness_d_s, UIColor.weakness_c_s, UIColor.weakness_b_s, UIColor.weakness_a_s]
	var first: Bool = true
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		bar = createBar(.disablebutton, w: bounds.width, half: false)
		layer.addSublayer(bar)
		
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func animationLine(toPercent percent: CGFloat) {
		let pathAnimate = CABasicAnimation(keyPath: "strokeEnd")
		pathAnimate.duration = 1.0
		pathAnimate.fromValue = 0.0
		pathAnimate.toValue = percent
		pathAnimate.fillMode = CAMediaTimingFillMode.forwards
		pathAnimate.isCumulative = false
		pathAnimate.isAdditive = false
		pathAnimate.isRemovedOnCompletion = false
		
		percentBar.add(pathAnimate, forKey: "strokeEnd")
		addSubview(percentLabel)
		
		UIView.animate(withDuration: 0.5, delay: 1.0, options: UIView.AnimationOptions(rawValue: 0), animations: {
			self.percentLabel.alpha = 1
		}, completion: nil)
	}
	func animationDidStop(animate: CAAnimation, finished: Bool) {
		if finished {
			UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions(rawValue: 0), animations: {
				self.percentLabel.alpha = 1
			}, completion: nil)
		}
	}
	
	func createGradientLayer(_ percent: CGFloat) {
		let mainColor = fromColor[Int(floor(percent*4))]
		let secondColor = toColor[Int(floor(percent*4))]
		
		let colors = [mainColor.cgColor, secondColor.cgColor]
		
		barGradient = CAGradientLayer()
		barGradient.frame = CGRect(x: -16, y: 0, width: bounds.width + 32, height: bounds.height)
		barGradient.colors = colors
		barGradient.startPoint = CGPoint(x: 1, y: 0.5)
		barGradient.endPoint = CGPoint(x: 0, y: 0.5)
		barGradient.locations = [0, 1]
		
	}
	func createBar(_ color: UIColor, w: CGFloat, half: Bool) -> CAShapeLayer {
		
		let pathBar = UIBezierPath()
		pathBar.move(to: CGPoint(x: 0, y: frame.height/2))
		pathBar.addLine(to: CGPoint(x: w, y: frame.height/2))
		pathBar.close()
		
		let tempBar = CAShapeLayer()
		tempBar.frame = bounds
		tempBar.path = pathBar.cgPath
		tempBar.lineWidth = tempBar.frame.height
		tempBar.fillColor = nil
		tempBar.lineJoin = CAShapeLayerLineJoin.round
		tempBar.lineCap = CAShapeLayerLineCap.round
		tempBar.strokeColor = color.cgColor
		tempBar.strokeStart = 0.0
		
		if half {
			tempBar.fillColor = UIColor.clear.cgColor
			tempBar.strokeColor = UIColor.black.cgColor
			tempBar.strokeEnd = 0.0
		}
		
		return tempBar
	}

	func updateData(_ percent: CGFloat) {
		var per: Double = 0
		if percent > 0 {
			per = Double(floor((percent-0.01)*4))
		}
		
		if percentBar != nil {
			percentBar.removeFromSuperlayer()
			barGradient.removeFromSuperlayer()
			percentLabel.removeFromSuperview()
		}

		percentBar = createBar(fromColor[Int(per)], w: bounds.width * percent, half: true)
		percentBar.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
		createGradientLayer(percent)
		
		barGradient.mask = percentBar
		layer.addSublayer(barGradient)
		
		percentLabel = UILabel(frame: CGRect(x: 0, y: 0, width: bounds.width * percent, height: bounds.height))
		percentLabel.textColor = .white
		percentLabel.numberOfLines = 1
		percentLabel.text = "\(percent*100) %"
		percentLabel.textAlignment = .right
		percentLabel.alpha = 0
		if percent == 0 {
			percentLabel.alpha = 1
		}
		if first {
			first = false
			animationLine(toPercent: percent)
		}
	}
	
}
