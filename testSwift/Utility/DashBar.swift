//
//  DashBar.swift
//  testSwift
//
//  Created by JJsobtid on 20/9/2561 BE.
//  Copyright © 2561 JJsobtid. All rights reserved.
//

import UIKit

class DashBar: UIView {

	var bar, percentBar: CAShapeLayer!
	var barGradient: CAGradientLayer!
	var percentLabel: UILabel!
	let fromColor = [UIColor.weakness_d, UIColor.weakness_c, UIColor.weakness_b, UIColor.weakness_a]
	let toColor = [UIColor.weakness_d_s, UIColor.weakness_c_s, UIColor.weakness_b_s, UIColor.weakness_a_s]
	var first: Bool = true
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		layer.borderWidth = 1.0
		layer.borderColor = UIColor.brandcolor1.cgColor
		
		let pathBar = UIBezierPath()
		pathBar.move(to: CGPoint(x: 0, y: frame.height/2))
		pathBar.addLine(to: CGPoint(x: frame.width, y: frame.height/2))
		
		let nTicks = 51
		
		let testWidth = Double(frame.width)/101
		// thickness of short ticks
		let tickWidth = Double(frame.width)/101//0.99
		
		// calculate the gap between ticks
		let gapWidth = (Double(frame.width) - (tickWidth * Double(nTicks))) / Double(nTicks - 1)
		
		print("tick \(tickWidth) gap \(gapWidth) \(testWidth)")
		bar = CAShapeLayer()
		bar.frame = bounds
		bar.path = pathBar.cgPath
		bar.lineWidth = frame.height
		bar.fillColor = nil
//		tempBar.lineJoin = kCALineJoinRound
//		tempBar.lineCap = kCALineCapRound
		bar.strokeColor = UIColor.clear.cgColor//UIColor.disablebutton.cgColor
		bar.strokeStart = 0.0
		bar.lineDashPattern = [testWidth as NSNumber,testWidth as NSNumber]
		
		layer.addSublayer(bar)
		print("\(frame) \(bounds)")

		percentBar = CAShapeLayer()
		percentBar.frame = bounds
		percentBar.path = pathBar.cgPath
		percentBar.lineWidth = frame.height
//		percentBar.fillColor = nil
		
//		percentBar.strokeColor = UIColor.disablebutton.cgColor
		percentBar.strokeStart = 0.0
		percentBar.lineDashPattern = [testWidth as NSNumber, testWidth as NSNumber]
		percentBar.fillColor = UIColor.clear.cgColor
		percentBar.strokeColor = UIColor.black.cgColor
		percentBar.strokeEnd = 0.0
		layer.addSublayer(percentBar)
		
		pathBar.close()
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func animationLine(toPercent percent: CGFloat) {
		let pathAnimate = CABasicAnimation(keyPath: "strokeEnd")
		pathAnimate.duration = 0.75
		pathAnimate.fromValue = 0.0
		pathAnimate.toValue = percent
		pathAnimate.fillMode = CAMediaTimingFillMode.forwards
		pathAnimate.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
		pathAnimate.isCumulative = false
		pathAnimate.isAdditive = false
		pathAnimate.isRemovedOnCompletion = false
		
		percentBar.add(pathAnimate, forKey: "strokeEnd")
		
		addSubview(percentLabel)
		
		UIView.animate(withDuration: 0.5, delay: 1.0, options: UIView.AnimationOptions(rawValue: 0), animations: {
			self.percentLabel.alpha = 1
		}, completion: nil)
	}
	func animationDidStop(animate: CAAnimation, finished: Bool) {
		if finished {
			UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions(rawValue: 0), animations: {
				self.percentLabel.alpha = 1
			}, completion: nil)
		}
	}
	
	func createGradientLayer(_ percent: CGFloat) {
		let mainColor = fromColor[Int(floor(percent*4))]
		let secondColor = toColor[Int(floor(percent*4))]
		
		let colors = [UIColor.weakness_d, UIColor.weakness_c, UIColor.weakness_b, UIColor.weakness_a].reversed().map { (color) -> CGColor in
			return color.cgColor
		} //[mainColor.cgColor, secondColor.cgColor]
		
		barGradient = CAGradientLayer()
		barGradient.frame = bounds
		barGradient.colors = colors
		barGradient.startPoint = CGPoint(x: 1, y: 0)
		barGradient.endPoint = CGPoint(x: 0, y: 0)
		barGradient.locations = [0, 0.33, 0.67, 1]
		
	}
	func createBar(_ color: UIColor, w: CGFloat, half: Bool) -> CAShapeLayer {
		
		let pathBar = UIBezierPath()
		pathBar.move(to: CGPoint(x: 0, y: 0))
		pathBar.addLine(to: CGPoint(x: w, y: 0))
		pathBar.close()
		
		let tempBar = CAShapeLayer()
		tempBar.frame = bounds
		tempBar.path = pathBar.cgPath
		tempBar.lineWidth = tempBar.frame.height
		tempBar.fillColor = nil
		//		tempBar.lineJoin = kCALineJoinRound
		//		tempBar.lineCap = kCALineCapRound
		tempBar.strokeColor = color.cgColor
		tempBar.strokeStart = 0.0
		print(w)
		tempBar.lineDashPattern = [1.96, 1.96]
		if half {
			tempBar.fillColor = UIColor.clear.cgColor
			tempBar.strokeColor = UIColor.black.cgColor
			tempBar.strokeEnd = 0.0
		}
		
		return tempBar
	}
	
	func updateData(_ percent: CGFloat) {
		var per: Double = 0
		if percent > 0 {
			per = Double(floor((percent-0.01)*4))
		}
		
		if barGradient != nil {
//			percentBar.removeFromSuperlayer()
			barGradient.removeFromSuperlayer()
			percentLabel.removeFromSuperview()
		}
		
//		percentBar = createBar(fromColor[Int(per)], w: bounds.width, half: true)
//		percentBar.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
		createGradientLayer(percent)
		
		barGradient.mask = percentBar
		layer.addSublayer(barGradient)
		
		percentLabel = UILabel(frame: CGRect(x: 0, y: 0, width: bounds.width * percent, height: bounds.height))
		percentLabel.textColor = percent < 0.6 ? .black : .black
		percentLabel.numberOfLines = 1
		percentLabel.text = "\(String(format: "%.2f %", percent*100)) %"
		percentLabel.textAlignment = .right
		percentLabel.alpha = 0
		if percent == 0 {
			percentLabel.alpha = 1
		}
		if first {
			first = false
			animationLine(toPercent: percent)
		}
	}

}
