//
//  CollectionPageFlowLayout.swift
//  testSwift
//
//  Created by JJsobtid on 9/5/2561 BE.
//  Copyright © 2561 JJsobtid. All rights reserved.
//

import UIKit

class CollectionPageFlowLayout: UICollectionViewFlowLayout {

	var innerSpace: CGFloat = 16
	var numberOfColumn: CGFloat = 1.0
	var cellHeight: CGFloat = 91
	var typeHeight: String!
	
	init(withcol col: CGFloat, height: CGFloat, axis: UICollectionView.ScrollDirection = .horizontal, innerLine: CGFloat = 16, innerInteritem: CGFloat = 16) {
		super.init()
		innerSpace = axis == .vertical ? innerInteritem : innerLine
		minimumLineSpacing = innerLine
		minimumInteritemSpacing = innerInteritem
		cellHeight = height
		numberOfColumn = col + 0.0001
		scrollDirection = axis
		typeHeight = "fixUser"
	}
	
	func cellWidth() -> CGFloat {
		return (collectionView!.frame.size.width/numberOfColumn) - (1/numberOfColumn+1)*innerSpace
	}
	
	override var itemSize: CGSize {
		set {
			self.itemSize = CGSize(width: cellWidth(), height: (typeHeight == "fixUser" ?  cellHeight : cellWidth() + 21))
		}
		get {
			return CGSize(width: cellWidth(), height:(typeHeight == "fixUser" ?  cellHeight : cellWidth() + 21))
		}
	}
	override var collectionViewContentSize : CGSize {
		// Only support single section for now.
		// Only support Horizontal scroll
		let count = self.collectionView?.dataSource?.collectionView(self.collectionView!, numberOfItemsInSection: 0)
		let canvasSize = self.collectionView!.frame.size
		var contentSize = canvasSize
		if self.scrollDirection == UICollectionView.ScrollDirection.horizontal {
			let rowCount = Int((canvasSize.height - self.itemSize.height) / (self.itemSize.height + self.minimumInteritemSpacing) + 1)
			let columnCount = Int((canvasSize.width - self.itemSize.width) / (self.itemSize.width + self.minimumLineSpacing) + 1)
			let page = ceilf(Float(count!) / Float(rowCount * columnCount))
			contentSize.width = CGFloat(page) * canvasSize.width
		}
		return contentSize
	}
	
	func frameForItemAtIndexPath(_ indexPath: IndexPath) -> CGRect {
		let canvasSize = self.collectionView!.frame.size
		let rowCount = Int((canvasSize.height - self.itemSize.height) / (self.itemSize.height + self.minimumInteritemSpacing) + 1)
		let columnCount = Int((canvasSize.width - self.itemSize.width) / (self.itemSize.width + self.minimumLineSpacing) + 1)
		
		let pageMarginX = (canvasSize.width - CGFloat(columnCount) * self.itemSize.width - (columnCount > 1 ? CGFloat(columnCount - 1) * self.minimumLineSpacing : 0)) / 2.0
		let pageMarginY = (canvasSize.height - CGFloat(rowCount) * self.itemSize.height - (rowCount > 1 ? CGFloat(rowCount - 1) * self.minimumInteritemSpacing : 0)) / 2.0
		
		let page = Int(CGFloat(indexPath.row) / CGFloat(rowCount * columnCount))
		let remainder = Int(CGFloat(indexPath.row) - CGFloat(page) * CGFloat(rowCount * columnCount))
		let row = Int(CGFloat(remainder) / CGFloat(columnCount))
		let column = Int(CGFloat(remainder) - CGFloat(row) * CGFloat(columnCount))
		
		var cellFrame = CGRect.zero
		cellFrame.origin.x = pageMarginX + CGFloat(column) * (self.itemSize.width + self.minimumLineSpacing)
		cellFrame.origin.y = pageMarginY + CGFloat(row) * (self.itemSize.height + self.minimumInteritemSpacing)
		cellFrame.size.width = self.itemSize.width
		cellFrame.size.height = self.itemSize.height
		
		if self.scrollDirection == UICollectionView.ScrollDirection.horizontal {
			cellFrame.origin.x += CGFloat(page) * canvasSize.width
		}
		
		return cellFrame
	}
	
	override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
		let attr = super.layoutAttributesForItem(at: indexPath)?.copy() as! UICollectionViewLayoutAttributes?
		attr!.frame = self.frameForItemAtIndexPath(indexPath)
		return attr
	}
	
	override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
		let originAttrs = super.layoutAttributesForElements(in: rect)
		var attrs: [UICollectionViewLayoutAttributes]? = Array<UICollectionViewLayoutAttributes>()
		
		for attr in originAttrs! {
			let idxPath = attr.indexPath
			let itemFrame = self.frameForItemAtIndexPath(idxPath)
			if itemFrame.intersects(rect) {
				let nAttr = self.layoutAttributesForItem(at: idxPath)
				attrs?.append(nAttr!)
			}
		}
		
		return attrs
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
}
