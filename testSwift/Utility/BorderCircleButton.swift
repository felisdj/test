//
//  BorderCircleButton.swift
//  testSwift
//
//  Created by JJsobtid on 18/10/2561 BE.
//  Copyright © 2561 JJsobtid. All rights reserved.
//

import UIKit

class BorderCircleButton: UIButton, CAAnimationDelegate {
	
	var profileImageView, selectImageView: UIImageView!
	var buttonTitleLabel: UILabel!
	var pointView: UIView!
	
	var circleLayer, colorLayer: CAShapeLayer!
	var isCancel: Bool = true
	var isAnimate: Bool = false
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		tintColor = .gray
		
		profileImageView = UIImageView(image: UIImage(named: "maleGov"))
		profileImageView.contentMode = .scaleAspectFill
		profileImageView.layer.masksToBounds = true
		profileImageView.frame = CGRect(x: 0, y: 0, width: bounds.width - 20, height: bounds.width - 20)
		profileImageView.center = CGPoint(x: bounds.width/2, y: bounds.width/2)
		profileImageView.layer.cornerRadius = (bounds.width - 20)/2
		addSubview(profileImageView)
		
		circleLayer = CAShapeLayer()
		circleLayer.frame = bounds
		circleLayer.path = pathForBox().cgPath
		circleLayer.strokeColor = tintColor.cgColor
		circleLayer.lineWidth = 3.0
		circleLayer.fillColor = UIColor.clear.cgColor
		circleLayer.lineCap = CAShapeLayerLineCap.round
		circleLayer.lineJoin = CAShapeLayerLineJoin.round
		circleLayer.strokeStart = 0.0
		circleLayer.strokeEnd = 1.0
		layer.addSublayer(circleLayer)
		
		colorLayer = CAShapeLayer()
		colorLayer.frame = bounds
		colorLayer.path = pathForBox().cgPath
		colorLayer.strokeColor = UIColor.green.cgColor
		colorLayer.lineWidth = 3.0
		colorLayer.fillColor = UIColor.clear.cgColor
		colorLayer.lineCap = CAShapeLayerLineCap.round
		colorLayer.lineJoin = CAShapeLayerLineJoin.round
		colorLayer.strokeStart = 0.0
		colorLayer.strokeEnd = 0.0
		layer.addSublayer(colorLayer)
		
		pointView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
		pointView.center = CGPoint(x: bounds.width/2, y: 0)
		pointView.layer.cornerRadius = 10
		pointView.backgroundColor = .green
		
		addSubview(pointView)
		pointView.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
		
		addTarget(self, action: #selector(touchDown), for: .touchDown)
		addTarget(self, action: #selector(touchDown), for: .touchDragEnter)
		addTarget(self, action: #selector(touchUp), for: .touchUpInside)
		addTarget(self, action: #selector(animationZoom), for: .touchDragExit)
	}
	
	@objc func touchDown() {
		unselected()
		UIView.animate(withDuration: 0.25) {
			self.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
		}
	}
	@objc func animationZoom() {
		UIView.animate(withDuration: 0.25) {
			print("exit.")
			self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
		}
	}
	@objc func touchUp() {
		animationZoom()
		selected()
	}
	
	@objc func selected() {
		isCancel = false
		if !isAnimate {
			isAnimate = true
			pointView.isHidden = false
			pointView.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
			let fadeAnimate = CABasicAnimation.init(keyPath: "strokeEnd")
			fadeAnimate.fromValue = 0.0
			fadeAnimate.toValue = 1.0
			fadeAnimate.duration = 0.5
			fadeAnimate.delegate = self
			fadeAnimate.fillMode = CAMediaTimingFillMode.forwards
			fadeAnimate.timingFunction = CAMediaTimingFunction.easeInOut
			fadeAnimate.isRemovedOnCompletion = false
			colorLayer.add(fadeAnimate, forKey: "strokeEnd")
			DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
				if self.isAnimate {
					UIView.animate(withDuration: 0.2, animations: {
						self.pointView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
					}) { _ in
						UIView.animate(withDuration: 0.05, animations: {
							self.pointView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
						}, completion: { _ in
							self.isAnimate = false
						})
					}
				}
			})
		}
	}
	@objc func unselected() {
		isCancel = true
		colorLayer.removeAllAnimations()
		pointView.isHidden = true
		pointView.transform = CGAffineTransform(scaleX: 0.001, y: 0.001)
		transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
	}
	
	func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
		if flag {
			print("animation stop.")
		} else {
			print("Cancel")
			isAnimate = flag
		}
		
	}
	
	
	func pathForBox() -> UIBezierPath {
		let radius = bounds.width/2
		
		let startAngle: CGFloat = 3 * .pi / 2
		let endAngle: CGFloat = (3 * .pi / 2) + (2 * .pi)
		
		let pathDraw = UIBezierPath(arcCenter: CGPoint(x: bounds.width/2, y: bounds.width/2), radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
		
		return pathDraw
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
