//
//  TableView.swift
//  testSwift
//
//  Created by JJsobtid on 26/3/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

class TableView<T, Cell: UITableViewCell>: UITableView, UITableViewDelegate, UITableViewDataSource {

	var items: [T]! {
		didSet {
			reloadData()
		}
	}
	var configure: (Cell, T) -> Void
	var selectHandler: (T) -> Void
	
	init(items: [T], configure: @escaping (Cell, T) -> Void, selectHandler: @escaping (T) -> Void) {
		self.items = items
		self.configure = configure
		self.selectHandler = selectHandler
		super.init(frame: CGRect(), style: .plain)
		self.delegate = self
		self.dataSource = self
		register(Cell.self, forCellReuseIdentifier: "reuse")
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return items.count
	}
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "reuse", for: indexPath) as! Cell
		
		configure(cell, items[indexPath.item])
		
		return cell
	}
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		let data = items[indexPath.item]
		selectHandler(data)
	}
	
}
