//
//  SizeSetting.swift
//  testSwift
//
//  Created by JJsobtid on 7/8/2561 BE.
//  Copyright © 2561 JJsobtid. All rights reserved.
//
import Foundation
import UIKit

var VERTICAL: Bool = UIDevice.current.orientation.isPortrait

func SCREEN_WIDTH() -> CGFloat { return UIScreen.main.bounds.size.width }
func SCREEN_HEIGHT() -> CGFloat { return UIScreen.main.bounds.size.height }

let MARGIN: CGFloat = 16

var hasNotch = UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
var topArea: CGFloat {
	return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0
}
