//
//  CustomViewController.swift
//  testSwift
//
//  Created by JJsobtid on 9/7/2561 BE.
//  Copyright © 2561 JJsobtid. All rights reserved.
//

import UIKit
import AVFoundation
import Hero
import RxSwift
import RxCocoa

class CustomViewController: UIViewController {

	let dismissButton = UIButton(type: .system)
	let navView = UIView()
	var panGR: UIPanGestureRecognizer!
	var startPoint: CGFloat = 0.0
	var startPan: Bool = true
	
	var avPlayer: AVPlayer!
	var playerLayer: AVPlayerLayer!
	var avPlay, avEnded: Bool!
	
	let pushButton = UIButton(type: .system)
	
	var someStr: String
	var isPresent: Bool = true {
		didSet {
			
		}
	}
	var id: String! {
		didSet {
			navView.hero.id = id
		}
	}
	
	init(withsome some: String) {
		someStr = some
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		view.backgroundColor = .white
//		view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTap)))
		
		navView.backgroundColor = .blue
//		navView.hero.id = "TestNav"
		navView.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH(), height: 64)
		view.addSubview(navView)
		navView.snp.makeConstraints { (make) in
			make.top.equalTo(0)
			make.leading.equalTo(view.snp.leading)
			make.height.equalTo(64)
			make.trailing.equalTo(view.snp.trailing)
		}
		
		dismissButton.setTitle("Back", for: .normal)
		dismissButton.addTarget(self, action: #selector(back), for: .touchUpInside)
		dismissButton.hero.id = "back button"
		view.addSubview(dismissButton)
		dismissButton.sizeToFit()
		dismissButton.snp.makeConstraints { (make) in
			make.leading.equalTo(view.snp.leading).offset(30)
			make.top.equalTo(view.snp.top).offset(50)
			make.size.equalTo(dismissButton.frame.size)
		}
		
		navigationController?.hero.navigationAnimationType = .selectBy(presenting: .zoom, dismissing: .zoomOut)

		panGR = UIPanGestureRecognizer(target: self,
									   action: #selector(handlePan(gestureRecognizer:)))
		view.addGestureRecognizer(panGR)
		
		let videoURL = URL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
		avPlayer = AVPlayer(url: videoURL!)
		playerLayer = AVPlayerLayer(player: avPlayer)
		playerLayer.frame = CGRect(x: 16, y: 72, width: SCREEN_WIDTH() - 32, height: (SCREEN_WIDTH() - 32)/2)
		self.view.layer.addSublayer(playerLayer)
	
		NotificationCenter.default.addObserver(self, selector: #selector(videoEnding), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: avPlayer.currentItem)

		avPlay = true
		avEnded = false
		avPlayer.play()
		
		let tapVideoGesture = UITapGestureRecognizer(target: self, action: #selector(touchOnVideo(gestureRecognizer:)))
		let tapVideoView = UIView(frame: playerLayer.frame)
		tapVideoView.addGestureRecognizer(tapVideoGesture)
		
		view.addSubview(tapVideoView)
		
		pushButton.setTitle(someStr, for: .normal)
		pushButton.addTarget(self, action: #selector(pushView), for: .touchUpInside)
		view.addSubview(pushButton)
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
//		tabBarController?.tabBar.isHidden = false
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		if !isPresent {
//			navigationController?.isNavigationBarHidden = true
//			navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
//			navigationController?.interactivePopGestureRecognizer?.isEnabled = true
		}
	}
	
	@objc func videoEnding() {
		avEnded = true
	}
	
	@objc func handlePan(gestureRecognizer:UIPanGestureRecognizer) {
//		switch panGR.state {
//		case .began:
//			// begin the transition as normal
//			navigationController?.popViewController(animated: true)
//		//			dismiss(animated: true, completion: nil)
//		case .changed:
//			// calculate the progress based on how far the user moved
//			let translation = panGR.translation(in: nil)
//			let progress = translation.x / 2 / view.bounds.width
//			Hero.shared.update(progress)
//		default:
//			// end the transition when user ended their touch
//			Hero.shared.finish()
//		}
		let translation = panGR.translation(in: view)
		if startPan {
			startPoint = panGR.location(in: view).x
//			print(startPoint)
			startPan = false
		}
		if startPoint <= 35.0 {
			switch panGR.state {
			case .began:
//				if panGR.location(in: view).x > 20.0 {
					navigationController?.popViewController(animated: true)
//					dismiss(animated: true, completion: nil)
//				}
			case .changed:
				Hero.shared.update(translation.x / view.bounds.width)
			default:
				let velocity = panGR.velocity(in: view)
				if ((translation.x + velocity.x) / view.bounds.width) > 0.5 {
					Hero.shared.finish()
//					startPan = true
				} else {
					Hero.shared.cancel()
					startPan = true
				}
			}
		} else {
			startPan = true
		}
	}
	
	
	@objc func touchOnVideo(gestureRecognizer: UITapGestureRecognizer) {
		if avEnded {
			avPlayer.seek(to: CMTime(seconds: 0.0, preferredTimescale: CMTimeScale(1.0)))
			avPlayer.play()
			avPlay = true
			avEnded = false
		} else {
			if avPlay {
				avPlayer.pause()
			} else {
				avPlayer.play()
			}
			avPlay = !avPlay
		}
	}
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
//		dismissButton.sizeToFit()
//		dismissButton.center = CGPoint(x: 30, y: 50)
		
		pushButton.sizeToFit()
		pushButton.center = view.center
		
		playerLayer.frame = CGRect(x: 16, y: 72, width: SCREEN_WIDTH() - 32, height: (SCREEN_WIDTH() - 32)/2)
	}
	
	@objc func back() {
		if isPresent {
			dismiss(animated: true, completion: nil)
		} else {
			navigationController?.popViewController(animated: true)
		}
	}
	
	@objc func onTap() {
		back() // default action is back on tap
	}
	
	deinit {
		print("deinit.")
	}
	
	@objc func pushView() {
		let vc = CustomViewController(withsome: "test2")
		vc.id = id
		vc.isPresent = false
		vc.navigationController?.hero.isEnabled = true

		navigationController?.hero.isEnabled = true
		navigationController?.pushViewController(vc, animated: true)
	}
}
