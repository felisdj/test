//
//  Utility.swift
//  testSwift
//
//  Created by JJsobtid on 17/10/2560 BE.
//  Copyright © 2560 JJsobtid. All rights reserved.
//

import UIKit
import Foundation

class Utility {
	
	func getDeviceModel() -> String {
		return UIDevice.current.model
	}
	
	func getDeviceOS() -> String {
		return "iOS \(UIDevice.current.systemVersion)"
	}
	
	func getDeviceOSModel() -> String {
		return "\(UIDevice.current.systemName) \(UIDevice.current.systemVersion)"
	}
	
	func getAppVersion() -> String {
		return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
	}
	
	func getCurrentViewController() -> UIViewController {
		let getWindow : UIWindow = UIApplication.shared.keyWindow!
		let topController = getWindow.visibleViewController()
		print(topController)
		return topController!
	}
	
	func isiPad() -> Bool {
		if UIDevice.current.userInterfaceIdiom == .pad {
			return true
		}
		return false
	}
	
	static func commaNumber(fromString num: String) -> String {
		let componentNumber = num.split(separator: ".")
		if num == "" {
			return "0.00"
		} else if componentNumber.count > 1 {
			var deci = commaNumber(Float(componentNumber[0])!).split(separator: ".")
			let point = componentNumber[1]
			var newPoint = ""
			if point.count == 1 {
				newPoint = "\(String(point))0"
			} else if point.count == 0 {
				newPoint = "00"
			} else {
				newPoint = String(point)
			}
			return "\(String(deci[0])).\(String(newPoint))"
		} else {
			return commaNumber(roundf(Float(num)!))
		}
	}
	
	static func commaNumber(_ num: Float, minDigits: Int = 2, maxDigits: Int = 2) -> String {
		let numberFormatter = NumberFormatter()
		numberFormatter.locale = Locale(identifier: "th_TH")
		numberFormatter.numberStyle = NumberFormatter.Style.decimal
		numberFormatter.maximumFractionDigits = maxDigits
		numberFormatter.minimumFractionDigits = minDigits
		return numberFormatter.string(from: NSNumber(value: num))!
	}
}
