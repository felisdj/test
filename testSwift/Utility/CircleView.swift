//
//  CircleView.swift
//  testSwift
//
//  Created by JJsobtid on 11/1/2561 BE.
//  Copyright © 2561 JJsobtid. All rights reserved.
//

import UIKit
import QuartzCore

enum CheckmarkType {
	case none
	case correct
	case error
}

class CircleView: UIView, CAAnimationDelegate {

	var circleLayer: CAShapeLayer!
	var checkLayer: CAShapeLayer!
	var errorLayer: CAShapeLayer?
	var typeCheck: CheckmarkType!
	let gradient = CAGradientLayer()
	
	init(withColor color: UIColor, type: CheckmarkType, frame: CGRect) {
		super.init(frame: frame)
		typeCheck = type
		tintColor = color
		drawCircle()
		drawLayer()
	}
	
	init(withColor color: UIColor, frame: CGRect) {
		super.init(frame: frame)
		typeCheck = .correct
		tintColor = color
		drawCircle()
		drawLayer()
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func animation() {
		
		let fadeAnimate = CABasicAnimation.init(keyPath: "strokeEnd")
		fadeAnimate.fromValue = 0.0
		fadeAnimate.toValue = 1.0
		fadeAnimate.duration = 1.0
		fadeAnimate.delegate = self
		fadeAnimate.fillMode = CAMediaTimingFillMode.forwards
		fadeAnimate.isRemovedOnCompletion = false
		circleLayer.add(fadeAnimate, forKey: "strokeEnd")
		if checkLayer != nil {
			checkLayer!.add(fadeAnimate, forKey: "strokeEnd")
		} else if errorLayer != nil {
			errorLayer!.add(fadeAnimate, forKey: "strokeEnd")
		}
	}
	
	func drawCircle() {
		if circleLayer != nil {
			circleLayer.removeFromSuperlayer()
		}
		circleLayer = CAShapeLayer()
		circleLayer.frame = bounds
		circleLayer.path = pathForBox().cgPath
		circleLayer.strokeColor = tintColor.cgColor
		circleLayer.lineWidth = 3.0
		circleLayer.fillColor = UIColor.clear.cgColor
		circleLayer.lineCap = CAShapeLayerLineCap.round
		circleLayer.lineJoin = CAShapeLayerLineJoin.round
		circleLayer.strokeStart = 0.0
		circleLayer.strokeEnd = 0.0
		layer.addSublayer(circleLayer)
	}
	
	func drawLayer() {
		if checkLayer != nil {
			checkLayer.removeFromSuperlayer()
		}
		checkLayer = CAShapeLayer()
		checkLayer.frame = bounds
		if typeCheck == .correct {
			checkLayer.path = pathForCheckMark().cgPath
		} else if typeCheck == .error {
			checkLayer.path = pathForErrorMark().cgPath
		}
		checkLayer.strokeColor = tintColor.cgColor
		checkLayer.lineWidth = 3.0
		checkLayer.fillColor = UIColor.clear.cgColor
		checkLayer.lineCap = CAShapeLayerLineCap.round
		checkLayer.lineJoin = CAShapeLayerLineJoin.round
		checkLayer.strokeStart = 0.0
		checkLayer.strokeEnd = 0.0
		layer.addSublayer(checkLayer)
	}
	
	func pathForBox() -> UIBezierPath {
		let radius = bounds.width/2
		
		let startAngle: CGFloat = 0
		let endAngle: CGFloat = startAngle + (2 * .pi)
		
		let pathDraw = UIBezierPath(arcCenter: center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
	
		return pathDraw
	}
	
	func pathForCheckMark() -> UIBezierPath {
		let checkMarkPath = UIBezierPath()
		checkMarkPath.move(to: CGPoint(x: bounds.width/3.1578, y: bounds.height/2))
		checkMarkPath.addLine(to: CGPoint(x: bounds.width/2.0618, y: bounds.height/1.57894))
		checkMarkPath.addLine(to: CGPoint(x: bounds.width/1.3953, y: bounds.height/2.7272))
		return checkMarkPath
	}
	
	func pathForErrorMark() -> UIBezierPath {
		let checkMarkPath = UIBezierPath()
		checkMarkPath.move(to: CGPoint(x: bounds.width*0.7477, y: bounds.height*0.6944))
		checkMarkPath.addLine(to: CGPoint(x: bounds.width*0.2673, y: bounds.height*0.32))
		checkMarkPath.move(to: CGPoint(x: bounds.width*0.7477, y: bounds.height*0.32))
		checkMarkPath.addLine(to: CGPoint(x: bounds.width*0.2673, y: bounds.height*0.6944))
		return checkMarkPath
	}
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
