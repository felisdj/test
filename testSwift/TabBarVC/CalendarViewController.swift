//
//  CalendarViewController.swift
//  testSwift
//
//  Created by JJsobtid on 15/11/2560 BE.
//  Copyright © 2560 JJsobtid. All rights reserved.
//

import UIKit
import FSCalendar

class CalendarViewController: UIViewController, FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance, BannerCollectionDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

	var titleForPage: String? { return "calendar" }
	var calendar: FSCalendar!
	var bannerView: CustomBannerCollectionView!
	var docCollectionView, collectionView: UICollectionView!
	var prevWeekButton, nextWeekButton: UIButton!
	var nowDate: Date!
	@IBOutlet var titleLabel: UILabel!
	
	override func viewDidLoad() {
        super.viewDidLoad()
		self.titleLabel.text? = "HELLO!"

		calendar = FSCalendar(frame: CGRect(x: 60, y: 0, width: 0, height: 300))
		calendar.backgroundColor = .white
		calendar.dataSource = self
		calendar.delegate = self
		calendar.pagingEnabled = true
		calendar.allowsSelection = true
		calendar.allowsMultipleSelection = false
		calendar.firstWeekday = 1
		calendar.placeholderType = .none
//		calendar.appearance.caseOptions = [.weekdayUsesSingleUpperCase, .headerUsesUpperCase]
		
		calendar.scope = .week
		calendar.scrollDirection = .horizontal
//		calendar.appearance.headerTitleFont = .kanitReg16
//		calendar.appearance.titleFont = .kanitLight14
//		calendar.appearance.weekdayFont = .kanitReg14
		calendar.appearance.headerTitleColor = .brandcolor1
		calendar.appearance.weekdayTextColor = .brandcolor2
		calendar.appearance.titleDefaultColor = .Celllable
		calendar.appearance.titlePlaceholderColor = .Cellinformation
		calendar.appearance.titleTodayColor = .Celllable
		calendar.appearance.todayColor = .clear
		calendar.appearance.selectionColor = .brandcolor2
		calendar.appearance.titleSelectionColor = .white
		
		view.addSubview(calendar)
		calendar.snp.makeConstraints { (make) in
			make.top.equalTo(view.snp.top).offset(60)
			make.leading.equalTo(view.snp.leading)
			make.trailing.equalTo(view.snp.trailing)
			make.height.equalTo(300)
		}
		calendar.select(Date(), scrollToDate: false)
		
		let layout = UICollectionViewFlowLayout()
		bannerView = CustomBannerCollectionView(frame: CGRect(), collectionViewLayout: layout)
		bannerView.pageDelegate = self
		view.addSubview(bannerView)
		bannerView.snp.makeConstraints { (make) in
			make.bottom.equalTo(view.snp.bottom).offset(-50)
			make.leading.equalTo(view.snp.leading)
			make.trailing.equalTo(view.snp.trailing)
			make.height.equalTo(200)
		}
		bannerView.imageURLs = ["https://cdn-images-1.medium.com/max/1600/1*oIthnXs1N7CJbqViQ2foUQ.png","https://cdn-images-1.medium.com/max/800/1*kH_bbdel9fszGy0AW2ab_g.png"]
		
		prevWeekButton = UIButton(frame: CGRect(x: 50, y: 50, width: 100, height: 30))
		prevWeekButton.setTitle("prev", for: .normal)
		prevWeekButton.backgroundColor = .black
		prevWeekButton.addTarget(self, action: #selector(moveWeek(_:)), for: .touchUpInside)
		nextWeekButton = UIButton(frame: CGRect(x: 50, y: 80, width: 100, height: 30))
		nextWeekButton.backgroundColor = .black
		nextWeekButton.setTitle("next", for: .normal)
		nextWeekButton.addTarget(self, action: #selector(moveWeek(_:)), for: .touchUpInside)
		view.addSubview(prevWeekButton)
		view.addSubview(nextWeekButton)
		prevWeekButton.snp.makeConstraints { (make) in
			make.centerX.equalTo(view.snp.centerX)
			make.centerY.equalTo(view.snp.centerY).offset(40)
			make.size.equalTo(CGSize(width: 100, height: 30))
		}
		nextWeekButton.snp.makeConstraints { (make) in
			make.edges.equalTo(prevWeekButton.snp.edges).offset(30)
		}
		nowDate = Date()
		
		let layout2 = CollectionPageFlowLayout(withcol: 3, height: 43)
		
		collectionView = UICollectionView(frame: CGRect(), collectionViewLayout: layout2)
		collectionView.delegate = self
		collectionView.dataSource = self
		collectionView.isPagingEnabled = true
		collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
		view.addSubview(collectionView)
		collectionView.snp.makeConstraints { (make) in
			make.height.equalTo(43)
			make.center.equalTo(view.snp.center)
			make.width.equalTo(view.snp.width)
		}
	}

	override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
		super.viewWillTransition(to: size, with: coordinator)
		print("\(VERTICAL) \(SCREEN_WIDTH()) \(SCREEN_HEIGHT())")
	}
	
	// MARK: - collectionview
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return 21
	}
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
		
		cell.backgroundColor = .brandcolor1
		
		
		return cell
	}
	//
	@objc func moveWeek(_ sender: UIButton) {
		print(collectionView.contentOffset)
		let contentOffset = CGPoint(x: collectionView.contentOffset.x + 16, y: collectionView.contentOffset.y)
		if sender == prevWeekButton {
			if let index = collectionView.indexPathForItem(at: contentOffset) {
				if index.item > 2 {
						self.collectionView.scrollToItem(at: IndexPath(item: index.item-3, section: 0), at: .left, animated: false)
						self.collectionView.contentOffset = CGPoint(x: self.collectionView.contentOffset.x - 16, y: 0)
				} else {
					
				}
			}
//			calendar.select(prevWeek(), scrollToDate: true)
		} else {
			if let index = collectionView.indexPathForItem(at: contentOffset) {
				if index.item == 15 {
					collectionView.scrollToItem(at: IndexPath(item: index.item+3, section: 0), at: .left, animated: true)
				} else if index.item < 16 {
//					collectionView.scrollRectToVisible(<#T##rect: CGRect##CGRect#>, animated: <#T##Bool#>)
					collectionView.scrollToItem(at: IndexPath(item: index.item+3, section: 0), at: .left, animated: true)
					collectionView.contentOffset = CGPoint(x: collectionView.contentOffset.x - 16, y: 0)
				} else {
					
				}
			}
//			calendar.select(nextWeek(), scrollToDate: true)
		}
	}
	func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
		print("end animate")
	}
	func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
		print("start")
	}
	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		print("end")
	}
	
	func bannerCollection(_ collection: CustomBannerCollectionView, didMoveToPage page: Int) {
		print(page)
	}
	func bannerCollection(_ collection: CustomBannerCollectionView, didTouchPage page: Int) {
		print(page)
	}
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
	func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
		calendar.snp.updateConstraints { (make) in
			make.height.equalTo(bounds.height)
		}
		view.layoutIfNeeded()
	}
	
	func nextWeek() -> Date {
		let now = nowDate
		let calendar = Calendar.current
		var comp = calendar.dateComponents([.year, .month, .weekOfMonth, .weekday, .day], from: now!)
		comp.day! += 7
		//		if comp.month == 0 {
		//			comp.month = 11
		//			comp.year = comp.year! - 1
		//		} else {
		//			comp.month = comp.month! - 1
		//		}
		
		nowDate = calendar.date(from: comp)!
		return nowDate
	}
	func prevWeek() -> Date {
		let now = nowDate
		let calendar = Calendar.current
		var comp = calendar.dateComponents([.year, .month, .weekOfMonth, .weekday, .day], from: now!)
		comp.day! -= 7
		
		//		if comp.month == 0 {
		//			comp.month = 11
		//			comp.year = comp.year! - 1
		//		} else {
		//			comp.month = comp.month! - 1
		//		}
		nowDate = calendar.date(from: comp)!
		return nowDate
	}
	// MARK: - FSCalendarDataSource
	func minimumDate(for calendar: FSCalendar) -> Date {
		let now = Date.init()
		let calendar = Calendar.current
		var comp = calendar.dateComponents([.year, .month, .weekOfMonth, .weekday], from: now)
		comp.day = 0
//		if comp.month == 0 {
//			comp.month = 11
//			comp.year = comp.year! - 1
//		} else {
//			comp.month = comp.month! - 1
//		}
		
		let dateMonth = calendar.date(from: comp)
		return Date()
	}
	func maximumDate(for calendar: FSCalendar) -> Date {
		let now = Date.init()
		let calendar = Calendar.current
		var comp = calendar.dateComponents([.year, .month, .weekOfMonth, .weekday], from: now)
		
		comp.year = comp.year!+1
		
		comp.day = 0
		
		let dateMonth = calendar.date(from: comp)
		return dateMonth!
	}
	func calendar(_ calendar: FSCalendar, subtitleFor date: Date) -> String? {
		return nil
	}
	// MARK: - FSCalendarDelegate
	func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
		calendar.select(date, scrollToDate: false)
	}
	func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
		return 0
	}
	// MARK: - FSCalendarDelegateAppearance
	func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
		return [.brandcolor2]
	}
	func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventSelectionColorsFor date: Date) -> [UIColor]? {
		return [.successgreen]
	}
	func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
		return nil
	}
	
}

extension CalendarViewController: PivotPageDelegate {
	var titleForPivotPage: String? {
		return "calendar"
	}
	
}
