//
//  ListViewController.swift
//  testSwift
//
//  Created by JJsobtid on 23/1/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit
import RxSwift

class ListViewController: UIViewController {

	var dataSourch: Observable<[String]> = Observable.just(["SelectSeatViewController", "TestOtpViewController", "ChangeAppIconViewController", "MajorHomeViewController", "FirstLayoutViewController", "GoogleMapViewController", "GlossViewController", "ViperViewController", "PlainTableViewController", "MLMainViewController", "ViewController", "NavigationStoryboard", "MainDashboardViewController", "WebViewController", "PairQuestionViewController", "QrScanViewController"])
	var disposeBag = DisposeBag()
	
	var tableView: UITableView!
	
    override func viewDidLoad() {
        super.viewDidLoad()

		createView()
		setConstraints()
		observeData()
        // Do any additional setup after loading the view.
    }
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		tabBarController?.tabBar.isHidden = false
		navigationController?.isNavigationBarHidden = true
	}

	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
//		tabBarController?.tabBar.isHidden = false
	}

	// MARK: - Observe
	func observeData() {
		dataSourch.bind(to: tableView.rx.items(cellIdentifier: "reuseCell", cellType: UITableViewCell.self)) {
			row, vc, cell in
			cell.textLabel?.text = vc
			}.disposed(by: disposeBag)
		
		tableView.rx.modelSelected(String.self).subscribe(onNext: { [weak self] (vc) in
			if let selectedRowIndexPath = self?.tableView.indexPathForSelectedRow {
				self?.tableView.deselectRow(at: selectedRowIndexPath, animated: true)
			}
			if vc == "ViperViewController" {
				let viperVC = ViperWireframe.createViperVC()
				self?.navigationController?.pushViewController(viperVC, animated: true)
			} else if vc == "ViewController" {
				let storyboard = UIStoryboard(name: "Main", bundle: nil)
				let customViewController = storyboard.instantiateViewController(withIdentifier: vc)
				self?.navigationController?.pushViewController(customViewController, animated: true)
			} else if vc == "NavigationStoryboard" {
				let storyboard = UIStoryboard(name: "Main", bundle: nil)
				let customViewController = storyboard.instantiateViewController(withIdentifier: vc)
				self?.present(customViewController, animated: true, completion: nil)
			} else if vc == "MainDashboardViewController", let viewController = viewControllerFromString(viewControllerName: vc)  {
				let navigation = CustomNavigationController(rootViewController: viewController)
				navigation.modalPresentationStyle = .fullScreen
				self?.present(navigation, animated: true, completion: nil)
			} else if let viewController = viewControllerFromString(viewControllerName: vc) {
				self?.navigationController?.pushViewController(viewController, animated: true)
			}
		}).disposed(by: disposeBag)
	}
	
	// MARK: - Create View
	func createView() {
		view.backgroundColor = .white
		
		tableView = UITableView(frame: CGRect(), style: .grouped)
		tableView.register(UITableViewCell.self, forCellReuseIdentifier: "reuseCell")
		view.addSubview(tableView)
	}
	
	// MARK: - Set View Constraints
	func setConstraints() {
		tableView.snp.makeConstraints { (make) in
			make.edges.equalTo(view.snp.edges)
		}
		view.layoutIfNeeded()
	}
	
	// MARK: Deinit
	deinit {
		print("list vc deinit.")
	}
}

func viewControllerFromString(viewControllerName: String) -> UIViewController? {
	
	if let appName = Bundle.main.infoDictionary?["CFBundleName"] as? String {
		print("CFBundleName - \(appName)")
		if let viewControllerType = NSClassFromString("\(appName).\(viewControllerName)") as? UIViewController.Type {
			//				if viewControllerName == "AuthenViewController" {
			//					let rand = arc4random() % 2
			//					let authType: AuthenType = (rand == 0 ? .otp : .facebook)
			//					return AuthenViewController(withType: authType)
			//				} else {
			return viewControllerType.init()
			//				}
		}
	}
	
	return nil
}
