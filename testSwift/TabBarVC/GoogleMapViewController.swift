//
//  GoogleMapViewController.swift
//  testSwift
//
//  Created by JJsobtid on 17/4/2561 BE.
//  Copyright © 2561 JJsobtid. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit
import RealmSwift

class GoogleMapViewController: UIViewController {

	var mapView: MKMapView!
	let regionRadius: CLLocationDistance = 1000
	
	var dynamicInt: Dynamic<Int> = Dynamic(0)
	
	var openInMap: UIButton!
	
	var containerView: UIView!
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		tabBarController?.tabBar.isHidden = true
		navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
		navigationController?.interactivePopGestureRecognizer?.isEnabled = true
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()

		mapView = MKMapView()
		
		let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
		centerMapOnLocation(location: initialLocation)
		
		view.addSubview(mapView)
		mapView.snp.makeConstraints { (make) in
			make.top.equalTo(view.snp.top)
			make.leading.equalTo(view.snp.leading)
			make.bottom.equalTo(view.snp.bottom).dividedBy(2)
			make.trailing.equalTo(view.snp.trailing)
		}
		
		containerView = UIView()
		view.addSubview(containerView)
		containerView.snp.makeConstraints { (make) in
			make.top.equalTo(view.snp.centerY)
			make.leading.equalTo(view.snp.leading)
			make.bottom.equalTo(view.snp.bottom)
			make.trailing.equalTo(view.snp.trailing)
		}
		view.layoutIfNeeded()
		let v4 = ChartViewController()
		
		let nav_4:UINavigationController = UINavigationController(rootViewController: v4)
		
		addChild(nav_4)
		nav_4.view.frame = containerView.bounds  // or better, turn off `translatesAutoresizingMaskIntoConstraints` and then define constraints for this subview
			containerView.addSubview(nav_4.view)
//		v4.didMove(toParentViewController: self)
		
		let artwork = Artwork(title: "King David Kalakaua",
							  locationName: "Waikiki Gateway Park",
							  discipline: "Sculpture",
							  coordinate: CLLocationCoordinate2D(latitude: 21.283921, longitude: -157.831661))
		mapView.addAnnotation(artwork)
		
		openInMap = UIButton(frame: CGRect(x: 100, y: 100, width: 100, height: 100))
		openInMap.setTitle("OPEN MAP", for: .normal)
		openInMap.addTarget(self, action: #selector(openGoogleMap), for: .touchUpInside)
		
		view.addSubview(openInMap)
		
		dynamicInt.bindAndFire { (no) in
			print("Dynamic \(no)")
		}
		
		for i in 1...10 {
			perform(#selector(setDynamicValue), with: self, afterDelay: TimeInterval(i*10))
		}
		
//
//		let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
//		let googleMapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
//		view = googleMapView
//
//		// Creates a marker in the center of the map.
//		let marker = GMSMarker()
//		marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
//		marker.title = "Sydney"
//		marker.snippet = "Australia"
//		marker.map = googleMapView
		
		var config = Realm.Configuration(schemaVersion: 3, migrationBlock: {
			migration, oldSchemaVersion in
			if oldSchemaVersion < 2 {
				migration.enumerateObjects(ofType: Dog.className(), { (oldObj, newObj) in
					let name = oldObj!["name"] as! String
					let age = oldObj!["age"] as! Int
					newObj!["desc"] = "\(name) is \(age) years old"
				})
			}
		})
		
		// Use the default directory, but replace the filename with the username
//		config.fileURL = config.fileURL!.deletingLastPathComponent().appendingPathComponent("defaultDB.realm")
		if let url = config.fileURL {
			print("fileURL : \(url)")
		}
		// Set this as the configuration used for the default Realm
		Realm.Configuration.defaultConfiguration = config
		// Use them like regular Swift objects
		let myDog = Dog()
		myDog.name = "Rex"
		myDog.age = 1
		print("name of dog: \(myDog.name)")

		let friendsDog = Dog()
		friendsDog.name = "Jake"
		friendsDog.age = 3
		// Get the default Realm
		do {
			let realm = try Realm()
			
			// Query Realm for all dogs less than 2 years old
			let puppies = realm.objects(Dog.self)//.filter("age < 2")
			print(puppies.count) // => 0 because no dogs have been added to the Realm yet
			
			let pupiesHasMyDog = puppies.filter{ $0.name == myDog.name }.count > 0
			let pupiesHasFriendDog = puppies.filter{ $0.name == friendsDog.name }.count > 0
			// Persist your data easily
			try realm.write {
				if !pupiesHasMyDog {
					realm.add(myDog)
				}
				if !pupiesHasFriendDog {
					realm.add(friendsDog)
				}
			}
			
			// Queries are updated in realtime
			for dog in puppies {
//				let duplicatePuppies = puppies.filter{ $0.name == dog.name }
//				if duplicatePuppies.count > 1 {
				
//				}
				print(dog.desc)
//				print(duplicatePuppies.count)
			}
			print(puppies.count) // => 1
			
			// Query and update from any thread
//			DispatchQueue(label: "background").async {
//				autoreleasepool {
//					let realm = try Realm()
//					let theDog = realm.objects(Dog.self).filter("age == 1").first
//					try realm.write {
//						theDog!.age = 3
//					}
//				}
//			}
		} catch let error as NSError {
			print(error)
		}
		
    }
	@objc func setDynamicValue() {
		dynamicInt.value = 0
	}
	
	func centerMapOnLocation(location: CLLocation) {
		let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
												  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
		mapView.setRegion(coordinateRegion, animated: true)
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	@objc func openGoogleMap() {
		if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
			UIApplication.shared.openURL(URL(string:
				"comgooglemaps://?saddr=&daddr=\(21.282778),\(-157.829444)&directionsmode=driving")!)
			
		} else {
			NSLog("Can't use comgooglemaps://");
		}
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

	class Artwork: NSObject, MKAnnotation {
		let title: String?
		let locationName: String
		let discipline: String
		let coordinate: CLLocationCoordinate2D
		
		init(title: String, locationName: String, discipline: String, coordinate: CLLocationCoordinate2D) {
			self.title = title
			self.locationName = locationName
			self.discipline = ""//discipline
			self.coordinate = coordinate
			
			super.init()
		}
		
		var subtitle: String? {
			return locationName
		}
	}
	
	
	
}
// Define your models like regular Swift classes
class Dog: Object {
	@objc dynamic var name = ""
	@objc dynamic var age = 0
	@objc dynamic var desc = ""
}
class Person: Object {
	@objc dynamic var name = ""
	@objc dynamic var picture: Data? = nil // optionals supported
	let dogs = List<Dog>()
}
