//
//  ViewController.swift
//  testSwift
//
//  Created by JJsobtid on 4/10/2560 BE.
//  Copyright © 2560 JJsobtid. All rights reserved.
//

import UIKit
import Alamofire
import CryptoSwift
import FirebaseFirestore
import FirebaseDatabase
import SnapKit
import TagListView
import LocalAuthentication
import FirebaseAnalytics

let IPHONE = 61

class FirstViewController: UIViewController, TagListViewDelegate {
	
	var str:String?
	@IBOutlet var api :API! = API()
//	var api: API = API()
	var drawAnimateView: CircleView!
	var lineAnimateView: barView!
	var dashAnimateView: DashBar!
	var tagView: TagListView!
	var otpField: UITextField!
	
	var backView = UIView()
	var backShow = false
	var round: Bool = true
	var maskPath = CAShapeLayer()
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	override func viewDidLoad() {
		super.viewDidLoad()
		drawAnimateView = CircleView(withColor: .red, type: .correct, frame: CGRect(x: 0, y: 0, width: 50, height: 50))
		view.addSubview(drawAnimateView)
		drawAnimateView.snp.makeConstraints { (make) in
			make.width.equalTo(50)
			make.height.equalTo(50)
			make.center.equalTo(view.snp.center)
		}
		view.addSubview(drawAnimateView)
		
		let lineDashPatterns: [[NSNumber]?]  = [nil, [1, 15], [8, 15, 1, 15]]
		
		for (index, lineDashPattern) in lineDashPatterns.enumerated() {
			
			let shapeLayer = CAShapeLayer()
			shapeLayer.strokeColor = UIColor.black.cgColor
			shapeLayer.lineWidth = 10
			shapeLayer.lineDashPattern = lineDashPattern
			shapeLayer.lineCap = CAShapeLayerLineCap.round
			
			let path = CGMutablePath()
			let y = CGFloat(index * 50)
			path.addLines(between: [CGPoint(x: 0, y: 100 + y),
									CGPoint(x: SCREEN_WIDTH(), y: 100 + y)])
			
			shapeLayer.path = path
			
			view.layer.addSublayer(shapeLayer)
		}
		
		var textField = UITextField()
//		textField.rx.text.asObservable()
		
		let shapeLayer = CAShapeLayer()
		shapeLayer.strokeColor = UIColor.black.cgColor
		shapeLayer.fillColor = UIColor.clear.cgColor
		shapeLayer.lineWidth = 5
		shapeLayer.lineDashPattern = [10,5,5,5]
		
		let path = CGMutablePath()
		path.addLines(between: [CGPoint(x: 0, y: 20),
								CGPoint(x: 640, y: 20)])
		shapeLayer.path = path
		
		let lineDashAnimation = CABasicAnimation(keyPath: "lineDashPhase")
		lineDashAnimation.fromValue = 0
		lineDashAnimation.toValue = shapeLayer.lineDashPattern?.reduce(0) { $0 + $1.intValue }
		lineDashAnimation.duration = 1
		lineDashAnimation.repeatCount = Float.greatestFiniteMagnitude
		
		shapeLayer.add(lineDashAnimation, forKey: nil)
		
		view.layer.addSublayer(shapeLayer)
		
		self.navigationController?.setNavigationBarHidden(true, animated: false)
		self.view.backgroundColor = .white
		let lb_str:UILabel = UILabel(frame: CGRect(x:self.view.frame.width/2 - self.view.frame.width/4, y:self.view.frame.height/2 - 30, width:self.view.frame.width/2, height:60))
		lb_str.textAlignment = .center
		let bt_addUser :UIButton = UIButton(frame: CGRect(x: self.view.frame.width/2 - 50, y: self.view.frame.size.height/2 - 100, width: 100, height: 20))
		bt_addUser.setTitle("Add User", for: .normal)
		bt_addUser.setTitleColor(.blue, for: .normal)
		bt_addUser.titleLabel?.textAlignment = .center
//		bt_addUser.rx.tap
//			.asObservable().subscribe { (event) in
//			switch event {
//			case .next:
//				let vc = UIViewController()
//				self.present(vc, animated: true, completion: nil)
//			case .completed:
//				print("complete")
//			case .error(let err):
//				print(err)
//			}
//		}
		bt_addUser.backgroundColor = .blue
		bt_addUser.round()
		bt_addUser.addTarget(self, action: #selector(showBack(_:)), for: .touchUpInside)
//		bt_addUser.addTarget(self, action: #selector(addUserToFireStore(_:)), for: .touchUpInside)
//		bt_addUser.showsTouchWhenHighlighted = true

		if Utility().isiPad() {
			print("This device is iPad.")
		} else {
			print("This device is iPhone.")
		}
		
		tagView = TagListView(frame: CGRect(x: 0, y: 50, width: SCREEN_WIDTH(), height: 50))
		tagView.addTags(["test1", "test2", "longTag", "four", "addTag..."])
		tagView.tagBackgroundColor = .brandcolor1
		tagView.textColor = .white
		tagView.delegate = self
		tagView.backgroundColor = .gray
//		tagView.cornerRadius = 10
		dashAnimateView = DashBar(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 50))
		view.addSubview(dashAnimateView)
		
		dashAnimateView.snp.makeConstraints { (make) in
			make.bottom.equalTo(view.snp.bottom).offset(-50)
			make.leading.equalTo(view.snp.leading)
			make.trailing.equalTo(view.snp.trailing)
			make.height.equalTo(50)
		}
		
		view.addSubview(tagView)
		
		if let str_title = str {
			lb_str.text = str_title.md5()
//			let vc = Utility().getCurrentViewController()
//			lb_str.text = String(describing: type(of: vc))
			lb_str.numberOfLines = 3
			api.test(parameter: [["name" : "test"]])
			
		} else {
			lb_str.text = "empty"
		}
		self.view.addSubview(lb_str)
		self.view.addSubview(bt_addUser)
//		lb_str.snp.makeConstraints { (make) in
//			make.top.equalTo(IPHONE)
//		}
		var a = UIColor.dark(color: UIColor.white)
		var e = UIColor.dark(color: UIColor.clear)
		var d = UIColor.dark(color: UIColor.black)
		var b = UIColor.dark(color: UIColor.blackalpha50)
		var c = UIColor.dark(color: UIColor.red)
		var f = UIColor.dark(color: UIColor.popoverbg)
		let todosEndpoint: String = ""//"https://api.sobtid.me/shortnote/api/viewshortnotebyfilterandsort"
		let newTodo: [String: Any] = ["hash" : "41789187125e0f8d199f8442791079ec",
		                              "filter_key" : "subject_group",
		                              "date" : 1507799750,
		                              "member_id" : "6405e8d50168ae52902919a212fec10758b47da2807c9a1136af5b8d40b523969f47c6a77f237448df55221023ef69e7d7ffe897e6b169406d811657ff3a5b3b0560c5cc843e0f3e915a5c3bf3f58886ebdc80ddfa57faff9cd0b64d95e57828f3666da0f70810b8d9399fb376404239df20be370018355962bc2c152135ceee9cec920a80c6741c01a39b26c757d3f281cb32",
		                              "uuid" : "bc7c39b2fc8c4150ac03e5246379bd21",
		                              "token" : "0b7052690351123f831f0031e59a0b49",
		                              "subject_group_id" : 3,
		                              "sort_key" : "popularity",
		                              "index" : 0]
		self.api.SendPost({ (finished, response) in
			if finished {
				print(response)
			}
		}, parameters: newTodo)
		Alamofire.request(todosEndpoint, method: .post, parameters: newTodo,
		                  encoding: JSONEncoding.default)
			.responseJSON { response in
				guard response.result.error == nil else {
					// got an error in getting the data, need to handle it
					print("error calling POST on /todos/1")
					print(response.result.error!)
					return
				}
				// make sure we got some JSON since that's what we expect
				guard let json = response.result.value as? [String: Any] else {
					print("didn't get todo object as JSON from API")
					print("Error: \(response.result.error)")
					return
				}
				// get and print the title
				guard let todoTitle = json["result"] as? [String: Any] else {
					print("Could not get todo title from JSON")
					return
				}
				guard let noteList = todoTitle["shortnotes"] as? [[String: Any]] else {
					print("Could not get todo title from JSON")
					return
				}
//				print("The title is: " + todoTitle)
		}
		queryData()
		
		otpField = UITextField()
		if #available(iOS 10.0, *) {
			if #available(iOS 12.0, *) {
				otpField.textContentType = .oneTimeCode
			} else {
				// Fallback on earlier versions
			}
		} else {
			// Fallback on earlier versions
		}
		
		let circleLoadView = BorderCircleButton(frame: CGRect(x: view.frame.width - 120, y: view.frame.height - 220, width: 100, height: 100))
		view.addSubview(circleLoadView)
		// Do any additional setup after loading the view, typically from a nib.
		
		backView.backgroundColor = .white
		backView.layer.cornerRadius = 10
		backView.layer.borderColor = UIColor.black.cgColor
		backView.layer.borderWidth = 1
		view.addSubview(backView)
		
		backView.snp.makeConstraints { (make) in
			make.leading.equalTo(view.snp.leading)
			make.trailing.equalTo(view.snp.trailing)
			make.top.equalTo(view.snp.bottom)
			make.bottom.equalTo(view.snp.bottom)
		}
		let roundButtonTest = UIButton()
		roundButtonTest.backgroundColor = .blue
		roundButtonTest.layer.cornerRadius = 25
		roundButtonTest.setTitle("testtt", for: .normal)
		roundButtonTest.addTarget(self, action: #selector(roundBtnAction(_:)), for: .touchUpInside)

		view.addSubview(roundButtonTest)
		roundButtonTest.snp.remakeConstraints { (make) in
			make.leading.equalTo(view.snp.leading).offset(50)
			make.trailing.equalTo(view.snp.trailing).offset(-50)
			make.height.equalTo(50)
			make.bottom.equalTo(view.snp.bottom).offset(-100)
		}
		
		let animateButton = AnimationButton()
		animateButton.clipsToBounds = true
		animateButton.setTitle("Animation", for: .normal)
		animateButton.backgroundColor = .yellow
		animateButton.addTarget(self, action: #selector(animationClick(_:)), for: .touchUpInside)
		view.addSubview(animateButton)
		animateButton.snp.makeConstraints { (make) in
			make.size.equalTo(CGSize(width: 100, height: 100))
			make.centerX.equalTo(view.snp.centerX)
			make.top.equalTo(view.snp.centerY).offset(100)
		}
		
		view.layoutIfNeeded()
		
		
		
//		roundButtonTest.layer.mask = maskPath
	}
	@objc func animationClick(_ sender: UIButton) {
		print("hello animated")
	}
	@objc func showBack(_ sender: UIButton) {
		if !backShow {
			backView.snp.remakeConstraints { (make) in
				make.leading.equalTo(view.snp.leading)
				make.trailing.equalTo(view.snp.trailing)
				make.top.equalTo(view.snp.centerY)
				make.bottom.equalTo(view.snp.bottom)
			}
		} else {
			backView.snp.remakeConstraints { (make) in
				make.leading.equalTo(view.snp.leading)
				make.trailing.equalTo(view.snp.trailing)
				make.top.equalTo(view.snp.bottom)
				make.bottom.equalTo(view.snp.bottom)
			}
		}

		UIView.animate(withDuration: 0.25, animations: {
			self.view.layoutIfNeeded()
		}) { (_) in
			self.backShow = !self.backShow
		}
	}
	@objc func roundBtnAction(_ sender: UIButton) {
		
		sender.isEnabled = false
		if !round {
			
//			Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
//				AnalyticsParameterItemID: "button_action",
//				AnalyticsParameterItemName: "round",
//				AnalyticsParameterContentType: "cont"
//			])
			Analytics.logEvent("ui_action", parameters: [
				AnalyticsParameterLocation: "first_view",
				AnalyticsParameterItemName: "round_button",
				AnalyticsParameterContent: "round click",
				AnalyticsParameterContentType: "button"
				])
			
//			id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//
//			[tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"ui_action"
//				action:@"button_press"
//				label:@"play"
//				value:nil] build]];
			
			sender.snp.remakeConstraints { (make) in
				make.leading.equalTo(view.snp.leading).offset(50)
				make.trailing.equalTo(view.snp.trailing).offset(-50)
				make.height.equalTo(50)
				make.bottom.equalTo(view.snp.bottom).offset(-100)
			}
			tabBarController?.tabBar.isHidden = false
		} else {
			
			Analytics.logEvent("ui_action", parameters: [
				AnalyticsParameterLocation: "first_view",
				AnalyticsParameterItemName: "round_button",
				AnalyticsParameterContent: "unround click",
				AnalyticsParameterContentType: "button"
			])
			
			sender.snp.remakeConstraints { (make) in
				make.leading.equalTo(view.snp.leading)
				make.trailing.equalTo(view.snp.trailing)
				make.top.equalTo(view.snp.centerY).offset(-100)
				make.bottom.equalTo(view.snp.bottom).offset(10)
			}
			tabBarController?.tabBar.isHidden = true
		}
		
		UIView.animate(withDuration: 0.2, animations: { [weak self] in
			
			if self?.round ?? true {
				sender.layer.cornerRadius = 10
			} else {
				sender.layer.cornerRadius = 25
			}
			self?.view.layoutIfNeeded()
			
		}) { [weak self] (_) in
			self?.round = !(self?.round ?? true)
			sender.isEnabled = true
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		drawAnimateView.animation()
		
	}
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		lineAnimateView = barView(frame: CGRect(x: 0, y: 0, width: 300, height: 30))
		lineAnimateView.removeFromSuperview()
		view.addSubview(lineAnimateView)
		lineAnimateView.snp.makeConstraints { (make) in
			make.width.equalTo(300)
			make.height.equalTo(30)
			make.center.equalTo(drawAnimateView.snp.center)
		}
	}
	
	func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
		if title != "addTag..." {
			sender.removeTag(title)
		} else {
			sender.removeTagView(tagView)
			sender.addTag("test\(sender.tagViews.count)")
			sender.addTag("addTag...")
			print("sender height: \(sender.intrinsicContentSize)")
			sender.frame = CGRect(origin: sender.frame.origin, size: sender.intrinsicContentSize) 
		}
	}
	
	func showAlertController(_ message: String) {
		let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
		alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
		present(alertController, animated: true, completion: nil)
	}
	
	@objc func addUserToFireStore(_ sender:UIButton) {

//		UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
		
		lineAnimateView.first = true
		let rand = Int(arc4random()) % 100
		lineAnimateView.updateData(CGFloat(rand) / CGFloat(100))
		
		dashAnimateView.first = true
		dashAnimateView.updateData(CGFloat(rand) / CGFloat(100))
		// MARK: - TOUCH ID
		let context = LAContext()
		var error: NSError?
		
		// 2
		// check if Touch ID is available
//		if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
////			// 3
//			let reason = "Authenticate with Touch ID"
//			context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason, reply:
//				{(succes, error) in
//					// 4
//					if succes {
//						self.showAlertController("Touch ID Authentication Succeeded")
//					}
//					else {
////						self.showAlertController("Touch ID Authentication Failed")
//					}
//				})
//		}
//			// 5
//		else {
//			showAlertController("Touch ID not available \(error!)")
//			let reason = "Authenticate with Touch ID"
//			context.evaluatePolicy(.deviceOwnerAuthentication,
//								   localizedReason: reason,
//								   reply: { (success, error) in
//			})
//		}
	
		var ref2: DatabaseReference!
		ref2 = Database.database().reference()
		
		ref2.child("pretest").child("ID1").child("online").child("member2").observeSingleEvent(of: .value, with: { (snapshot) in
			// Get user value
			if snapshot.exists() {
				let value = snapshot.value as? NSDictionary
				print(value!);
				ref2.child("pretest").child("ID1").child("online").child("member2").removeValue()
			} else {
				if self.str! == "View 1" {
					let key = ref2.child("pretest").child("ID1").child("online").childByAutoId().key
					print(key)
					let post = ["uuid": "23fd",
								"device": "iOS" ]
					let childUpdates = ["/pretest/ID1/online/member2" : post
						/* "\(key)/member2": post] as [String : Any] */]
					ref2.updateChildValues(childUpdates)
				} else {
					ref2.child("pretest").child("ID1").child("online").child("member2").removeValue()
				}
			}
			// ...
		}) { (error) in
			print(error.localizedDescription)
		}
		let defaultStore = Firestore.firestore()
		var ref: DocumentReference? = nil
		
		for i in 0...10 {
			let data :[String:Any] = [ "username": "Ada\(i)", "password": "Lovelace\(i)".sha1(), "email": "test\(i)@sobtid.me" ]
			defaultStore.collection("users").document("new-user\(i)").setData(data)
			{ err in
				if let err = err {
					print("Error writing document: \(err)")
				} else {
					print("Document successfully written!")
				}
			}
		}
		let data :[String:Any] = [ "username": "Ada", "password": "Lovelace".sha1(), "email": "test@sobtid.me" ]
//		defaultStore.collection("user").document("new-user").updateData(<#T##fields: [AnyHashable : Any]##[AnyHashable : Any]#>) { (err) in
//
//		}
//		ref = defaultStore.collection("users").addDocument(data: [
//			"username": "Ada",
//			"password": "Lovelace".sha1(),
//			"email": "test@sobtid.me"
//		]) { err in
//			if let err = err {
//				print("Error adding document: \(err)")
//			} else {
//				print("Document added with ID: \(ref!.documentID)")
//			}
//		}
		defaultStore.collection("users").document("new-user").setData(data)
		{ err in
			if let err = err {
				print("Error writing document: \(err)")
			} else {
				print("Document successfully written!")
			}
		}
	}
	
	func queryData() {
		let db = Firestore.firestore()
		db.collection("users").whereField("username", isEqualTo: "Ada").getDocuments()
		{ (querySnapshot, err) in
			if let err = err {
				print("Error getting documents: \(err)")
			} else {
				if querySnapshot?.documents.count == 0 {
					print("no document")
				} else {
					for document in querySnapshot!.documents {
						print("\(document.documentID) => \(document.data())")
					}
				}
			}
		}
		db.collection("users").document("new-user").getDocument { (querySnapshot, error) in
			if let err = error {
				print(err)
			} else {
				if (querySnapshot?.exists)! {
					print(querySnapshot?.data())
					if let arr : [[String : Any]] = querySnapshot?["array"] as? [[String : Any]] {
						for i in arr {
							if let a = i["ID"] {
								print("ID : \(a)")
							}
							if let a = i["age"] {
								print("Age : \(a)")
							}
							if let a = i["name"] {
								print("Name : \(a)")
							}
						}
						let con = arr.contains(where: { dict -> Bool in
							dict.values.contains(where: { key -> Bool in
								if key as? String == "felis" {
									return true
								}
								return false
							})
						})
						let data = ["name" : "felis", "ID" : "1", "Age" : 10] as [String : Any]
						var updateData = arr
						if(!con) {
							updateData.append(data)
							db.collection("users").document("new-user").updateData(["array" : updateData])
							{ (err) in
								if let err = err {
									print(err)
								} else {
									print("update completed.")
								}
							}
						} else {
							print("Felis")
							
						}
					} else {
					}
					
				} else {
					
				}
			}
		}
		let listenerPath = db.collection("users").whereField("username", isEqualTo: "Ada").addSnapshotListener { querySnapshot, error in
			guard let snapshot = querySnapshot else {
				print("Error fetching snapshots: \(error!)")
				return
			}
			snapshot.documentChanges.forEach { diff in
				if (diff.type == .added) {
					print("New city: \(diff.document.data())")
				}
				if (diff.type == .modified) {
					print("Modified city: \(diff.document.data())")
				}
				if (diff.type == .removed) {
					print("Removed city: \(diff.document.data())")
				}
			}
		}
	}
}

extension ViewController: PivotPageDelegate {
	var titleForPivotPage: String? {
		return "view"
	}

}

extension UIView {
	func corner(radius: CGFloat = 8.0, clipsToBounds: Bool = false) {
		layer.cornerRadius = radius
		self.clipsToBounds = clipsToBounds
	}
	func round(corners: UIRectCorner = [.allCorners], radius: CGFloat = 10.0) -> CGPath {
		let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
		let mask = CAShapeLayer()
		mask.path = path.cgPath
		
		return path.cgPath
	}
}
