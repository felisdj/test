//
//  GlossViewController.swift
//  testSwift
//
//  Created by JJsobtid on 5/2/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit
import MapKit

class GlossViewController: UIViewController, CLLocationManagerDelegate {

	struct Person {
		var name: String
		var surname: String
		
		static var data: [Person] {
			return [
				Person(name: "test", surname: "hello"),
				Person(name: "name", surname: "world")
			]
		}
	}
	
    private let locationManager = CLLocationManager()
    var tableView: TableView<Person, UITableViewCell>!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createView()
        setConstraints()
        
        perform(#selector(setData), with: nil, afterDelay: 5.0)
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }
    
	@objc func setData() {
		tableView.items = Person.data
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		tabBarController?.tabBar.isHidden = true
		navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
		navigationController?.interactivePopGestureRecognizer?.isEnabled = true
	}
	
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .denied:
            print("User denied location permission") // Bail out of switch statement. Consider showing an alert that your app will need location to work.
            return
        case .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            print("App is authorized to use location while in use")
            break
        case .authorizedAlways:
            print("App is authorized to always use this device's location")
            break
        default:
            print("User has not yet determined location permission")
            return
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let userLocation = locations.first else {
            return
        }
        userLocation.calculateDrivingDistance(to: CLLocation(latitude: 13.811345, longitude: 100.564406)) { (time, distance) in
            print("distance: \(distance ?? 0) and \(ceil((time ?? 0)/60))")
            
        }
        locationManager.stopUpdatingLocation()
    }
    
	// MARK: - Create View
	func createView() {
		view.backgroundColor = .white
		
		tableView = TableView(items: [], configure: { (cell: UITableViewCell, person: Person) in
			cell.textLabel?.text = person.name
		}, selectHandler: { (person) in
			print(person.surname)
		})
		
		view.addSubview(tableView)
	}
	
	// MARK: - Set View Constraints
	func setConstraints() {
		tableView.snp.makeConstraints { (make) in
			make.edges.equalTo(view.snp.edges)
		}
		view.layoutIfNeeded()
	}
	
	// MARK: Deinit
	deinit {
		print("First Layout Deinit.")
	}

}

extension CLLocation {
    /// Returns a distance in meters from a starting location to a destination location.
    func calculateDrivingDistance(to destination: CLLocation, completion: @escaping(TimeInterval?, CLLocationDistance?) -> Void) {
        let request = MKDirections.Request()
        let startingPoint = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2DMake(self.coordinate.latitude, self.coordinate.longitude)))
        let endingPoint = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2DMake(destination.coordinate.latitude, destination.coordinate.longitude)))
        request.source = startingPoint
        request.destination = endingPoint
        
        let directions = MKDirections(request: request)
        directions.calculate { (response, error) in
            if error != nil {
//                assertionFailure("Failed to calculate driving distance. \(String(describing: error))")
            }
            
            guard let data = response else { return }
            
            let meterDistance = data.routes.first?.distance
            let travelTime = data.routes.first?.expectedTravelTime
            completion(travelTime, meterDistance)
        }
    }
}
