//
//  BluetoothViewController.swift
//  testSwift
//
//  Created by JSobtid on 25/11/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

class BezierPathView: UIView {
	
	var mirror: Bool = false
	
	override func layoutSubviews() {
		super.layoutSubviews()
		createPath()
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
	}
	
	/*
	78
	function drawShape(ctx, xoff, yoff) {
	  ctx.beginPath();
	  ctx.moveTo(78 + xoff, 316 + yoff);
	  ctx.bezierCurveTo(151 + xoff, 316 + yoff, 135 + xoff, 350 + yoff, 165 + xoff, 350 + yoff);
	  ctx.bezierCurveTo(199 + xoff, 350 + yoff, 193 + xoff, 285 + yoff, 222 + xoff, 285 + yoff);
	  ctx.bezierCurveTo(249 + xoff, 285 + yoff, 238 + xoff, 339 + yoff, 266 + xoff, 339 + yoff);
	  ctx.bezierCurveTo(295 + xoff, 339 + yoff, 275 + xoff, 298 + yoff, 317 + xoff, 296 + yoff);
	  ctx.stroke();
	}
	*/
	func createPath() {

		let path = UIBezierPath()
		path.move(to: CGPoint(x: 0, y: frame.size.height*0.52)) // 1c
		path.addCurve(to: CGPoint(x: frame.size.width*0.36, y: frame.size.height-2), // 2c
					  controlPoint1: CGPoint(x: frame.size.width*0.3, y: frame.size.height*0.52), // 1r
					  controlPoint2: CGPoint(x: frame.size.width*0.24, y: frame.size.height-2)) // 2l
		path.addCurve(to: CGPoint(x: frame.size.width*0.6, y: 2), // 3c
					  controlPoint1: CGPoint(x: frame.size.width*0.51, y: frame.size.height-2), // 2r
					  controlPoint2: CGPoint(x: frame.size.width*0.48, y: 2)) // 3l
		path.addCurve(to: CGPoint(x: frame.size.width*0.79, y: frame.size.height*0.83), // 4c
					  controlPoint1: CGPoint(x: frame.size.width*0.72, y: 2), // 3r
					  controlPoint2: CGPoint(x: frame.size.width*0.67, y: frame.size.height*0.83)) // 4l
		path.addCurve(to: CGPoint(x: frame.size.width, y: frame.size.height*0.2), // 5c
					  controlPoint1: CGPoint(x: frame.size.width*0.91, y: frame.size.height*0.83), // 4r
					  controlPoint2: CGPoint(x: frame.size.width*0.86, y: frame.size.height*0.2)) // 5l
		
		let gradient = CAGradientLayer()
		gradient.frame = bounds
		if mirror {
			gradient.colors = [UIColor.green.cgColor, UIColor.cyan.cgColor]
		} else {
			gradient.colors = [UIColor.magenta.cgColor, UIColor.cyan.cgColor]
		}
		gradient.startPoint = CGPoint(x: 0, y: 0.5)
		gradient.endPoint = CGPoint(x: 1, y: 0.5)
		
		let shapeLayer = CAShapeLayer()
		shapeLayer.path = path.cgPath
		shapeLayer.lineWidth = 2
		shapeLayer.strokeColor = UIColor.black.cgColor
		shapeLayer.fillColor = nil
		
		layer.addSublayer(shapeLayer)
		
		gradient.mask = shapeLayer
		
		layer.addSublayer(gradient)
	}
}

class BluetoothViewController: UIViewController {

	var leftView, rightView: BezierPathView!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		createView()
		setConstraints()
	}
	
	// MARK: create view
	private func createView() {
		leftView = BezierPathView()
		rightView = BezierPathView()

		view.addSubview(leftView)
		view.addSubview(rightView)
	}
	
	// MARK: set constraints
	private func setConstraints() {
		leftView.snp.makeConstraints { (make) in
			make.leading.equalTo(view.snp.leading)
			make.centerY.equalTo(view.snp.centerY)
			make.trailing.equalTo(view.snp.centerX)
			make.height.equalTo(50)
		}
		rightView.snp.makeConstraints { (make) in
			make.trailing.equalTo(view.snp.trailing)
			make.centerY.equalTo(view.snp.centerY)
			make.leading.equalTo(view.snp.centerX)
			make.height.equalTo(50)
		}
		view.layoutIfNeeded()
	}
}
