//
//  CircleScoreView.swift
//  testSwift
//
//  Created by JSobtid on 4/10/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

class CircleScoreView: UIView {

	let circleWidth: CGFloat = 6.0

	let circleBackgroundLayer = CAShapeLayer()
	let circleLayer = CAShapeLayer()
	var circlePath: UIBezierPath!
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		createView()
		setConstraints()
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
		createView()
		setConstraints()
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		circlePath = UIBezierPath(arcCenter: CGPoint(x: bounds.width/2, y: bounds.height/2), radius: bounds.width/2, startAngle: CGFloat(Double.pi*3/4), endAngle: CGFloat(Double.pi/4), clockwise: true)
		
		circleBackgroundLayer.frame = bounds
		circleBackgroundLayer.path = circlePath.cgPath
		
		circleLayer.frame = bounds
		circleLayer.path = circlePath.cgPath
	}
	
	func runAnimation(per: CGFloat) {
		let fadeAnimate = CABasicAnimation.init(keyPath: "strokeEnd")
		fadeAnimate.fromValue = 0.0
		fadeAnimate.toValue = per
		fadeAnimate.duration = 1.0
//		fadeAnimate.delegate = self
		fadeAnimate.fillMode = CAMediaTimingFillMode.forwards
		fadeAnimate.isRemovedOnCompletion = false
		circleLayer.add(fadeAnimate, forKey: "strokeEnd")
	}
	
	private func setLayer(_ layer:CAShapeLayer) {
		layer.path = circlePath.cgPath
		layer.fillColor = UIColor.clear.cgColor
		layer.lineWidth = circleWidth
		layer.lineCap = CAShapeLayerLineCap.round
		layer.lineJoin = CAShapeLayerLineJoin.round
		layer.strokeStart = 0.0
	}
	// MARK: create view
	private func createView() {
		circlePath = UIBezierPath(arcCenter: CGPoint(x: bounds.width/2, y: bounds.height/2), radius: bounds.width/2, startAngle: CGFloat(Double.pi*3/4), endAngle: CGFloat(Double.pi/4), clockwise: true)
		
		setLayer(circleBackgroundLayer)
		circleBackgroundLayer.strokeColor = UIColor.init(red: 200/255, green: 200/255, blue: 200/255, alpha: 0.16).cgColor
		circleBackgroundLayer.strokeEnd = 1.0
	
		setLayer(circleLayer)
		circleLayer.strokeColor = UIColor.gray.cgColor
		circleLayer.strokeEnd = 0.0

		layer.addSublayer(circleBackgroundLayer)
		layer.addSublayer(circleLayer)
	}
	
	// MARK: set constraints
	private func setConstraints() {
		
	}
}
