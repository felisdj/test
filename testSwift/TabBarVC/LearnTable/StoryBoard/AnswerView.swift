//
//  AnswerView.swift
//  testSwift
//
//  Created by JJsobtid on 12/7/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

class AnswerView: UIView {
	
	private var closeButton: UIButton!
	private var titleLabel: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
	override init(frame: CGRect) {
		super.init(frame: frame)
		createView()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		createView()
		setDefault()
	}
	
	@objc func deleteAnswer() {
		setDefault()
	}
	
	func setAnswer(_ text: String) {
		closeButton.isHidden = false
		titleLabel.text = text
		titleLabel.layer.borderColor = UIColor.gray.cgColor
		titleLabel.layer.borderWidth = 1.0
	}
	func setDefault() {
		closeButton.isHidden = true
		titleLabel.text = ""
		backgroundColor = UIColor(white: 216/255, alpha: 1.0)
		titleLabel.layer.borderWidth = 0.0
	}
	
	// MARK: - Create View
	func createView() {
		closeButton = UIButton(type: .infoDark)
		closeButton.addTarget(self, action: #selector(deleteAnswer), for: .touchUpInside)
		addSubview(closeButton)
		
		titleLabel = UILabel(frame: CGRect())
		titleLabel.textAlignment = .center
		titleLabel.minimumScaleFactor = 12
		titleLabel.numberOfLines = 1
		titleLabel.adjustsFontSizeToFitWidth = true
		addSubview(titleLabel)
		setConstraints()
	}
	
	// MARK: - Set View Constraints
	func setConstraints() {
		closeButton.snp.makeConstraints { (make) in
			make.top.equalTo(snp.top)
			make.trailing.equalTo(snp.trailing)
		}
		titleLabel.snp.makeConstraints { (make) in
			make.edges.equalTo(snp.edges)
			make.baseline.equalTo(snp.centerY)
		}
		layoutIfNeeded()
	}
	
	// MARK: Deinit
	deinit {
		print("answer view deinit.")
	}
	
}
