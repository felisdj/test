//
//  MainStoryViewController.swift
//  testSwift
//
//  Created by JJsobtid on 4/7/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

var hasTopNotch: Bool {
	if #available(iOS 11.0,  *) {
		return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
	}
	
	return false
}

class ViewController: UIViewController {
	
	enum color: String {
		case red, green, blue, gray
		static func returnColor(color: color) -> UIColor {
			switch color {
			case .red:
				return .red
			case .green:
				return .green
			case .blue:
				return .blue
			default:
				return .gray
			}
		}
		static func returnText(color: UIColor) -> String {
			if color == .red {
				return "red"
			} else if color == .green {
				return "green"
			} else if color == .blue {
				return "blue"
			} else {
				return "gray"
			}
		}
	}
	var nowAnswerView: AnswerView?
	var choicesData: [color] = [.red, .green, .blue, .gray]
	
	@IBOutlet weak var answerOne: AnswerView!
	@IBOutlet weak var answerTwo: AnswerView!
	@IBOutlet weak var answerThree: AnswerView!
	@IBOutlet weak var answerFour: AnswerView!

	@IBOutlet weak var choicesView: UIView!
	@IBOutlet weak var collectionChoice: UICollectionView!
	
	var answerOneTap, answerTwoTap, answerThreeTap, answerFourTap: UITapGestureRecognizer!
	@IBOutlet var api :API! = API()
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .default
	}
	override func viewDidLoad() {
		super.viewDidLoad()
		if hasTopNotch {
		print("have notch")
		}
		answerOneTap = UITapGestureRecognizer(target: self, action: #selector(selectedChoice(_:)))
		answerOne.addGestureRecognizer(answerOneTap)
		let dropInteraction = UIDropInteraction(delegate: self)
		answerOne.addInteraction(dropInteraction)
		let dragInteraction = UIDragInteraction(delegate: self)
		dragInteraction.isEnabled = true
		answerOne.addInteraction(dragInteraction)
		answerTwoTap = UITapGestureRecognizer(target: self, action: #selector(selectedChoice(_:)))
		answerTwo.addGestureRecognizer(answerTwoTap)
		answerThreeTap = UITapGestureRecognizer(target: self, action: #selector(selectedChoice(_:)))
		answerThree.addGestureRecognizer(answerThreeTap)
		answerFourTap = UITapGestureRecognizer(target: self, action: #selector(selectedChoice(_:)))
		answerFour.addGestureRecognizer(answerFourTap)
		choicesView.layer.borderWidth = 1.0
		choicesView.layer.borderColor = UIColor.black.cgColor
		choicesView.isHidden = true
		collectionChoice.dragInteractionEnabled = true
	}

	@objc func selectedChoice(_ sender: UITapGestureRecognizer) {
		choicesView.isHidden = false
		nowAnswerView?.layer.borderWidth = 0.0
		if sender == answerOneTap {
			nowAnswerView = answerOne
		} else if sender == answerTwoTap {
			nowAnswerView = answerTwo
		} else if sender == answerThreeTap {
			nowAnswerView = answerThree
		} else {
			nowAnswerView = answerFour
		}
		nowAnswerView?.layer.borderColor = UIColor.black.cgColor
		nowAnswerView?.layer.borderWidth = 1.0
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
	}
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

	}
	
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDragDelegate, UICollectionViewDropDelegate {
	func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
		let item = choicesData[indexPath.item]
		guard let data = item.rawValue.data(using: .utf8) else { return [] }
		let itemProvider = NSItemProvider(item: data as NSSecureCoding, typeIdentifier: "text")
		let dragItem = UIDragItem(itemProvider: NSItemProvider(object: color.returnColor(color: item)))
		return [dragItem]//[UIDragItem(itemProvider: itemProvider)]
	}
	
	func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
		print("drop cell")
	}
//	func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
//		<#code#>
//	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return choicesData.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reuse", for: indexPath)
		print("\(cell.frame)")
		let colorName = choicesData[indexPath.item]
		cell.backgroundColor = color.returnColor(color: colorName)
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		collectionView.deselectItem(at: indexPath, animated: true)
		nowAnswerView?.backgroundColor = color.returnColor(color: choicesData[indexPath.item])
//		choicesData.remove(at: indexPath.item)
//		collectionView.deleteItems(at: [indexPath])
		if choicesData.count == 0 {
			choicesView.isHidden = true
		}
	}
	
}

extension ViewController: UIDropInteractionDelegate, UIDragInteractionDelegate {
	func dragInteraction(_ interaction: UIDragInteraction, itemsForBeginning session: UIDragSession) -> [UIDragItem] {
		let dragItem = UIDragItem(itemProvider: NSItemProvider(object: nowAnswerView!.backgroundColor!))
		return [dragItem]//[UIDragItem(itemProvider: itemProvider)]
	}
	
	func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
		return session.canLoadObjects(ofClass: UIColor.self) && session.items.count == 1
//		return session.hasItemsConforming(toTypeIdentifiers: [kUTTypeImage as String]) && session.items.count == 1
	}
	func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
		let dropLocation = session.location(in: view)
		print(dropLocation)
		let operation: UIDropOperation
//		if imageView.frame.contains(dropLocation) {
//			/*
//			If you add in-app drag-and-drop support for the .move
//			operation, you must write code to coordinate between the drag
//			interaction delegate and the drop interaction delegate.
//			*/
//			operation = session.localDragSession == nil ? .copy : .move
//		} else {
//			// Do not allow dropping outside of the image view.
			operation = .move
//		}
		return UIDropProposal(operation: operation)
	}
	func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
		session.loadObjects(ofClass: UIColor.self) { (colorItems) in
			let colors = colorItems as? [UIColor] ?? []
			self.nowAnswerView?.backgroundColor = colors.first
			self.nowAnswerView?.setAnswer(color.returnText(color: colors.first!))
		}
	}
}
