//
//  DynamicTableViewController.swift
//  testSwift
//
//  Created by JSobtid on 12/12/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit
import SkeletonView

class DynamicTableViewController: UIViewController, UITableViewDelegate, SkeletonTableViewDataSource {

	@IBOutlet weak var tableView: UITableView!

	var nowPage: Int = 0
	let limitPage: Int = 10
	
	let tempData: [Int] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	var dataSource: [Int] = []
	
    override func viewDidLoad() {
        super.viewDidLoad()
		view.showAnimatedGradientSkeleton()
        // Do any additional setup after loading the view.
    }
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [weak self] in
			self?.appendData()
		}
	}
	@IBAction func closeView(_ sender: Any) {
		dismiss(animated: true, completion: nil)
	}
	
	func appendData() {
		if nowPage <= limitPage {
			dataSource.append(contentsOf: tempData)
			tableView.hideSkeleton(transition: .crossDissolve(0.25))
			tableView.reloadData()
			if nowPage == 0 {
			}
			nowPage += 1
		}
	}
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return dataSource.count + (dataSource.count+1)/5
	}
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "photoList", for: indexPath) as? PhotoListTableViewCell else {
			return PhotoListTableViewCell()
		}
		cell.set(indexPath: indexPath, numSep: indexPath.item/5)
		return cell
	}
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		if indexPath.item == dataSource.count - 1 + (dataSource.count+1)/5 {
			appendData()
		}
	}
	func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
		return "photoList"
	}
}
