//
//  PhotoListTableViewCell.swift
//  testSwift
//
//  Created by JSobtid on 12/12/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

class PhotoListTableViewCell: UITableViewCell {

	@IBOutlet weak var backgroundHeight: NSLayoutConstraint!
	@IBOutlet weak var imageHeight: NSLayoutConstraint!
	
	@IBOutlet weak var coverImageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var detailLabel: UILabel!
	@IBOutlet weak var backgroundImageView: UIImageView!
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

	func set(indexPath: IndexPath, numSep: Int) {
		if (indexPath.item+1) % 5 == 0 {
			titleLabel.text = nil
			detailLabel.text = nil
			coverImageView.isHidden = true
			imageHeight.constant = 0
			backgroundImageView.alpha = 1
		} else {
			titleLabel.text = ["short", "long longlonglong longlonglonglonglong"].randomElement()
			detailLabel.text = ["\(indexPath.item - numSep + 1) nil", "\(indexPath.item - numSep + 1) text\n3 line\ntesting", "\(indexPath.item - numSep + 1) text\n3 line\ntestingtext\n3 line\ntestingtext\n3 line\ntestingtext\n3 line\ntestingtext\n3 line\ntesting"][indexPath.item % 3]
			coverImageView.isHidden = false
			imageHeight.constant = 80
			backgroundImageView.alpha = 0
		}
	}
}
