//
//  PlainTableViewController.swift
//  testSwift
//
//  Created by JJsobtid on 26/6/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

class PlainTableViewController: UIViewController {

	lazy var biometricAuth = BiometricAuth.shared
	
	private class HeaderView: UITableViewHeaderFooterView {
		var titleLabel: UILabel!
		
		override init(reuseIdentifier: String?) {
			super.init(reuseIdentifier: reuseIdentifier)
			
			contentView.backgroundColor = .blue
			titleLabel = UILabel()
			titleLabel.textColor = .white
			titleLabel.numberOfLines = 0
			titleLabel.lineBreakMode = .byWordWrapping
			
			contentView.addSubview(titleLabel)
			titleLabel.snp.remakeConstraints { (make) in
				make.top.equalTo(contentView.snp.top).offset(4)
				make.bottom.equalTo(contentView.snp.bottom).offset(-4)
				make.leading.equalTo(contentView.snp.leading).offset(MARGIN)
				make.trailing.equalTo(contentView.snp.trailing).offset(-MARGIN)
			}
		}

		func setTitle(_ text: String) {
			titleLabel.text = text
		}
		required init?(coder aDecoder: NSCoder) {
			fatalError("init(coder:) has not been implemented")
		}
	}
	
	var tableView: UITableView!
	
	var viewModel: PlainTableViewModel!
	var biometricLabel: UILabel!
	
	var dataSource: [TableData] = []
	
	var mainUserView: UIImageView!
	var userViews: [UIImageView] = []
	
	var refreshButton: UIButton!
	
	override func viewDidLoad() {
        super.viewDidLoad()

		view.backgroundColor = .white
		viewModel = PlainTableViewModel()
		
		mainUserView = UIImageView(image: UIImage(named: "female"))
		mainUserView.contentMode = .scaleAspectFill
		mainUserView.layer.cornerRadius = 30
		mainUserView.layer.masksToBounds = true
		
		biometricLabel = UILabel()
		biometricLabel.textAlignment = .center
		biometricLabel.text = "\(biometricAuth.type.rawValue)"
		
		tableView = UITableView(frame: CGRect(), style: .plain)
		tableView.delegate = self
		tableView.dataSource = self
		tableView.register(UITableViewCell.self, forCellReuseIdentifier: "reuse")
		tableView.register(HeaderView.self, forHeaderFooterViewReuseIdentifier: "header")
		tableView.sectionHeaderHeight = UITableView.automaticDimension
		tableView.estimatedSectionHeaderHeight = 44
		
		refreshButton = UIButton(type: .infoDark)
		refreshButton.addTarget(viewModel, action: #selector(viewModel.changeDataSource), for: .touchUpInside)
		
		view.addSubview(mainUserView)
		view.addSubview(tableView)
		view.addSubview(biometricLabel)
		view.addSubview(refreshButton)
		
		setConstraints()
		
		viewModel.dataSource.bindAndFire { [weak self] (data) in
			self?.dataSource = data
			self?.tableView.reloadData()
		}
		viewModel.userData.bindAndFire { [weak self] (users) in
			guard let self = self else {
				return
			}
			for userView in self.userViews {
				userView.removeFromSuperview()
			}
			self.userViews = []
			var beforeUser: UIImageView?
			for (index, user) in users.enumerated() {
				if index > 0 {
					let userView = UIImageView(image: UIImage(named: user))
					userView.contentMode = .scaleAspectFill
					userView.layer.cornerRadius = 10
					userView.layer.masksToBounds = true
					if let before = beforeUser {
						self.view.insertSubview(userView, belowSubview: before)
					} else {
						self.view.addSubview(userView)
					}
					self.userViews.append(userView)
					beforeUser = userView
					userView.snp.makeConstraints({ (make) in
						make.size.equalTo(CGSize(width: 20, height: 20))
						make.bottom.equalTo(self.mainUserView.snp.bottom)
						make.trailing.equalTo(self.mainUserView.snp.trailing).offset((index-1)*12)
					})
				} else {
					let toImage = UIImage(named: user)
					UIView.transition(with: self.mainUserView,
									  duration: 0.25,
									  options: UIView.AnimationOptions.transitionCrossDissolve,
									  animations: {
											self.mainUserView.image = toImage
										},
									  completion: nil)
				}
			}
		}
		
    }
	
	fileprivate func setConstraints() {
		mainUserView.snp.makeConstraints { (make) in
			make.size.equalTo(CGSize(width: 60, height: 60))
			make.centerX.equalTo(view.snp.centerX)
			make.centerY.equalTo(view.snp.centerY).dividedBy(2)
		}
		biometricLabel.snp.makeConstraints { (make) in
			make.leading.equalTo(view.snp.leading)
			make.trailing.equalTo(view.snp.trailing)
			make.top.equalTo(mainUserView.snp.bottom).offset(MARGIN)
		}
		tableView.snp.makeConstraints { (make) in
			make.top.equalTo(view.snp.centerY)
			make.leading.equalTo(view.snp.leading)
			make.bottom.equalTo(view.snp.bottom)
			make.trailing.equalTo(view.snp.trailing)
		}
		refreshButton.snp.makeConstraints { (make) in
			make.size.equalTo(CGSize(width: 30, height: 30))
			make.top.equalTo(view.snp.topMargin)
			make.trailing.equalTo(view.snp.trailing)
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		tabBarController?.tabBar.isHidden = true
		navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
		navigationController?.interactivePopGestureRecognizer?.isEnabled = true
	}

}

extension PlainTableViewController: UITableViewDelegate, UITableViewDataSource {

	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? HeaderView
		view?.setTitle(dataSource[section].header)
		return view
	}
	func numberOfSections(in tableView: UITableView) -> Int {
		return dataSource.count
	}
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return dataSource[section].body.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "reuse", for: indexPath)
		cell.textLabel?.text = dataSource[indexPath.section].body[indexPath.item]
		return cell
	}
	
	
}
