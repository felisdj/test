//
//  PlainTableViewModel.swift
//  testSwift
//
//  Created by JJsobtid on 26/6/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import Foundation

struct TableData {
	var header: String
	var body: [String]
}

class PlainTableViewModel {
	
	static let usersOne = ["unknown", "maleGov", "unknown", "female"]
	static let usersTwo = ["maleGov"]
	static let usersThree = ["female", "unknown"]
	
	static let dataOne = [TableData(header: "section 1 Long Xiaomi Mijia WalkingPad Xiaomi Mijia WalkingPad Xiaomi Mijia WalkingPad Xiaomi Mijia WalkingPad", body: ["1, 1", "1, 2", "1, 2"]),
						  TableData(header: "section 2", body: ["2, 1"]),
						  TableData(header: "section 3Long Xiaomi Mijia WalkingPad XiaomiLong Xiaomi Mijia", body: ["3, 1", "3, 2"])]
	static let dataTwo = [TableData(header: "section 1", body: ["1, 1"])]
	
	var userData: Dynamic<[String]> = Dynamic([])
	var dataSource: Dynamic<[TableData]> = Dynamic([])
	
	init() {
		userData.value = PlainTableViewModel.usersOne
		dataSource.value = PlainTableViewModel.dataTwo
	}
	
	@objc func changeDataSource() {
		let randomInt = Int.random(in: 0...2)
		userData.value = randomInt == 0 ? PlainTableViewModel.usersOne : randomInt == 1 ? PlainTableViewModel.usersTwo : PlainTableViewModel.usersThree
		dataSource.value = Int.random(in: 0...1) == 0 ? PlainTableViewModel.dataTwo : PlainTableViewModel.dataOne
	}
	
}
