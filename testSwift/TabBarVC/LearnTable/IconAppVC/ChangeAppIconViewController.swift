//
//  ChangeAppIconViewController.swift
//  testSwift
//
//  Created by JSobtid on 6/11/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

class ChangeAppIconViewController: UIViewController {

	var tableView: UITableView!
	var dataSource: [String?] = [nil, "kiddy", "rat"]
	
    override func viewDidLoad() {
        super.viewDidLoad()

        createView()
		setConstraints()
    }
    
	func changeIcon(to iconName: String?) {
		// 1
		guard UIApplication.shared.supportsAlternateIcons else {
			return
		}

		// 2
		UIApplication.shared.setAlternateIconName(iconName, completionHandler: { (error) in
			// 3
			if let error = error {
				print("App icon failed to change due to \(error.localizedDescription)")
			} else {
				print("App icon changed successfully")
			}
		})
	}
	
	// MARK: create view
	private func createView() {
		tableView = UITableView(frame: CGRect(), style: .grouped)
		tableView.delegate = self
		tableView.dataSource = self
		tableView.register(UITableViewCell.self, forCellReuseIdentifier: "reuse")
		
		view.addSubview(tableView)
	}
	
	// MARK: set constraints
	private func setConstraints() {
		tableView.snp.makeConstraints { (make) in
			make.edges.equalTo(view.snp.edges)
		}
	}
}

extension ChangeAppIconViewController: UITableViewDataSource, UITableViewDelegate {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return dataSource.count
	}
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "reuse", for: indexPath)
		cell.selectionStyle = .none
		cell.textLabel?.text = dataSource[indexPath.item] ?? "Default"
		return cell
	}
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		changeIcon(to: dataSource[indexPath.item])
	}
}
