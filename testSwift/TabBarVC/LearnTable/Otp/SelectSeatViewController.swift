//
//  SelectSeatViewController.swift
//  testSwift
//
//  Created by JSobtid on 21/11/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit
import LineSDK
import PNChart

class SelectSeatViewController: UIViewController, UIGestureRecognizerDelegate {

	var leftView, rightView: BezierPathView!
	
	var facebookButton, lineButton: UIButton!
	var fbImageView: UIImageView!
	var fbLabel: UILabel!
	
	var lineChart: PNLineChart!
	
	var arrayText: [String] = ["Test", "b", "test", "test1", "test0", "sea", "seaB", "a"]
	
    override func viewDidLoad() {
        super.viewDidLoad()

        createView()
		setConstraints()
    }
    
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		navigationController?.interactivePopGestureRecognizer?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
	}
	
	@objc func lineLogin() {
		LoginManager.shared.login(permissions: [.profile], in: self) {
            result in
            switch result {
            case .success(let loginResult):
                print(loginResult.accessToken.value)
				let profileEnabled = loginResult.permissions.contains(.profile)
				if let profile = loginResult.userProfile {
					print("User ID: \(profile.userID)")
					print("User Display Name: \(profile.displayName)")
					print("User Icon: \(String(describing: profile.pictureURL))")
				}
                // Do other things you need with the login result
            case .failure(let error):
                print(error)
            }
        }
	}
	
	// MARK: create view
	private func createView() {
		view.backgroundColor = .white
		
//		lineChart.showSmoothLines = true
		
		lineChart = PNLineChart(frame: CGRect(x: 20, y: 100, width: view.frame.width - 40, height: 300))
		lineChart.yGridLinesColor = .gray
		lineChart.backgroundColor = .white
		lineChart.isShowCoordinateAxis = true
		lineChart.yLabelFormat = "%1.1f"
		lineChart.yLabelColor = .blue
		lineChart.xLabelColor = .blue
		lineChart.showGenYLabels = false
		lineChart.showYGridLines = true
		lineChart.xLabels = ["1", "2", "3", "4", "5"]

		lineChart.xLabelWidth = 20
//		lineChart.showYGridLines = true
		lineChart.legendStyle = .stacked
		lineChart.legendFontColor = .cencelborder
		lineChart.yFixedValueMin = 0
		lineChart.yFixedValueMax = 100
		lineChart.yLabels = ["0, 20, 40, 60, 80, 100"]
		
		let data01Array: [CGFloat] = [20.0, 10.0, 40.0, 100.0, 30.0]
		let data01 = PNLineChartData()
		data01.dataTitle = "Test 1"
		data01.color = .red
		data01.alpha = 0.8
		data01.itemCount = UInt(data01Array.count)
		data01.inflexionPointStyle = .circle
		data01.getData = { (index) in
			let yValue = data01Array[Int(index)]
			return PNLineChartDataItem(y: yValue)
		}
		lineChart.chartData = [data01]
		lineChart.stroke()
		
		facebookButton = UIButton()
		facebookButton.corner(radius: 8, clipsToBounds: true)
		facebookButton.backgroundColor = .gray
		fbImageView = UIImageView(image: UIImage(named: "appbg"))
		fbImageView.contentMode = .scaleAspectFit
		fbLabel = UILabel()
		fbLabel.text = "facebook login"
		fbLabel.textAlignment = .center
		
		facebookButton.addSubview(fbImageView)
		facebookButton.addSubview(fbLabel)
		
		lineButton = UIButton()
		lineButton.backgroundColor = .greenendexam
		lineButton.corner(radius: 8, clipsToBounds: true)
		lineButton.setTitle("Login with Line", for: .normal)
		lineButton.addTarget(self, action: #selector(lineLogin), for: .touchUpInside)
		
		leftView = BezierPathView()
		rightView = BezierPathView()
		rightView.mirror = true

		view.addSubview(leftView)
		view.addSubview(rightView)
		
		view.addSubview(facebookButton)
		view.addSubview(lineButton)
		view.addSubview(lineChart)
		
		print(arrayText.sorted{ $0.lowercased() < $1.lowercased() })
	
	}
	
	// MARK: set constraints
	private func setConstraints() {
		facebookButton.snp.makeConstraints { (make) in
			make.height.equalTo(50)
			make.centerY.equalTo(view.snp.centerY)
			make.leading.equalTo(view.snp.leading).offset(20)
			make.trailing.equalTo(view.snp.trailing).offset(-20)
		}
		fbLabel.snp.makeConstraints { (make) in
			make.centerY.equalTo(facebookButton.snp.centerY)
			make.centerX.equalTo(facebookButton.snp.centerX).offset(14)
		}
		fbImageView.snp.makeConstraints { (make) in
			make.size.equalTo(CGSize(width: 20, height: 20))
			make.centerY.equalTo(fbLabel.snp.centerY)
			make.trailing.equalTo(fbLabel.snp.leading).offset(-8)
		}
		leftView.snp.makeConstraints { (make) in
			make.leading.equalTo(view.snp.leading)
			make.bottom.equalTo(facebookButton.snp.top).offset(-20)
			make.trailing.equalTo(view.snp.centerX)
			make.height.equalTo(50)
		}
		rightView.snp.makeConstraints { (make) in
			make.trailing.equalTo(view.snp.trailing)
			make.centerY.equalTo(leftView.snp.centerY)
			make.leading.equalTo(view.snp.centerX)
			make.height.equalTo(leftView.snp.height)
		}
		lineButton.snp.makeConstraints { (make) in
			make.top.equalTo(facebookButton.snp.bottom).offset(20)
			make.leading.equalTo(facebookButton.snp.leading)
			make.trailing.equalTo(facebookButton.snp.trailing)
			make.height.equalTo(facebookButton.snp.height)
		}
		view.layoutIfNeeded()
		rightView.transform = CGAffineTransform(scaleX: -1, y: 1)
	}
}
