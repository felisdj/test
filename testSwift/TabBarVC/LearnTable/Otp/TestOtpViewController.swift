//
//  TestOtpViewController.swift
//  testSwift
//
//  Created by JSobtid on 11/11/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit
import BMPlayer
import DropDown

class OtpLabelView: UIView {
	var line: UIView!
	var label: UILabel!
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		createView()
		setConstraints()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func setOtp(_ otp: String?) {
		label.text = otp
	}
	
	// MARK: create view
	private func createView() {
		line = UIView()
		line.backgroundColor = .white
		label = UILabel()
		label.textAlignment = .center
		
		addSubview(line)
		addSubview(label)
	}
	
	// MARK: set constraints
	private func setConstraints() {
		line.snp.makeConstraints { (make) in
			make.leading.equalTo(snp.leading)
			make.bottom.equalTo(snp.bottom)
			make.trailing.equalTo(snp.trailing)
			make.height.equalTo(1)
		}
		label.snp.makeConstraints { (make) in
			make.edges.equalTo(snp.edges)
		}
	}
}

class TestOtpViewController: UIViewController, UIGestureRecognizerDelegate {
	
	var videoPlayer: BMPlayer!
	var alphaView: UIView!
	
	var dropDownButton: UIButton!
	
	var otpTextField: UITextField!
	var otpView: UIView!
	var otpOne, otpTwo, otpThree, otpFour, otpFive, otpSix: OtpLabelView!
	
	var otps: [OtpLabelView] = []
	
	lazy var dropDown = DropDown()
	var showDropdown: Bool = false
	
	init() {
		super.init(nibName: nil, bundle: nil)
		hidesBottomBarWhenPushed = true
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()

        createView()
		setConstraints()
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		navigationController?.interactivePopGestureRecognizer?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
		otpTextField.becomeFirstResponder()
	}
	
	@objc func dropdownClick(_ sender: UIButton) {
		if showDropdown {
			dropDown.hide()
		} else {
			UIView.animate(withDuration: 0.25) {
				sender.transform = CGAffineTransform(rotationAngle: .pi)
			}
			/* - dont custom cell
			DropDown.appearance().textColor = UIColor.black
			DropDown.appearance().selectedTextColor = UIColor.red
			DropDown.appearance().textFont = UIFont.systemFont(ofSize: 15)
			DropDown.appearance().backgroundColor = UIColor.white
			DropDown.appearance().selectionBackgroundColor = UIColor.lightGray
			DropDown.appearance().cellHeight = 60
			*/
			dropDown.anchorView = sender
			dropDown.dataSource = ["test 1", "test 2", "test 3"]
			dropDown.selectionAction = { [weak self] (index: Int, item: String) in
				print("Selected item: \(item) at index: \(index)")
				UIView.animate(withDuration: 0.25) {
					self?.dropDownButton.transform = CGAffineTransform(rotationAngle: 0)
				}
				self?.showDropdown = false
			}
			dropDown.width = 100
//			dropDown.register(CustomDropDown.self, forCellReuseIdentifier: "reuse")
			dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
				guard let cell = cell as? CustomDropDown else { return }


				// Setup your custom UI components
				cell.set(title: ["test 1", "test 2", "test 3"][index])
//			   cell.logoImageView.image = UIImage(named: "logo_\(index)")
			}
			dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)! + 16)
			dropDown.cancelAction = { [weak self] in
				UIView.animate(withDuration: 0.25) {
					self?.dropDownButton.transform = CGAffineTransform(rotationAngle: 0)
				}
				self?.showDropdown = false
			}
			dropDown.show()
		}
		showDropdown = !showDropdown
	}
	
	@objc func tapPlay(_ sender: UITapGestureRecognizer) {
		alphaView.alpha = 0
		videoPlayer.play()
	}

	@objc func tapOtpView(_ sender: UITapGestureRecognizer) {
		otpTextField.becomeFirstResponder()
	}
	func resetField() {
		otpTextField.text = ""
		otps.forEach{ $0.setOtp("") }
	}
	
	// MARK: create view
	private func createView() {
		view.backgroundColor = .white
		
		BMPlayerConf.allowLog = true
		BMPlayerConf.shouldAutoPlay = false
		// options to show header view (which include the back button, title and definition change button) , default .Always，options: .Always, .HorizantalOnly and .None
		BMPlayerConf.topBarShowInCase = .none
		BMPlayerConf.animateDelayTimeInterval = TimeInterval(2.5)
		// loader type, see detail：https://github.com/ninjaprox/NVActivityIndicatorView
//		BMPlayerConf.loaderType  = NVActivityIndicatorType.BallRotateChase
		// enable setting the brightness by touch gesture in the player
		BMPlayerConf.enableBrightnessGestures = false
		// enable setting the volume by touch gesture in the player
		BMPlayerConf.enableVolumeGestures = true
		// enable setting the playtime by touch gesture in the player
//		BMPlayerConf.enablePlaytimeGestures = true
		let control = BMPlayerControlView()
		control.controlViewAnimation(isShow: true)
		let asset = BMPlayerResource(url: URL(string: "http://cdn.majorcineplex.com/uploads/trailer/rawvideo/5424/5424.mp4")!, name: "tailor", cover: URL(string: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/BigBuckBunny.jpg"))
		
//		let asset = BMPlayerResource(url: URL(string: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4")!, name: "tailor", cover: URL(string: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/BigBuckBunny.jpg"))

		videoPlayer = BMPlayer(customControlView: control)
		videoPlayer.setVideo(resource: asset)
		videoPlayer.pause(allowAutoPlay: false)
		videoPlayer.delegate = self
		
		let tapVideo = UITapGestureRecognizer(target: self, action: #selector(tapPlay(_:)))
		alphaView = UIView()
		alphaView.backgroundColor = .blackalpha
		alphaView.addGestureRecognizer(tapVideo)
		alphaView.isUserInteractionEnabled = true
		
		dropDownButton = UIButton()
		dropDownButton.setImage(UIImage(named: "dropdown"), for: .normal)
		dropDownButton.addTarget(self, action: #selector(dropdownClick(_:)), for: .touchUpInside)
		
		otpTextField = UITextField()
		otpTextField.delegate = self
		otpTextField.alpha = 0.0
		otpTextField.keyboardType = .decimalPad
		otpTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
		if #available(iOS 12.0, *) {
			otpTextField.textContentType = .oneTimeCode
		} else {
			// Fallback on earlier versions
		}
		
		let tapOtp = UITapGestureRecognizer(target: self, action: #selector(tapOtpView(_:)))
		
		otpView = UIView()
		otpView.backgroundColor = .blackalpha
		otpView.corner()
		otpView.addGestureRecognizer(tapOtp)
		otpView.isUserInteractionEnabled = true
		
		otpOne = OtpLabelView()
		otpTwo = OtpLabelView()
		otpThree = OtpLabelView()
		otpFour = OtpLabelView()
		otpFive = OtpLabelView()
		otpSix = OtpLabelView()
		
		otps = [otpOne, otpTwo, otpThree, otpFour, otpFive, otpSix]
		
		otps.forEach{ otpView.addSubview($0) }
		view.addSubview(otpTextField)
		view.addSubview(otpView)
		
		view.addSubview(videoPlayer)
		view.addSubview(alphaView)
		view.addSubview(dropDownButton)
	}
	
	// MARK: set constraints
	private func setConstraints() {
		videoPlayer.snp.makeConstraints { (make) in
			make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
			make.left.right.equalTo(view)
			make.height.equalTo(videoPlayer.snp.width).multipliedBy(9.0/16.0)
		}
		alphaView.snp.makeConstraints { (make) in
			make.edges.equalTo(videoPlayer.snp.edges)
		}
		dropDownButton.snp.makeConstraints { (make) in
			make.size.equalTo(CGSize(width: 20, height: 20))
			make.top.equalTo(videoPlayer.snp.bottom).offset(8)
			make.trailing.equalTo(videoPlayer.snp.trailing).offset(-8)
		}
		otpTextField.snp.makeConstraints { (make) in
			make.height.equalTo(40)
			make.center.equalTo(view.snp.center)
			make.width.equalTo(180)
		}
		otpView.snp.makeConstraints { (make) in
			make.edges.equalTo(otpTextField.snp.edges)
		}
		otpThree.snp.makeConstraints { (make) in
			make.trailing.equalTo(otpTextField.snp.centerX).offset(-4)
			make.bottom.equalTo(otpTextField.snp.bottom).offset(-4)
			make.top.equalTo(otpTextField.snp.top).offset(4)
			make.width.equalTo((180-7*8)/6)
		}
		otpTwo.snp.makeConstraints { (make) in
			make.top.equalTo(otpThree.snp.top)
			make.bottom.equalTo(otpThree.snp.bottom)
			make.trailing.equalTo(otpThree.snp.leading).offset(-8)
			make.width.equalTo(otpThree.snp.width)
		}
		otpOne.snp.makeConstraints { (make) in
			make.top.equalTo(otpThree.snp.top)
			make.bottom.equalTo(otpThree.snp.bottom)
			make.trailing.equalTo(otpTwo.snp.leading).offset(-8)
			make.width.equalTo(otpThree.snp.width)
		}
		otpFour.snp.makeConstraints { (make) in
			make.top.equalTo(otpThree.snp.top)
			make.bottom.equalTo(otpThree.snp.bottom)
			make.leading.equalTo(otpThree.snp.trailing).offset(8)
			make.width.equalTo(otpThree.snp.width)
		}
		otpFive.snp.makeConstraints { (make) in
			make.top.equalTo(otpThree.snp.top)
			make.bottom.equalTo(otpThree.snp.bottom)
			make.leading.equalTo(otpFour.snp.trailing).offset(8)
			make.width.equalTo(otpThree.snp.width)
		}
		otpSix.snp.makeConstraints { (make) in
			make.top.equalTo(otpThree.snp.top)
			make.bottom.equalTo(otpThree.snp.bottom)
			make.leading.equalTo(otpFive.snp.trailing).offset(8)
			make.width.equalTo(otpThree.snp.width)
		}
		view.layoutIfNeeded()
	}

	deinit {
		print("otp deini!!!")
	}
}

extension TestOtpViewController: UITextFieldDelegate, BMPlayerDelegate {
	func bmPlayer(player: BMPlayer, playerStateDidChange state: BMPlayerState) {
		
	}
	
	func bmPlayer(player: BMPlayer, loadedTimeDidChange loadedDuration: TimeInterval, totalDuration: TimeInterval) {
		
	}
	
	func bmPlayer(player: BMPlayer, playTimeDidChange currentTime: TimeInterval, totalTime: TimeInterval) {
		
	}
	
	func bmPlayer(player: BMPlayer, playerIsPlaying playing: Bool) {
		
	}
	
	func bmPlayer(player: BMPlayer, playerOrientChanged isFullscreen: Bool) {
		if isFullscreen {
			videoPlayer.snp.remakeConstraints { (make) in
				make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
				make.left.right.equalTo(view)
				make.bottom.equalTo(view.snp.bottom)
			}
		} else {
			videoPlayer.snp.remakeConstraints { (make) in
				make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
				make.left.right.equalTo(view)
				make.height.equalTo(videoPlayer.snp.width).multipliedBy(9.0/16.0)
			}
		}
		view.layoutIfNeeded()
	}
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		print(textField.text ?? "", range.location, string)
		if string.isEmpty {
			otps[range.location].setOtp(string)
			return true
		} else if Int(string) == nil {
			return false
		} else if textField.text?.count ?? 0 < 6 {
			otps[range.location].setOtp(string)
			return true
		}
		view.endEditing(true)
		return false
	}
	
	@objc func textFieldDidChange(_ textField: UITextField) {
		if textField.text?.count ?? 0 >= 6 {
			view.endEditing(true)
			resetField()
		}
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		print("end : \(textField.text ?? "")")
	}

}

class CustomDropDown: DropDownCell {
	
	var titleLabel: UILabel!
	var dropImageView: UIImageView!
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		createView()
		setConstraints()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func set(title: String) {
		titleLabel.text = title
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		let executeSelection: () -> Void = { [weak self] in
			guard let `self` = self else { return }

			if selected {
				self.contentView.backgroundColor = .gray
				self.titleLabel.textColor = .white
			} else {
				self.contentView.backgroundColor = .white
				self.titleLabel.textColor = .black
			}
			
		}
		
		if animated {
			UIView.animate(withDuration: 0.3, animations: {
				executeSelection()
			})
		} else {
			executeSelection()
		}

		accessibilityTraits = selected ? .selected : .none
	}
	
	// MARK: create view
	private func createView() {
		contentView.backgroundColor = .white
		titleLabel = UILabel()
		titleLabel.textColor = .black
		dropImageView = UIImageView(image: UIImage(named: "dropdown"))
		dropImageView.contentMode = .scaleAspectFit
		
		contentView.addSubview(titleLabel)
		contentView.addSubview(dropImageView)
	}
	
	// MARK: set constraints
	private func setConstraints() {
		titleLabel.snp.makeConstraints { (make) in
			make.leading.equalTo(contentView.snp.leading).offset(8)
			make.trailing.equalTo(dropImageView.snp.leading).offset(-8)
			make.centerY.equalTo(contentView.snp.centerY)
		}
		dropImageView.snp.makeConstraints { (make) in
			make.size.equalTo(CGSize(width: 20, height: 20))
			make.trailing.equalTo(contentView.snp.trailing).offset(-8)
			make.centerY.equalTo(contentView.snp.centerY)
		}
	}
}
