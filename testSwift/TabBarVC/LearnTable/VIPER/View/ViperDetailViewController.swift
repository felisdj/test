//
//  ViperDetailViewController.swift
//  testSwift
//
//  Created by JJsobtid on 16/5/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

class ViperDetailViewController: UIViewController, ViperDetailViewProtocol {
	
	var presenter: ViperDetailPresenterProtocol?
	
	func setDetail(_ number: Int) {
		detailLabel.text = "\(number)"
	}

	var backButton: UIButton!
	var detailLabel: UILabel!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		view.backgroundColor = .white
		
		backButton = UIButton(frame: CGRect(x: 20, y: 20, width: 100, height: 50))
		backButton.addTarget(self, action: #selector(back), for: .touchUpInside)
		backButton.setTitleColor(.blue, for: .normal)
		backButton.setTitle("back", for: .normal)
		view.addSubview(backButton)
		
		detailLabel = UILabel(frame: CGRect(x: 100, y: 100, width: 200, height: 30))
		detailLabel.textAlignment = .center
		detailLabel.font = UIFont.systemFont(ofSize: 40)
		view.addSubview(detailLabel)
		
		presenter?.viewDidLoad()
    }
	
	@objc func back() {
		presenter?.backToViper(view: self)
	}
	
	deinit {
		print("detail vc deinit")
	}
}
