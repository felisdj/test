//
//  ViperViewController.swift
//  testSwift
//
//  Created by JJsobtid on 14/5/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

class ViperViewController: UIViewController, ViperViewProtocol {
	
	var presenter: ViperPresenterProtocol?

	var button, detailButton: UIButton!
	var textLabel: UILabel!
	
//	init(presenter: ViperPresenterProtocol) {
//		super.init(nibName: nil, bundle: nil)
//		self.presenter = presenter
//	}
//	
//	required init?(coder aDecoder: NSCoder) {
//		fatalError("init(coder:) has not been implemented")
//	}
	
	override func viewDidLoad() {
        super.viewDidLoad()

		view.backgroundColor = .white
		
		button = UIButton(frame: CGRect(x: 100, y: 150, width: 100, height: 50))
		button.addTarget(self, action: #selector(randomNumber), for: .touchUpInside)
		button.setTitleColor(.blue, for: .normal)
		view.addSubview(button)
		
		detailButton = UIButton(frame: CGRect(x: 100, y: 200, width: 100, height: 50))
		detailButton.addTarget(self, action: #selector(showDetail), for: .touchUpInside)
		detailButton.setTitleColor(.blue, for: .normal)
		view.addSubview(detailButton)
		
		textLabel = UILabel(frame: CGRect(x: 100, y: 100, width: 200, height: 30))
		textLabel.textColor = .blue
		view.addSubview(textLabel)
		
		presenter?.viewDidLoad()
    }

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		tabBarController?.tabBar.isHidden = true
		navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
		navigationController?.interactivePopGestureRecognizer?.isEnabled = true
	}
	
	func setView() {
		button.setTitle("Random", for: .normal)
		detailButton.setTitle("Detail", for: .normal)
		textLabel.text = "Random Number"
		print("set view")
	}
	
	@objc func randomNumber() {
		presenter?.randomNumber()
	}
	@objc func showDetail() {
		presenter?.showDetail(from: self)
	}
	
	func setNumber(_ number: Int) {
		textLabel.text = "Random Number: \(number)"
	}
	
	deinit {
		print("view deinit.")
	}
}
