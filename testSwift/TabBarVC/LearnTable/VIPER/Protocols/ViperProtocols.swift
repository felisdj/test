//
//  ViperProtocols.swift
//  testSwift
//
//  Created by JJsobtid on 14/5/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

protocol ViperViewProtocol: class {
 	var presenter: ViperPresenterProtocol? { get set }
	
	func setView()
	func setNumber(_ number: Int)
}

protocol ViperPresenterProtocol: class {
	var interact: ViperInteractProtocol? { get set }
	var view: ViperViewProtocol? { get set }
	var route: ViperRouteProtocol? { get set }
	
	// presenter -> view
	func viewDidLoad()
	func getNumber(_ number: Int)

	// presenter -> interact
	func randomNumber()
	
	// presenter -> route
	func showDetail(from vc: UIViewController)
}

protocol ViperRouteProtocol: class {
	func showDetail(_ number: Int, from vc: UIViewController)
	static func createViperVC() -> UIViewController
}

protocol ViperInteractProtocol: class {
	var presenter: ViperPresenterProtocol? { get set }
	
	var number: Int! { get set }
	
	func randomNumber()
	func fetchWeather()
}
