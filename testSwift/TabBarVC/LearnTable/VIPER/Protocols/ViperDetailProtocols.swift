//
//  ViperDetailProtocols.swift
//  testSwift
//
//  Created by JJsobtid on 14/5/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

protocol ViperDetailViewProtocol: class {
	var presenter: ViperDetailPresenterProtocol? { get set }
	
	func setDetail(_ number: Int)
}

protocol ViperDetailPresenterProtocol: class {
	var view: ViperDetailViewProtocol? { get set }
	var route: ViperDetailWireframeProtocol? { get set }
	var number: Int! { get set }
	
	func viewDidLoad()
	
	func backToViper(view: UIViewController)
}

protocol ViperDetailWireframeProtocol: class {
	static func createViperDetailVC(with number: Int) -> UIViewController
	func backToMainViper(view: UIViewController)
}
