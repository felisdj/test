//
//  ViperInteract.swift
//  testSwift
//
//  Created by JJsobtid on 14/5/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit
import Alamofire

class ViperInteract: NSObject, ViperInteractProtocol {
	weak var presenter: ViperPresenterProtocol?
	var number: Int!
	
	var url = "https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22"
	
	func randomNumber() {
		number = Int.random(in: 0...10)
		presenter?.getNumber(number)
	}
	
	func fetchWeather() {
		Alamofire.request(url,
						  method: .get,
						  parameters: nil,
						  encoding: JSONEncoding.default,
						  headers: nil)
			.responseJSON { (response) in
				if let result = response.result.value as? [String: Any] {
					print(result)
				}
		}
	}
	
	deinit {
		print("interact deinit")
	}
}
