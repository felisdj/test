//
//  ViperWireframe.swift
//  testSwift
//
//  Created by JJsobtid on 14/5/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

class ViperWireframe: ViperRouteProtocol {
	
	class func createViperVC() -> UIViewController {
		let viperVC = ViperViewController()
		let presenter = ViperPresenter()
		let route = ViperWireframe()
		let interact = ViperInteract()

//		let viperVC = ViperViewController(presenter: presenter)
		
		viperVC.presenter = presenter
		presenter.view = viperVC
		presenter.route = route
		presenter.interact = interact
		interact.presenter = presenter
		
		return viperVC
	}

	func showDetail(_ number: Int, from vc: UIViewController) {
		let detailVC = ViperDetailWireframe.createViperDetailVC(with: number)
		vc.present(detailVC, animated: true, completion: nil)
	}

	deinit {
		print("wireframe deinit.")
	}
}
