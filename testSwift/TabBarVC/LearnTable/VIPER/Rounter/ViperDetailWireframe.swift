//
//  ViperDetailWireframe.swift
//  testSwift
//
//  Created by JJsobtid on 16/5/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

class ViperDetailWireframe: ViperDetailWireframeProtocol {
	static func createViperDetailVC(with number: Int) -> UIViewController {
		let viperVC = ViperDetailViewController()
		let presenter = ViperDetailPresenter()
		let route = ViperDetailWireframe()
		
		viperVC.presenter = presenter
		presenter.view = viperVC
		presenter.route = route
		presenter.number = number
		
		return viperVC
	}

	func backToMainViper(view: UIViewController) {
		view.dismiss(animated: true, completion: nil)
	}
	
	deinit {
		print("wireframe detail deinit.")
	}
}
