//
//  ViperPresenter.swift
//  testSwift
//
//  Created by JJsobtid on 14/5/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

class ViperPresenter: ViperPresenterProtocol {
	
	weak var view: ViperViewProtocol?
	var interact: ViperInteractProtocol?
	var route: ViperRouteProtocol?
	
	func viewDidLoad() {
		view?.setView()
		interact?.fetchWeather()
	}
	func getNumber(_ number: Int) {
		view?.setNumber(number)
	}

	func randomNumber() {
		interact?.randomNumber()
	}
	
	func showDetail(from vc: UIViewController) {
		if let number = interact?.number {
			route?.showDetail(number, from: vc)
		} else {
			randomNumber()
		}
	}
	
	deinit {
		print("presenter deinit.")
	}
}
