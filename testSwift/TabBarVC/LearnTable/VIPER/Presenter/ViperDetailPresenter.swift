//
//  ViperDetailPresenter.swift
//  testSwift
//
//  Created by JJsobtid on 16/5/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

class ViperDetailPresenter: ViperDetailPresenterProtocol {
	
	weak var view: ViperDetailViewProtocol?
	var route: ViperDetailWireframeProtocol?
	var number: Int!
	
	func viewDidLoad() {
		view?.setDetail(number!)
	}
	
	func backToViper(view: UIViewController) {
		route?.backToMainViper(view: view)
	}
	
	deinit {
		print("detail presenter deinit")
	}
}
