//
//  MajorHomeViewController.swift
//  testSwift
//
//  Created by JSobtid on 4/10/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

let navBar: CGFloat = 44
let coverHeight: CGFloat = 150
let tabViewHeight: CGFloat = 50

class MajorHomeViewController: UIPageViewController {

	var coverView: UIView!
	var tabBarView: UIView!
	
	var firstVC: FirstMajorViewController!
	var secondVC: SecondMajorViewController!
	var thirdVC: ThirdMajorViewController!
	
	init() {
		super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
	}
	
	required init?(coder: NSCoder) {
		super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
	}
	
    override func viewDidLoad() {
		
        super.viewDidLoad()

        createView()
		setConstraints()
    }
	
	// MARK: create view
	private func createView() {
		view.backgroundColor = .white
		
		delegate = self
		dataSource = self
		
		coverView = UIView()
		coverView.backgroundColor = .gray
		coverView.alpha = 0.4
		
		tabBarView = UIView()
		tabBarView.backgroundColor = .black
		
		firstVC = FirstMajorViewController()
		firstVC.delegate = self
		secondVC = SecondMajorViewController()
		secondVC.delegate = self
		thirdVC = ThirdMajorViewController()
		
		setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
		let scrollView = view.subviews.filter { $0 is UIScrollView }.first as! UIScrollView
		scrollView.delegate = self
		
		view.addSubview(coverView)
		view.addSubview(tabBarView)
	}
	
	// MARK: set constraints
	private func setConstraints() {
		coverView.snp.makeConstraints { (make) in
			make.leading.equalTo(view.snp.leading)
			make.top.equalTo(view.snp.top)
			make.trailing.equalTo(view.snp.trailing)
			make.bottom.equalTo(tabBarView.snp.top)
		}
		tabBarView.snp.makeConstraints { (make) in
			make.leading.equalTo(view.snp.leading)
			make.trailing.equalTo(view.snp.trailing)
			make.bottom.equalTo(view.safeAreaLayoutGuide.snp.top).offset(coverHeight+tabViewHeight)
			make.height.equalTo(tabViewHeight)
		}
		view.layoutIfNeeded()
		print(tabBarView.frame)
	}
}

extension MajorHomeViewController: ViewInPageDelegate, UIScrollViewDelegate, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
	
	func scrollOffsetY(_ y: CGFloat, in vc: UIViewController) {
		print("\(tabBarView.frame.origin.y) \(y)")
		if view.safeAreaInsets.top < tabBarView.frame.origin.y - y {
			print("tringger \(y)")
			tabBarView.snp.updateConstraints { (make) in	make.bottom.equalTo(view.safeAreaLayoutGuide.snp.top).offset(coverHeight+tabViewHeight-y)
			}
			[firstVC, secondVC, thirdVC].forEach { (viewController) in
				if viewController != vc {
					if let first = viewController as? FirstMajorViewController {
						first.setTopOffset(y)
					} else if let second = viewController as? SecondMajorViewController {
						second.setTopOffset(y)
					} else if let third = viewController as? ThirdMajorViewController {

					}
				}
			}
		} else {
			print("fixed tabbar : \(y)")
			tabBarView.snp.updateConstraints { (make) in
				make.bottom.equalTo(view.safeAreaLayoutGuide.snp.top).offset(tabViewHeight)
			}
		}
		view.layoutIfNeeded()
	}
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		let positionScroll = (scrollView.contentOffset.x - scrollView.frame.width)/scrollView.frame.width
		print(positionScroll)
	}
	
	func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
		if viewController is FirstMajorViewController {
			return nil
		} else if viewController is SecondMajorViewController {
			return firstVC
		} else {
			return secondVC
		}
	}
	
	func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
		if viewController is FirstMajorViewController {
			return secondVC
		} else if viewController is SecondMajorViewController {
			return thirdVC
		} else {
			return nil
		}
	}
	
	
}
