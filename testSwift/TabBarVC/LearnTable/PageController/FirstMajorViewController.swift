//
//  FirstMajorViewController.swift
//  testSwift
//
//  Created by JSobtid on 4/10/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

protocol ViewInPageDelegate: NSObjectProtocol {
	func scrollOffsetY(_ y: CGFloat, in vc: UIViewController)
}
class FirstMajorViewController: UIViewController {
	
	var nowDisplay: Bool = false
	
	var headerSpacing: CGFloat = -200
	var collectionView: UICollectionView!
	
	weak var delegate: ViewInPageDelegate?
	
    override func viewDidLoad() {
        super.viewDidLoad()

        createView()
		setConstraints()
    }
   
	func setTopOffset(_ offset: CGFloat) {
		if collectionView != nil {
			collectionView.setContentOffset(CGPoint(x: 0, y: offset), animated: false)
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		nowDisplay = true
	}
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		nowDisplay = false
	}
	
	// MARK: create view
	private func createView() {
		view.backgroundColor = .red
		
		let flowLayout = UICollectionViewFlowLayout()
		flowLayout.itemSize = CGSize(width: view.frame.width/2, height: 100)
		flowLayout.minimumLineSpacing = 0
		flowLayout.minimumInteritemSpacing = 0
		
		collectionView = UICollectionView(frame: CGRect(), collectionViewLayout: flowLayout)
		collectionView.backgroundColor = .white
		collectionView.delegate = self
		collectionView.dataSource = self
		collectionView.contentInset = UIEdgeInsets(top: -headerSpacing, left: 0, bottom: 0, right: 0)
		collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "reuse")
		view.addSubview(collectionView)
	}
	
	// MARK: set constraints
	private func setConstraints() {
		collectionView.snp.makeConstraints { (make) in
			make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
			make.leading.equalTo(view.snp.leading)
			make.trailing.equalTo(view.snp.trailing)
			make.bottom.equalTo(view.snp.bottom)
		}
	}

}

extension FirstMajorViewController: UICollectionViewDelegate, UICollectionViewDataSource {
	
	func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
		headerSpacing = scrollView.contentOffset.y
	}
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		if nowDisplay {
			delegate?.scrollOffsetY(scrollView.contentOffset.y-headerSpacing, in: self)
//			print("collection FIRST scroll offset: \(scrollView.contentOffset.y)")
		}
	}
//	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//		headerSpacing = scrollView.contentOffset.y
//		print("stop scroll")
//	}
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return 20
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reuse", for: indexPath)
		cell.contentView.backgroundColor = .gray
		cell.testBorder(.white)
		return cell
	}
	
	
}
