//
//  ThirdMajorViewController.swift
//  testSwift
//
//  Created by JSobtid on 4/10/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

class ThirdMajorViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

			createView()
			setConstraints()
		}

		// MARK: create view
		private func createView() {
			view.backgroundColor = .blue
		}

		// MARK: set constraints
		private func setConstraints() {
		 
		}
}
