//
//  SecondMajorViewController.swift
//  testSwift
//
//  Created by JSobtid on 4/10/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

class SecondMajorViewController: UIViewController {

	var nowDisplay: Bool = false
	var nowContentOffsetY: CGFloat = 0
	
	var headerSpacing: CGFloat = -200
	var collectionView: UICollectionView!
	
	var firsttime = true
	
	weak var delegate: ViewInPageDelegate?
	
	override func viewDidLoad() {
		super.viewDidLoad()

		createView()
		setConstraints()
	}
   
	func setTopOffset(_ offset: CGFloat) {
		if collectionView != nil {
			print("second \(collectionView.contentOffset.y)")
			if offset < collectionView.contentOffset.y {
				return
			}
			collectionView.setContentOffset(CGPoint(x: 0, y: offset), animated: false)
		} else {
			headerSpacing = offset > -tabViewHeight ? -tabViewHeight : offset
		}
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		if firsttime {
			firsttime = false
			collectionView.contentOffset = CGPoint(x: 0, y: headerSpacing)
		}
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		nowDisplay = true
	}
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		nowDisplay = false
	}
	
	// MARK: create view
	private func createView() {
		view.backgroundColor = .red
		
		let flowLayout = UICollectionViewFlowLayout()
		flowLayout.itemSize = CGSize(width: view.frame.width/2, height: 100)
		flowLayout.minimumLineSpacing = 0
		flowLayout.minimumInteritemSpacing = 0
		
		collectionView = UICollectionView(frame: CGRect(), collectionViewLayout: flowLayout)
		collectionView.backgroundColor = .white
		collectionView.delegate = self
		collectionView.dataSource = self
		collectionView.contentInset = UIEdgeInsets(top: tabViewHeight+coverHeight, left: 0, bottom: 0, right: 0)
		collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "reuse")
		view.addSubview(collectionView)
	}
	
	// MARK: set constraints
	private func setConstraints() {
		collectionView.snp.makeConstraints { (make) in
			make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
			make.leading.equalTo(view.snp.leading)
			make.trailing.equalTo(view.snp.trailing)
			make.bottom.equalTo(view.snp.bottom)
		}
	}

}

extension SecondMajorViewController: UICollectionViewDelegate, UICollectionViewDataSource {
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		if nowDisplay {
			delegate?.scrollOffsetY(scrollView.contentOffset.y, in: self)
			print("collection SECOND scroll offset: \(scrollView.contentOffset.y)")
		}
		
	}
	func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
		print("collection SECOND end offset: \(scrollView.contentOffset.y)")
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return 20
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reuse", for: indexPath)
		cell.contentView.backgroundColor = .blue
		cell.testBorder(.white)
		return cell
	}
	
	
}
