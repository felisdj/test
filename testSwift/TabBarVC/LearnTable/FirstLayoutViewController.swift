//
//  FirstLayoutViewController.swift
//  testSwift
//
//  Created by JJsobtid on 23/1/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit
import Lottie

class FirstLayoutViewController: UIViewController {

	let floatRandom = Float.random(in: 0...1)
	
	var leftView, rightView: UIView!
	var testCircleScoreView: CircleScoreView!
	var label: UILabel!
	
	var firstStarView, secondStarView, thirdStarView, resultBg: AnimationView!
	
	override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews()
		print("will layout subviews")
		testCircleScoreView.center = view.center
	}
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		print("did layout subviews")
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()

        createView()
		setConstraints()
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		tabBarController?.tabBar.isHidden = true
		navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
		navigationController?.interactivePopGestureRecognizer?.isEnabled = true
		
		testCircleScoreView.runAnimation(per: CGFloat(floatRandom))
		playStar1()
		perform(#selector(playStar2), with: nil, afterDelay: 1)
		perform(#selector(playStar3), with: nil, afterDelay: 2)
		resultBg.play(fromFrame: 0, toFrame: 120, loopMode: .playOnce) { (complete) in
			self.resultBg.play(fromFrame: 120, toFrame: 100, loopMode: .autoReverse, completion: nil)
		}
	}
	func playStar1() {
		if floatRandom > 0.33 {
			firstStarView.play(toFrame: 127)
		} else {
			let atFrame = CGFloat(floatRandom*25.0/127.0)
			firstStarView.play(toFrame: atFrame)
		}
	}
	@objc func playStar2() {
		if floatRandom - 0.33 > 0 {
			let percentPlay = floatRandom - 0.33
			if percentPlay > 0.67 {
				secondStarView.play(toFrame: 127)
			} else {
				let atFrame = CGFloat(percentPlay*25.0/127.0)
				secondStarView.play(toFrame: atFrame)
			}
		}
	}
	@objc func playStar3() {
		if floatRandom - 0.67 > 0 {
			let percentPlay = floatRandom - 0.67
			if percentPlay >= 1 {
				secondStarView.play(toFrame: 127)
			} else {
				let atFrame = CGFloat(percentPlay*25.0/127.0)
				secondStarView.play(toFrame: atFrame)
			}
		}
	}
	// MARK: - Create View
	func createView() {
		view.backgroundColor = .white
		
		testCircleScoreView = CircleScoreView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
		label = UILabel()
		label.text = "\(floatRandom)"
		label.textColor = .black
		
		firstStarView = AnimationView(name: "Star1")
		firstStarView.frame = CGRect(origin: CGPoint(), size: CGSize(width: 100, height: 100))
		firstStarView.contentMode = .scaleAspectFill
		firstStarView.loopMode = .playOnce
		firstStarView.stop()
		secondStarView = AnimationView(name: "Star2")
		secondStarView.frame = CGRect(origin: CGPoint(), size: CGSize(width: 100, height: 100))
		secondStarView.contentMode = .scaleAspectFill
		secondStarView.loopMode = .playOnce
		secondStarView.stop()
		thirdStarView = AnimationView(name: "Star3")
		thirdStarView.frame = CGRect(origin: CGPoint(), size: CGSize(width: 100, height: 100))
		thirdStarView.contentMode = .scaleAspectFill
		thirdStarView.loopMode = .playOnce
		thirdStarView.stop()
		resultBg = AnimationView(name: "ResultBG")
		resultBg.frame = CGRect(origin: CGPoint(), size: CGSize(width: 300, height: 100))
		resultBg.contentMode = .scaleAspectFill
		resultBg.loopMode = .playOnce
		resultBg.stop()
		
		view.addSubview(resultBg)
		view.addSubview(testCircleScoreView)
		view.addSubview(label)
		view.addSubview(firstStarView)
		view.addSubview(secondStarView)
		view.addSubview(thirdStarView)
		
	}
	
	// MARK: - Set View Constraints
	func setConstraints() {
		label.snp.makeConstraints { (make) in
			make.centerX.equalTo(view.snp.centerX)
			make.size.equalTo(CGSize(width: 100, height: 100))
			make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
		}
		secondStarView.snp.makeConstraints { (make) in
			make.size.equalTo(CGSize(width: 100, height: 100))
			make.centerX.equalTo(view.snp.centerX)
			make.centerY.equalTo(view.snp.centerY).offset(40)
		}
		firstStarView.snp.makeConstraints { (make) in
			make.size.equalTo(secondStarView.snp.size)
			make.centerY.equalTo(secondStarView.snp.centerY).offset(16)
			make.centerX.equalTo(secondStarView.snp.centerX).offset(-40)
		}
		thirdStarView.snp.makeConstraints { (make) in
			make.size.equalTo(secondStarView.snp.size)
			make.centerY.equalTo(secondStarView.snp.centerY).offset(16)
			make.centerX.equalTo(secondStarView.snp.centerX).offset(40)
		}
		resultBg.snp.makeConstraints { (make) in
			make.size.equalTo(CGSize(width: 300, height: 100))
			make.center.equalTo(view.snp.center)
		}
		view.layoutIfNeeded()
	}
	
	// MARK: Deinit
	deinit {
		print("First Layout Deinit.")
	}
}
