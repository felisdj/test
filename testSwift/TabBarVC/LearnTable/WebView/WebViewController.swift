//
//  WebViewController.swift
//  
//
//  Created by JSobitd on 6/9/2562 BE.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate, UIGestureRecognizerDelegate {

    let solution: Bool = Bool.random()
    let isMath: Bool = Bool.random()
    var oldButtons: [String] = []
    let question = """
	<script>
	  function speakWord() {
	var textToSpeech = document.getElementById("front").innerHTML;
	const lang = 'ja-JP'
	const voiceIndex = 0

	const speak = async text => {
		if (!speechSynthesis) {
			return
		}
		const message = new SpeechSynthesisUtterance(text)
		message.voice = await chooseVoice()
		speechSynthesis.speak(message)
	}

	const getVoices = () => {
		return new Promise(resolve => {
			let voices = speechSynthesis.getVoices()
			if (voices.length) {
				resolve(voices)
				return
			}
			speechSynthesis.onvoiceschanged = () => {
				voices = speechSynthesis.getVoices()
				resolve(voices)
			}
		})
	}

	const chooseVoice = async () => {
		const voices = (await getVoices()).filter(voice => voice.lang == lang)

		return new Promise(resolve => {
			resolve(voices[voiceIndex])
		})
	}

	speak(textToSpeech)
  }
	  
	  function showMeaning() {
	   
	  var status = document.getElementById("back").style.display || "none";
	 
	  if(status == "none") {
	  document.getElementById("back").style.display = "inline";
	  document.getElementById("showbutton").innerHTML = "hide meaning";
	  } else {
	  document.getElementById("back").style.display = "none";
		document.getElementById("showbutton").innerHTML = "show meaning";
	  }
	  }
	</script>

	<div id="front" class="front">
	  わたし
	</div>
	<div class="reading">
	  reading
	  <button onclick="speakWord()">speak</button>
	</div>
	<div>
	<button id="showbutton" onclick="showMeaning()">show meaning</button>
	</div>
	<div id="back" class="back">
	  back
	</div>
"""
	/*"""
        <p>
    <strong>ข้อ 19.&nbsp;&nbsp;</strong><math xmlns="http://www.w3.org/1998/Math/MathML">
        <mfrac>
            <mn>4</mn>
            <mn>7</mn>
        </mfrac>
        <mo>&#xA0;</mo>
        <mo>,</mo>
        <mo>&#xA0;</mo>
        <mfrac>
            <mn>12</mn>
            <mn>15</mn>
        </mfrac>
        <mo>&#xA0;</mo>
        <mo>,</mo>
        <mo>&#xA0;</mo>
        <mfrac>
            <mn>29</mn>
            <mn>39</mn>
        </mfrac>
        <mo>&#xA0;</mo>
        <mo>,</mo>
        <mo>&#xA0;</mo>
        <mfrac>
            <mn>71</mn>
            <mn>97</mn>
        </mfrac>
        <mo>&#xA0;</mo>
        <mo>,</mo>
        <mo>&#xA0;</mo>
        <mo>?</mo>
    </math>
    </p>
    <div class="choice" id="button:1" onclick="window.location.href='button:1'">
    &nbsp;&nbsp;ก.&nbsp;
    <math xmlns="http://www.w3.org/1998/Math/MathML">
        <mfrac>
            <mn>168</mn>
            <mn>243</mn>
        </mfrac>
    </math>
    </div>
    <div class="choice" id="button:2" onclick="window.location.href='button:2'">
    &nbsp;&nbsp;ข.&nbsp;
    <math xmlns="http://www.w3.org/1998/Math/MathML">
        <mfrac>
            <mn>172</mn>
            <mn>243</mn>
        </mfrac>
    </math>
    </div>
    <div class="choice" id="button:3" onclick="window.location.href='button:3'">
    <span>&nbsp;&nbsp;ค.&nbsp;</span>
    <math xmlns="http://www.w3.org/1998/Math/MathML\">
        <mfrac>
            <mn>168</mn>
            <mn>239</mn>
        </mfrac>
    </math>
    </div>
    <div class="choice" id="button:4" onclick="window.location.href='button:4'">
    <span>&nbsp;&nbsp;ง.&nbsp;</span>
    <math xmlns="http://www.w3.org/1998/Math/MathML">
        <mfrac>
            <mn>172</mn>
            <mn>239</mn>
        </mfrac>
    </math>
    </div>
    <div class="correctChoice" id="button:5" onclick="window.location.href='button:5'">
    <span>&nbsp;&nbsp;ง.&nbsp;</span>
    <math xmlns="http://www.w3.org/1998/Math/MathML">
        <mfrac>
            <mn>172</mn>
            <mn>239</mn>
        </mfrac>
    </math>
    <img class="imgResult"
        src="https://cdn3.iconfinder.com/data/icons/flat-actions-icons-9/792/Tick_Mark_Dark-512.png" />
    </div>
    <div class="wrongChoice" id="button:6" onclick="window.location.href='button:6'">
    <span>&nbsp;&nbsp;ง.&nbsp;โจทย์ภาษาไทยยาวๆแบบไม่มีเว้นวรรคตอนเพื่อทดสอบการตัดคำของเว็บ</span>
    <img class="imgResult"
        src="https://cdn.iconscout.com/icon/free/png-256/false-delete-remove-cross-wrong-7-16030.png" />
    </div>
    <div class="wrongChoice" id="button:6" onclick="window.location.href='button:6'">
    <img src="https://api.sobtid.me/image/exam/qx7w3guicbum0fjthhwpswa10qbkp0.png" />
    <img class="imgResult"
        src="https://cdn.iconscout.com/icon/free/png-256/false-delete-remove-cross-wrong-7-16030.png" />

    </div>

    <div><img src="https://api.sobtid.me/image/exam/qx7w3guicbum0fjthhwpswa10qbkp0.png"
        style="width: 85%; max-width:500px;" /></div>
    <div>โดยการให้เหตุผลแบบอุปนัย a &ndash; b + c มีค่าเท่ากับข้อใดต่อไปนี้</div>
    </div>

    19. กำหนดให้<br />
    <img src="https://api.sobtid.me/image/exam/s5a3wctgoedyd44g4967yx14yvnd7r.png"
    style="width: 85%; max-width:500px;" /><br />
    แล้วในรูปที่ 8 มีจำนวนจุดกี่จุด</div>
    """
    */
    let disableSelectionScriptString = "document.documentElement.style.webkitUserSelect='none';"
    
    var textView: UITextView!
    var webViewText: WKWebView!

    var webViewLoading: UIActivityIndicatorView!
    
    var moveView: UIView!
    var leftButton, rightButton, upButton, downButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createView()
        setConstraints()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        if let data = setHTMLText().data(using: .utf8) {
            webViewText.load(data, mimeType: "text/html", characterEncodingName: "UTF-8", baseURL: Bundle.main.resourceURL!)
            textView.text = setHTMLText()
            textView.attributedText = htmlToAttributedString
        }
    }
    
    // MARK: - Create UI
    private func createView() {
        view.backgroundColor = .white
        
        let disableSelectionScript = WKUserScript(source: disableSelectionScriptString, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let controller = WKUserContentController()
        controller.addUserScript(disableSelectionScript)
        
        let configuration = WKWebViewConfiguration()
        configuration.userContentController = controller
        webViewText = WKWebView(frame: CGRect(), configuration: configuration)
        webViewText.navigationDelegate = self
        webViewText.backgroundColor = .clear
        webViewText.scrollView.contentInset = UIEdgeInsets(top: MARGIN, left: 0, bottom: MARGIN, right: 0)
        webViewText.scrollView.layer.masksToBounds = false
        webViewText.scrollView.delegate = self
        webViewText.isOpaque = false
        webViewText.scrollView.showsVerticalScrollIndicator = false
        webViewText.scrollView.showsHorizontalScrollIndicator = false
//        webViewText.scrollView.isScrollEnabled = false
        textView = UITextView()
        
        moveView = UIView(frame: CGRect(origin: CGPoint(), size: CGSize(width: 50, height: 50)))
        moveView.backgroundColor = .black
        leftButton = UIButton()
        leftButton.setTitle("⬅️", for: .normal)
        leftButton.addTarget(self, action: #selector(arrowAction(_:)), for: .touchUpInside)
        rightButton = UIButton()
        rightButton.setTitle("➡️", for: .normal)
        rightButton.addTarget(self, action: #selector(arrowAction(_:)), for: .touchUpInside)
        upButton = UIButton()
        upButton.setTitle("⬆️", for: .normal)
        upButton.addTarget(self, action: #selector(arrowAction(_:)), for: .touchUpInside)
        downButton = UIButton()
        downButton.setTitle("⬇️", for: .normal)
        downButton.addTarget(self, action: #selector(arrowAction(_:)), for: .touchUpInside)
        
        view.addSubview(webViewText)
        view.addSubview(leftButton)
        view.addSubview(rightButton)
        view.addSubview(upButton)
        view.addSubview(downButton)
        view.addSubview(moveView)
//        view.addSubview(textView)
        
        leftButton.snp.makeConstraints { (make) in
            make.size.equalTo(CGSize(width: 30, height: 30))
            make.centerX.equalTo(view.snp.centerX).offset(-50)
            make.centerY.equalTo(view.snp.centerY)
        }
        rightButton.snp.makeConstraints { (make) in
            make.size.equalTo(leftButton.snp.size)
            make.centerY.equalTo(leftButton.snp.centerY)
            make.centerX.equalTo(view.snp.centerX).offset(50)
        }
        downButton.snp.makeConstraints { (make) in
            make.size.equalTo(leftButton.snp.size)
            make.centerY.equalTo(view.snp.centerY).offset(50)
            make.centerX.equalTo(view.snp.centerX)
        }
        upButton.snp.makeConstraints { (make) in
            make.size.equalTo(leftButton.snp.size)
            make.centerY.equalTo(view.snp.centerY).offset(-50)
            make.centerX.equalTo(view.snp.centerX)
        }
    }
    
    @objc private func arrowAction(_ sender: UIButton) {
        let previousOriginalPoint = moveView.frame.origin
        if sender == leftButton {
            UIView.animate(withDuration: 0.25) {
                self.moveView.frame = CGRect(origin: CGPoint(x: previousOriginalPoint.x - 10, y: previousOriginalPoint.y), size: self.moveView.frame.size)
            }
        } else if sender == rightButton {
            UIView.animate(withDuration: 0.25) {
                self.moveView.frame = CGRect(origin: CGPoint(x: previousOriginalPoint.x + 10, y: previousOriginalPoint.y), size: self.moveView.frame.size)
            }
        } else if sender == upButton {
            moveView.frame = CGRect(origin: CGPoint(x: previousOriginalPoint.x, y: previousOriginalPoint.y - 10), size: moveView.frame.size)
        } else {
            moveView.frame = CGRect(origin: CGPoint(x: previousOriginalPoint.x, y: previousOriginalPoint.y + 10), size: moveView.frame.size)
        }
    }
    
    // MARK: - Set View Constraints
    private func setConstraints() {
        webViewText.snp.makeConstraints { (make) in
            make.edges.equalTo(view.snp.edges)
        }
    }
    
    private func setHTMLText() -> String {
        let htmlString = "<!doctype html xmlns:fb=@\"http://ogp.me/ns/fb#\"><head><meta charset=\"utf-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, shrink-to-fit=no\(isMath ? "" : ", user-scalable=no")\"><link href=\"style.css\" rel=\"stylesheet\"></head><body>"
        let retrunstr = "\(htmlString)<div id=\"content\">\(question)</div></body></html>"
        
        return retrunstr
    }
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = setHTMLText().data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("nav")
    }
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = navigationAction.request.url?.relativeString {
            if (url.hasPrefix("button")) && !solution {
                var js = """
                document.getElementById("\(url)").classList = "selectChoice";
                """
                if !oldButtons.contains(url) {
                    if !oldButtons.isEmpty && oldButtons.count >= 1 {
                        js += """
                        document.getElementById("\(oldButtons.removeFirst())").classList = "choice";
                        """
                    }
                    oldButtons.append(url)
                }
                print(js, oldButtons)
                webView.evaluateJavaScript(js) { (result, error) in
                    guard let err = error else {
                        return
                    }
                    print(err)
                }
                
                decisionHandler(.cancel)
            } else {
                decisionHandler(.allow)
            }
        } else {
            decisionHandler(.allow)
        }
        
    }
    
    // MARK: - Deinit
    deinit {
        print("deinit web vc.")
    }

}

extension WebViewController: UIScrollViewDelegate {
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        if !isMath {
            scrollView.pinchGestureRecognizer?.isEnabled = false
        }
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return nil
    }
//    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
//        return false
//    }
}
