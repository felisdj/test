//
//  PairQuestionViewController.swift
//  testSwift
//
//  Created by JSobitd on 10/9/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

class PairQuestionViewController: UIViewController, UIGestureRecognizerDelegate {

    let questionDimension: CGFloat = 148
    let answerDimension: CGFloat = 120
    
    var pairView: UIView!
    var scrollView: UIScrollView!
    var questionTitleLabel, answerTitleLabel: UILabel!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        createView()
        setConstraints()
    }

    // MARK: - Create UI
    private func createView() {
        pairView = UIView()
        
        scrollView = UIScrollView()
        
        questionTitleLabel = UILabel()
        questionTitleLabel.text = "โจทย์"
        answerTitleLabel = UILabel()
        answerTitleLabel.text = "คำตอบ"
        
        view.addSubview(pairView)
        pairView.addSubview(scrollView)
        scrollView.addSubview(questionTitleLabel)
        scrollView.addSubview(answerTitleLabel)
        
    }
    
    // MARK: - Set View Constraints
    private func setConstraints() {
        pairView.snp.makeConstraints { (make) in
            make.edges.equalTo(view.snp.edges)
        }
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalTo(pairView.snp.edges)
        }
        questionTitleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(pairView.safeAreaLayoutGuide.snp.leading).offset(20)
            make.top.equalTo(pairView.snp.top).offset(20)
        }
        answerTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(pairView.snp.top).offset(20)
            make.leading.equalTo(pairView.snp.centerX).offset(20)
            make.trailing.equalTo(pairView.safeAreaLayoutGuide.snp.trailing).offset(-20)
        }
        view.layoutIfNeeded()
        
    }
    
    // MARK: - Deinit
    deinit {
        
    }
    
    enum PairAnswerState {
        case empty, select, answer
    }
    
    class PairAnswerView: UIView {
        
        var state: PairAnswerState = .empty
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            createView()
            setConstraints()
        }
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            createView()
            setConstraints()
        }
        
        // MARK: - Create UI
        private func createView() {
            
        }
        
        // MARK: - Set View Constraints
        private func setConstraints() {
            
        }
        
        // MARK: - Deinit
        deinit {
            
        }
    }
}
