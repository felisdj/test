//
//  MLMainViewController.swift
//  testSwift
//
//  Created by JJsobtid on 27/5/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit
import AVFoundation
// [START import_vision]
import FirebaseMLVision
// [END import_vision]

import FirebaseMLVisionObjectDetection
import FirebaseMLVisionAutoML
import FirebaseMLCommon

class MLMainViewController: UIViewController, AVCapturePhotoCaptureDelegate {

	var photoPreviewImageView: UIView!
	
	var session: AVCaptureSession?
	var stillImageOutput: AVCapturePhotoOutput?
	var videoPreviewLayer: AVCaptureVideoPreviewLayer?
	
	override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
		super.viewWillTransition(to: size, with: coordinator)
		coordinator.animate(alongsideTransition: nil) { [weak self](_) in
			if let weakSelf = self {
				if UIDevice.current.orientation == .landscapeLeft {
					weakSelf.videoPreviewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.landscapeRight
				} else if UIDevice.current.orientation == .landscapeRight {
					weakSelf.videoPreviewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.landscapeLeft
				} else {
					weakSelf.videoPreviewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
				}
			}
			self?.photoPreviewImageView.frame = self?.view.bounds ?? CGRect()
			self?.videoPreviewLayer!.frame = self?.photoPreviewImageView.bounds ?? CGRect()
			
		}
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()

		view.backgroundColor = .white
		
		photoPreviewImageView = UIView(frame: view.bounds)
		photoPreviewImageView.backgroundColor = .gray
		view.addSubview(photoPreviewImageView)
		
		session = AVCaptureSession()
		session?.sessionPreset = AVCaptureSession.Preset.photo
		
		let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
		var error: NSError?
		var input: AVCaptureDeviceInput!
		do {
			input = try AVCaptureDeviceInput(device: backCamera!)
		} catch let err as NSError {
			error = err
			input = nil
			print(err.localizedDescription)
		}
		if error == nil && session!.canAddInput(input) {
			session!.addInput(input)
			stillImageOutput = AVCapturePhotoOutput()
//			stillImageOutput?.setting = [AVVideoCodecKey:  AVVideoCodecJPEG]
			if session!.canAddOutput(stillImageOutput!) {
				session!.addOutput(stillImageOutput!)
				videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session!)
				videoPreviewLayer!.videoGravity = AVLayerVideoGravity.resizeAspect
				videoPreviewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
				photoPreviewImageView.layer.addSublayer(videoPreviewLayer!)
				session!.startRunning()
				
//				let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
//				stillImageOutput!.capturePhoto(with: settings, delegate: self)
			}
		}
		
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		tabBarController?.tabBar.isHidden = true
		navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
		navigationController?.interactivePopGestureRecognizer?.isEnabled = true
		videoPreviewLayer!.frame = photoPreviewImageView.bounds
		print(photoPreviewImageView.frame)
	}

	func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
		
		guard let imageData = photo.fileDataRepresentation()
			else { return }
		
		let image = UIImage(data: imageData)
		
	}
	
	deinit {
		session!.stopRunning()
		print("session deinit")
	}
}
