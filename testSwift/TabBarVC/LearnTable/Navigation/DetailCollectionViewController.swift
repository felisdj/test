//
//  DetailCollectionViewController.swift
//  testSwift
//
//  Created by JJsobtid on 12/7/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit
import MXSegmentedPager

private let reuseIdentifier = "Cell"

extension UIImageView {
    var imageSize: CGSize {
        return image?.size ?? CGSize()
    }
    var ratioW2H: CGFloat {
        return imageSize.width/imageSize.height
    }
}

class DetailCollectionViewController: UICollectionViewController {
	
	var topView: UIView!
    var imageView: UIImageView!
    
	var pager: MXSegmentedPager!
	
	var isVertical: Bool {
		switch UIDevice.current.orientation {
		case .portrait, .portraitUpsideDown:
			return true
		default:
			return false
		}
	}
	
	var titleShow = false
	override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
		super.willTransition(to: newCollection, with: coordinator)
		coordinator.animate(alongsideTransition: { (context) in
//            self.topView.snp.updateConstraints { (make) in
//                make.height.equalTo(self.navigationBottom+(self.isVertical ? 20 : 0))
//            }
			print("top area: \(topArea)")
		}) { (context) in
		}
	}

	override init(collectionViewLayout layout: UICollectionViewLayout) {
		super.init(collectionViewLayout: layout)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
        super.viewDidLoad()
	
//		navigationItem.title = "Detail"
		
		navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
		
		let trailView = UIView()
		let trailTitle = UILabel()
		trailTitle.text = Bool.random() ? "00:00" : "9:00"
		let trailImage = UIImageView(image: UIImage(named: "bell"))
		trailImage.contentMode = .scaleAspectFit
		trailView.addSubview(trailTitle)
		trailView.addSubview(trailImage)
		trailImage.snp.makeConstraints { (make) in
			make.size.equalTo(CGSize(width: 16, height: 16))
			make.trailing.equalTo(trailView.snp.trailing)
			make.centerY.equalTo(trailView.snp.centerY)
		}
		trailTitle.snp.makeConstraints { (make) in
			make.trailing.equalTo(trailImage.snp.leading).offset(-2)
			make.centerY.equalTo(trailView.snp.centerY)
			make.leading.equalTo(trailView.snp.leading)
		}
		navigationItem.rightBarButtonItem = UIBarButtonItem(customView: trailView)
		
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.itemSize = CGSize(width: 150, height: 150)
//            layout.minimumLineSpacing = 0
        }
        collectionView.backgroundColor = .white
        collectionView.alwaysBounceVertical = true
        self.collectionView?.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        print("\(view.frame) \(collectionView.frame)")
        topView = UIView()
        topView.backgroundColor = .black
		topView.alpha = 0.5
        
        imageView = UIImageView(image: UIImage(named: "group804"))
        imageView.contentMode = .scaleAspectFit
        topView.addSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            make.trailing.equalTo(topView.snp.trailing)
            make.top.greaterThanOrEqualTo(topView.snp.top)
            make.bottom.equalTo(topView.snp.bottom)
            make.width.equalTo(imageView.snp.height).multipliedBy(imageView.ratioW2H)
            make.leading.greaterThanOrEqualTo(topView.snp.trailing).dividedBy(4)
        }
        view.addSubview(topView)
        topView.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.top)
            make.leading.equalTo(view.snp.leading)
			make.bottom.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.trailing.equalTo(view.snp.trailing)
        }
        
        view.layoutIfNeeded()
        
		let headerView = UIView()
		headerView.alpha = 0.5
		headerView.backgroundColor = .blue
		
		let segmentedPager = MXSegmentedPager()
		segmentedPager.parallaxHeader.view = headerView
		segmentedPager.parallaxHeader.height = 150
		segmentedPager.parallaxHeader.mode = .fill
		segmentedPager.parallaxHeader.minimumHeight = 0
		segmentedPager.parallaxHeader.delegate = self
		
		segmentedPager.dataSource = self
		segmentedPager.delegate = self
	
		view.addSubview(segmentedPager)
		segmentedPager.snp.makeConstraints { (make) in
			make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
			make.leading.equalTo(view.snp.leading)
			make.trailing.equalTo(view.snp.trailing)
			make.bottom.equalTo(view.snp.bottom)
		}
        // Do any additional setup after loading the view.
//        collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
    }
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
    }
    
	override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print(scrollView.contentOffset)
		if scrollView.contentOffset.y < -topView.frame.height {
			if titleShow {
				let fadeTextAnimation = CATransition()
				fadeTextAnimation.duration = 0.5
				fadeTextAnimation.type = .fade
				
				navigationController?.navigationBar.layer.add(fadeTextAnimation, forKey: "text")
				navigationItem.title = ""
				topView.layer.add(fadeTextAnimation, forKey: "topView")
				topView.alpha = 0.0
				titleShow = false
			}
		} else {
			if !titleShow {
				let fadeTextAnimation = CATransition()
				fadeTextAnimation.duration = 0.5
				fadeTextAnimation.type = .fade
				
				navigationController?.navigationBar.layer.add(fadeTextAnimation, forKey: "text")
				navigationItem.title = "Detail"
				topView.layer.add(fadeTextAnimation, forKey: "topView")
				topView.alpha = 0.5
				titleShow = true
			}
		}
//        collectionView.visibleCells.forEach { (cell) in
//            let cellCenterX = collectionView.convert(cell.center, to: view).x
//            let distance = abs(cellCenterX - collectionView.center.x)
//            let tolerance : CGFloat = 0.02
//            var scale = 1.0 + tolerance - (0.105 * distance/collectionView.center.x)
//
//            if(scale > 1.0){
//                scale = 1.0
//            }
//            if(scale < 0.860091){
//                scale = 0.860091
//            }
//            print("\(distance) \(scale)")
//            cell.transform = CGAffineTransform(scaleX: scale, y: scale)
//        }
	}

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 50
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
    
        // Configure the cell
    	cell.contentView.backgroundColor = .black
        cell.layer.borderColor = UIColor.red.cgColor
        cell.layer.borderWidth = 4.0
        return cell
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}

extension DetailCollectionViewController: MXSegmentedPagerDataSource, MXSegmentedPagerDelegate, MXParallaxHeaderDelegate {
	
	func parallaxHeaderDidScroll(_ parallaxHeader: MXParallaxHeader) {
	
	}
	
	func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
		return 3
	}

	// Asks the data source for a title realted to a particular page of the segmented pager.
	func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
		return "Page \(index)"
	}

	// Asks the data source for a view to insert in a particular page of the pager.
	func segmentedPager(_ segmentedPager: MXSegmentedPager, viewForPageAt index: Int) -> UIView {
		let label = UILabel()
		label.text = "Page \(index)"
		label.textAlignment = .center
		return label
	}
	
	func segmentedPager(_ segmentedPager: MXSegmentedPager, didEndDraggingWith parallaxHeader: MXParallaxHeader) {
//		print(parallaxHeader.progress)
//		UIView.animate(withDuration: 0.25) {

//		}
	}
}

//
//  PageCollectionFlowLayout.swift
//  samitivej
//
//  Created by JJsobtid on 9/5/2561 BE.
//  Copyright © 2561 Jackthip Pureesatian. All rights reserved.
//

import UIKit

class PageCollectionFlowLayout: UICollectionViewFlowLayout {
    
    private var lastCollectionViewSize: CGSize = CGSize.zero
    
    public var scalingOffset: CGFloat = 200 //for offsets >= scalingOffset scale factor is minimumScaleFactor
    public var minimumScaleFactor: CGFloat = 0.5
    public var scaleItems: Bool = true
    
    
    static func configureLayout(collectionView: UICollectionView, itemSize: CGSize, minimumLineSpacing: CGFloat) -> PageCollectionFlowLayout {
        let layout = PageCollectionFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = minimumLineSpacing
        layout.itemSize = itemSize
        
        collectionView.decelerationRate = .fast
        collectionView.collectionViewLayout = layout
        collectionView.backgroundColor = .white
        return layout
    }
    
    override public func invalidateLayout(with context: UICollectionViewLayoutInvalidationContext) {
        super.invalidateLayout(with: context)
        
        if self.collectionView == nil {
            return
        }
        
        let currentCollectionViewSize = self.collectionView!.bounds.size
        
        if !currentCollectionViewSize.equalTo(self.lastCollectionViewSize) {
            self.configureInset()
            self.lastCollectionViewSize = currentCollectionViewSize
        }
    }
    
    private func configureInset() -> Void {
        if self.collectionView == nil {
            return
        }
        
        let inset = self.collectionView!.bounds.size.width / 2 - self.itemSize.width / 2
        self.collectionView!.contentInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        self.collectionView!.contentOffset = CGPoint(x: -inset, y: 0)
    }
    
    public override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        if self.collectionView == nil {
            return proposedContentOffset
        }
        
        let collectionViewSize = self.collectionView!.bounds.size
        let proposedRect = CGRect(x: proposedContentOffset.x, y: 0, width: collectionViewSize.width, height: collectionViewSize.height)
        
        let layoutAttributes = self.layoutAttributesForElements(in: proposedRect)
        
        if layoutAttributes == nil {
            return proposedContentOffset
        }
        
        var candidateAttributes: UICollectionViewLayoutAttributes?
        let proposedContentOffsetCenterX = proposedContentOffset.x + collectionViewSize.width / 2
        
        for attributes: UICollectionViewLayoutAttributes in layoutAttributes! {
            if attributes.representedElementCategory != .cell {
                continue
            }
            
            if candidateAttributes == nil {
                candidateAttributes = attributes
                continue
            }
            
            if abs(attributes.center.x - proposedContentOffsetCenterX) < abs(candidateAttributes!.center.x - proposedContentOffsetCenterX) {
                candidateAttributes = attributes
            }
        }
        
        if candidateAttributes == nil {
            return proposedContentOffset
        }
        
        var newOffsetX = candidateAttributes!.center.x - self.collectionView!.bounds.size.width / 2
        
        let offset = newOffsetX - self.collectionView!.contentOffset.x
        
        if (velocity.x < 0 && offset > 0) || (velocity.x > 0 && offset < 0) {
            let pageWidth = self.itemSize.width + self.minimumLineSpacing
            newOffsetX += velocity.x > 0 ? pageWidth : -pageWidth
        }
        
        return CGPoint(x: newOffsetX, y: proposedContentOffset.y)
    }
    
    public override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    public override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        if !self.scaleItems || self.collectionView == nil {
            return super.layoutAttributesForElements(in: rect)
        }
        
        let superAttributes = super.layoutAttributesForElements(in: rect)
        
        if superAttributes == nil {
            return nil
        }
        
        let contentOffset = self.collectionView!.contentOffset
        let size = self.collectionView!.bounds.size
        
        let visibleRect = CGRect(x: contentOffset.x, y: contentOffset.y, width: size.width, height: size.height)
        let visibleCenterX = visibleRect.midX
        
        var newAttributesArray = Array<UICollectionViewLayoutAttributes>()
        
        for (_, attributes) in superAttributes!.enumerated() {
            let newAttributes = attributes.copy() as! UICollectionViewLayoutAttributes
            newAttributesArray.append(newAttributes)
            let distanceFromCenter = visibleCenterX - newAttributes.center.x
            let absDistanceFromCenter = min(abs(distanceFromCenter), self.scalingOffset)
            let scale = absDistanceFromCenter * (self.minimumScaleFactor - 1) / self.scalingOffset + 1
            newAttributes.transform3D = CATransform3DScale(CATransform3DIdentity, 1, scale, 1)
//            newAttributes.transform3D = CATransform3DScale(CATransform3DIdentity, scale, scale, 1)
        }
        
        return newAttributesArray;
    }
}
