//
//  MainDashboardViewController.swift
//  testSwift
//
//  Created by JJsobtid on 12/7/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit
import UICountingLabel

class MainDashboardViewController: UIViewController {

	var isVertical: Bool {
		switch UIDevice.current.orientation {
		case .portrait, .portraitUpsideDown:
			return true
		default:
			return false
		}
	}
    
	var navigationBottom: CGFloat {
		let navBarFrame = navigationController!.navigationBar.frame
		return topArea + navBarFrame.height
	}
    var countingLabel: UICountingLabel!
    
    var label: UILabel!
    var textField: UITextField!
    
	private var previousTextFieldContent: String?
    private var previousSelection: UITextRange?
	
//    var collectionViewLayout: PageCollectionFlowLayout!
    var dataSource: Array<String>!
    
    private var collectionView: UICollectionView!
    
    private var animationsCount = 0
    
//    private var pageWidth: CGFloat {
//        return self.collectionViewLayout.itemSize.width + self.collectionViewLayout.minimumLineSpacing
//    }
    
//    private var contentOffset: CGFloat {
//        return self.collectionView.contentOffset.x + self.collectionView.contentInset.left
//    }
    
    // MARK: Configuration
    
    private func configureDataSource() {
        self.dataSource = Array()
        for index in 1...10 {
            self.dataSource.append("Page \(index)")
        }
    }
    
    private func configureCollectionView() {
//        collectionViewLayout = PageCollectionFlowLayout()
//        collectionViewLayout.scrollDirection = .horizontal
//        collectionViewLayout.minimumLineSpacing = MARGIN
//        collectionViewLayout.itemSize = CGSize(width: 120, height: 420)
//
//        collectionView = UICollectionView(frame: CGRect(), collectionViewLayout: collectionViewLayout)
//        collectionView.decelerationRate = .fast
//        collectionView.backgroundColor = .white
//        collectionView.delegate = self
//        collectionView.dataSource = self
//        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "reuse")
    }
    
    private func scrollToPage(page: Int, animated: Bool) {
//        self.collectionView.isUserInteractionEnabled = false
//        self.animationsCount += 1
//        let pageOffset = CGFloat(page) * self.pageWidth - self.collectionView.contentInset.left
//        self.collectionView.setContentOffset(CGPoint(x: pageOffset, y: collectionView.contentOffset.y), animated: true)
    }

	override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
		super.willTransition(to: newCollection, with: coordinator)
		coordinator.animate(alongsideTransition: { (context) in
			
			print("top area: \(topArea)")
		}) { (context) in
		}
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		navigationController?.navigationBar.setBackgroundImage(UIImage(named: "bg-1"), for: .default)
	}
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        countingLabel.count(from: 0, to: 100, withDuration: 1.0)
    }
    
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()

		navigationItem.titleView = UIImageView(image: UIImage(named: "logoEds01"))
		
		view.backgroundColor = .gray
		
		navigationController?.navigationBar.prefersLargeTitles = false
		navigationItem.largeTitleDisplayMode = .never
		
		navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(closeVC))
		
		let badgeLabel = UILabel()
		badgeLabel.backgroundColor = .red
		badgeLabel.textAlignment = .center
		badgeLabel.textColor = .white
		badgeLabel.font = UIFont.systemFont(ofSize: 10)
		badgeLabel.layer.cornerRadius = 5
		badgeLabel.layer.masksToBounds = true
		badgeLabel.text = "N"
		let leadButton = UIButton()
		leadButton.setImage(UIImage(named: "bell"), for: .normal)
		leadButton.addSubview(badgeLabel)
		leadButton.addTarget(self, action: #selector(closeVC), for: .touchUpInside)
		let leadImage = UIImageView(image: UIImage(named: "bell"))
		leadImage.contentMode = .scaleAspectFit
//		leadImage.addSubview(badgeLabel)
		badgeLabel.snp.makeConstraints { (make) in
			make.size.equalTo(CGSize(width: 12, height: 12))
			make.trailing.equalTo(leadButton.snp.trailing).offset(4)
			make.top.equalTo(leadButton.snp.top).offset(-4)
		}
		navigationItem.rightBarButtonItem = UIBarButtonItem(customView: leadButton)
		
		navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .fastForward, target: self, action: #selector(showDetail))
		
        countingLabel = UICountingLabel(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        countingLabel.center = view.center
        
        self.configureDataSource()
//        self.configureCollectionView()
//        
//        view.addSubview(collectionView)
        
        view.addSubview(countingLabel)
		
        label = UILabel()
        label.testBorder(.red)
        label.textColor = UIColor(named: "Color")
        textField = UITextField()
        textField.backgroundColor = .white
		textField.textColor = .black
        textField.delegate = self
        textField.addTarget(self, action: #selector(reformatAsCardNumber), for: .editingChanged)
		
        view.addSubview(label)
        view.addSubview(textField)
        label.snp.makeConstraints { (make) in
            make.centerX.equalTo(view.snp.centerX)
            make.top.equalTo(countingLabel.snp.bottom).offset(26)
        }
//        collectionView.snp.makeConstraints { (make) in
//            make.edges.equalTo(view.snp.edges)
//        }
        textField.snp.makeConstraints { (make) in
            make.centerX.equalTo(view.snp.centerX)
            make.top.equalTo(label.snp.bottom).offset(16)
            make.height.equalTo(20)
            make.width.equalTo(150)
        }
        view.layoutIfNeeded()
//        textField
        // Do any additional setup after loading the view.
    }
	@objc func showDetail() {
		let layout = UICollectionViewFlowLayout()
		
		let vc = DetailCollectionViewController(collectionViewLayout: layout)
		navigationController?.pushViewController(vc, animated: true)
	}
	@objc func closeVC() {
		dismiss(animated: true, completion: nil)
	}
}

extension MainDashboardViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        label.text = NSLocalizedString(textField.text ?? "", bundle: Bundle(path: Bundle.main.path(forResource: "th", ofType: "lproj")!)!, comment: "")
        view.endEditing(true)
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        previousTextFieldContent = textField.text;
        previousSelection = textField.selectedTextRange;
        return true
    }

    @objc func reformatAsCardNumber(textField: UITextField) {
        var targetCursorPosition = 0
        if let startPosition = textField.selectedTextRange?.start {
            targetCursorPosition = textField.offset(from: textField.beginningOfDocument, to: startPosition)
        }

        var cardNumberWithoutSpaces = ""
        if let text = textField.text {
            cardNumberWithoutSpaces = self.removeNonDigits(string: text, andPreserveCursorPosition: &targetCursorPosition)
        }

        if cardNumberWithoutSpaces.count > 19 {
            textField.text = previousTextFieldContent
            textField.selectedTextRange = previousSelection
            return
        }

        let cardNumberWithSpaces = self.insertCreditCardSpaces(cardNumberWithoutSpaces, preserveCursorPosition: &targetCursorPosition)
        textField.text = cardNumberWithSpaces

        if let targetPosition = textField.position(from: textField.beginningOfDocument, offset: targetCursorPosition) {
            textField.selectedTextRange = textField.textRange(from: targetPosition, to: targetPosition)
        }
    }

    func removeNonDigits(string: String, andPreserveCursorPosition cursorPosition: inout Int) -> String {
        var digitsOnlyString = ""
        let originalCursorPosition = cursorPosition

        for i in Swift.stride(from: 0, to: string.count, by: 1) {
            let characterToAdd = string[string.index(string.startIndex, offsetBy: i)]
            if characterToAdd >= "0" && characterToAdd <= "9" {
                digitsOnlyString.append(characterToAdd)
            }
            else if i < originalCursorPosition {
                cursorPosition -= 1
            }
        }

        return digitsOnlyString
    }

    func insertCreditCardSpaces(_ string: String, preserveCursorPosition cursorPosition: inout Int) -> String {
        // Mapping of card prefix to pattern is taken from
        // https://baymard.com/checkout-usability/credit-card-patterns

        // UATP cards have 4-5-6 (XXXX-XXXXX-XXXXXX) format
        let is456 = string.hasPrefix("1")

        // These prefixes reliably indicate either a 4-6-5 or 4-6-4 card. We treat all these
        // as 4-6-5-4 to err on the side of always letting the user type more digits.
        let is465 = [
            // Amex
            "34", "37",

            // Diners Club
            "300", "301", "302", "303", "304", "305", "309", "36", "38", "39"
        ].contains { string.hasPrefix($0) }

        // In all other cases, assume 4-4-4-4-3.
        // This won't always be correct; for instance, Maestro has 4-4-5 cards according
        // to https://baymard.com/checkout-usability/credit-card-patterns, but I don't
        // know what prefixes identify particular formats.
        let is4444 = !(is456 || is465)

        var stringWithAddedSpaces = ""
        let cursorPositionInSpacelessString = cursorPosition

        for i in 0..<string.count {
            let needs465Spacing = (is465 && (i == 4 || i == 10 || i == 15))
            let needs456Spacing = (is456 && (i == 4 || i == 9 || i == 15))
            let needs4444Spacing = (is4444 && i > 0 && (i % 4) == 0)

            if needs465Spacing || needs456Spacing || needs4444Spacing {
                stringWithAddedSpaces.append(" ")

                if i < cursorPositionInSpacelessString {
                    cursorPosition += 1
                }
            }

            let characterToAdd = string[string.index(string.startIndex, offsetBy:i)]
            stringWithAddedSpaces.append(characterToAdd)
        }

        return stringWithAddedSpaces
    }
}

extension MainDashboardViewController: UICollectionViewDataSource, UICollectionViewDelegate      {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "reuse", for: indexPath)
        collectionViewCell.contentView.backgroundColor = .gray
//        collectionViewCell.testBorder(.red)
//        collectionViewCell.tex.text = self.dataSource[indexPath.row]
        return collectionViewCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.isDragging || collectionView.isDecelerating || collectionView.isTracking {
            return
        }
        
        let selectedPage = indexPath.row
        self.scrollToPage(page: selectedPage, animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        self.pageControl.currentPage = Int(self.contentOffset / self.pageWidth)
//
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
//        self.animationsCount-=1
//        if self.animationsCount == 0 {
            self.collectionView.isUserInteractionEnabled = true
//        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        for cell in self.collectionView.visibleCells {
            let indexPath = self.collectionView.indexPath(for: cell)!
            let attributes =   self.collectionView.layoutAttributesForItem(at: indexPath)!
            let cellRect = attributes.frame
            let cellFrameInSuperview = self.collectionView.convert(cellRect,  to: self.collectionView.superview)
            let centerOffset = cellFrameInSuperview.origin.x -      self.collectionView.contentInset.left
            
            let alpha = abs(centerOffset/collectionView.center.x)
            var centerOff = abs(cellFrameInSuperview.midX - collectionView.center.x)/collectionView.center.x
            if centerOff > 1 {
                centerOff = 1
            }
            print(1-centerOff)
            let color = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1-centerOff)
            cell.contentView.backgroundColor = color
//            cell.updateWithOffset(centerOffset)
        }
    }
}
