//
//  CustomNavigationController.swift
//  testSwift
//
//  Created by JJsobtid on 12/7/2562 BE.
//  Copyright © 2562 JJsobtid. All rights reserved.
//

import UIKit

class CustomNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
		
		navigationItem.title = "Transparent Nav Bar"
		
//		navigationBar.barTintColor = UIColor.clear
		navigationBar.setBackgroundImage(UIImage(named: "bg-1"), for: .default)
		navigationBar.shadowImage = UIImage()
		navigationBar.isTranslucent = true
		navigationBar.tintColor = .white
		navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
		view.backgroundColor = .clear
        // Do any additional setup after loading the view.
    }
	

}
