//
//  CustomBannerCollectionView.swift
//  testSwift
//
//  Created by JJsobtid on 27/4/2561 BE.
//  Copyright © 2561 JJsobtid. All rights reserved.
//

import UIKit
import SDWebImage

@objc protocol BannerCollectionDelegate {
	@objc optional func bannerCollection(_ collection: CustomBannerCollectionView, didMoveToPage page: Int)
	@objc optional func bannerCollection(_ collection: CustomBannerCollectionView, didTouchPage page: Int)
	@objc optional func bannerCollection(_ collection: CustomBannerCollectionView, didDownloadImages image: UIImage?, atPage page: Int)
}
class CustomBannerCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

	var pageDelegate: BannerCollectionDelegate?

	var timer: Timer!
	var currentPage: Int!
	
	var imageURLs: [String]! {
		didSet {
			if imageURLs.count > 0 {
				imageURLs = [imageURLs.last!] + imageURLs + [imageURLs.first!]
			}
			reloadData()
			startMovingIfNeed()
			needRefresh = true
			
			isScrollEnabled = imageURLs.count > 1
		}
	}
	
	var imageViewMode: UIView.ContentMode!
	var moveTimeInterval: TimeInterval! {
		didSet {
			startMovingIfNeed()
		}
	}
	var autoMoving: Bool!
	var movingOnTracking: Bool!
	var needRefresh: Bool!

	override var frame: CGRect {
		didSet {
			
		}
	}
	
	override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
		super.init(frame: frame, collectionViewLayout: layout)
		backgroundColor = .clear
		setUp()
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
		
		setUp()
	}
	
	override func layoutSubviews() {
		if needRefresh && imageURLs.count > 1 {
			let item = 1
			
			scrollToItem(at: IndexPath(item: item, section: 0), at: .left, animated: false)
			needRefresh = false
		}
		
		super.layoutSubviews()
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	func startMoving() {
		startMovingIfNeed()
	}
	func stopMoving() {
		removeTimer()
	}
	
	override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
		super.traitCollectionDidChange(previousTraitCollection)
		needRefresh = true
		reloadData()
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return max(imageURLs.count, 1)
	}
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: SCREEN_WIDTH(), height: 100)
	}
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BannerCollectionCell.cellIdentifier, for: indexPath) as! BannerCollectionCell
		
		cell.contentMode = imageViewMode
		cell.backgroundColor = .red
		if imageURLs.count == 0 {
			cell.bannerImageView.image = UIImage()
		}
		
		cell.setImage(imageURLs[indexPath.item])
		
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		pageDelegate?.bannerCollection?(self, didTouchPage: indexPath.item)
	}
	
	fileprivate func jumpToLastImage() {
		let lastItem = imageURLs.count - 2
		if lastItem < 0 {
			return
		}
		let indexPath = IndexPath(item: lastItem, section: 0)
		scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
	}
	fileprivate func jumpToFirstImage() {
		if imageURLs.count < 2 {
			return
		}
		
		let indexPath = IndexPath(item: 1, section: 0)
		scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
	}
	fileprivate func jumpWithContentOffset(offset: CGPoint) {
		if offset.x <= 0 {
			jumpToLastImage()
		}
		if offset.x >= CGFloat(imageURLs.count - 1) * frame.width {
			jumpToFirstImage()
		}
	}
	
	func adjustCurrentPageWithContentOffset(offset: CGPoint) {
		let adjustPoint = CGPoint(x: offset.x + (0.5 * frame.width), y: offset.y)
		let indexPath = indexPathForItem(at: adjustPoint)
		let currentPage = pageWithIndexPath(indexPath: indexPath!)
		
		if self.currentPage == currentPage {
			return
		}
		
		self.currentPage = currentPage
		tellDelegateCurrentPage()
	}
	
	fileprivate func pageWithIndexPath(indexPath: IndexPath) -> Int {
		var page: Int
		let index = indexPath.item
		let suffixIndex = imageURLs.count - 1
		let prefixIndex = 0
		
		let firstPage = 0
		let lastPage = suffixIndex - 2
		
		if index == prefixIndex {
			page = lastPage
		} else if index == suffixIndex {
			page = firstPage
		} else {
			page = index - 1
		}
		return page
	}

	fileprivate func setUp() {
		imageViewMode = .scaleAspectFill
		autoMoving = true
		imageURLs = []
		moveTimeInterval = 3.0
		movingOnTracking = true
		
		needRefresh = false
		delegate = self
		dataSource = self
		isPagingEnabled = true
		showsHorizontalScrollIndicator = false
		
		if collectionViewLayout is UICollectionViewFlowLayout {
			let layout = collectionViewLayout as! UICollectionViewFlowLayout
			layout.scrollDirection = .horizontal
			layout.minimumLineSpacing = 0
			layout.minimumInteritemSpacing = 0
		}
		
		register(BannerCollectionCell.self, forCellWithReuseIdentifier: BannerCollectionCell.cellIdentifier)
		registerNotification()
	}
	
	func registerNotification() {
		NotificationCenter.default.addObserver(self, selector: #selector(applicationWillResignAction), name:  UIApplication.willResignActiveNotification, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeAction), name:  UIApplication.didBecomeActiveNotification, object: nil)
	}
	func addTimer() {
		removeTimer()
		let speed = moveTimeInterval < 0.5 ? 3 : moveTimeInterval
		timer = Timer(timeInterval: speed!, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
		timer.tolerance = 0.1*speed!
		if movingOnTracking {
			RunLoop.current.add(timer, forMode: .common)
		}
		
	}
	func removeTimer() {
		if timer != nil {
			timer.invalidate()
			timer = nil
		}
	}
	@objc func moveToNextPage() {
		
		if imageURLs.count > 1 {
			let currentIndexPath = indexPathForItem(at: contentOffset)
			guard let indexPath = currentIndexPath else {
				return
			}
			let item = indexPath.item + 1
			let nextIndexPath = IndexPath(item: item, section: currentIndexPath!.section)
			scrollToItem(at: nextIndexPath, at: .centeredHorizontally, animated: true)
		}
	}
	func startMovingIfNeed() {
		
		if autoMoving && imageURLs.count > 3 {
			addTimer()
		}
	}
	
	// MARK: - UIScrollDelegate
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		adjustCurrentPageWithContentOffset(offset: scrollView.contentOffset)
		jumpWithContentOffset(offset: scrollView.contentOffset)
	}
	func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
		removeTimer()
	}
	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		startMovingIfNeed()
	}
	
	func tellDelegateCurrentPage() {
		pageDelegate?.bannerCollection?(self, didMoveToPage: currentPage)
	}
	// MARK: - Notification
	@objc func applicationWillResignAction() {
		
	}
	@objc func applicationDidBecomeAction() {
		startMovingIfNeed()
	}
	
	class BannerCollectionCell: UICollectionViewCell {
		
		static let cellIdentifier = "BannerCollectionCell"
		
		var bannerImageView: UIImageView!
		
		override init(frame: CGRect) {
			super.init(frame: frame)
			
			bannerImageView = UIImageView()
			
			contentView.addSubview(bannerImageView)
			bannerImageView.snp.makeConstraints { (make) in
				make.edges.equalTo(contentView.snp.edges)
			}
			contentView.snp.makeConstraints { (make) in
				make.edges.equalTo(snp.edges)
			}
		}
		
		required init?(coder aDecoder: NSCoder) {
			fatalError("init(coder:) has not been implemented")
		}
		
		func setImage(_ url: String) {
			bannerImageView.sd_setImage(with: URL(string: url), placeholderImage: nil, options: .fromCacheOnly) { (image, err, cache, url) in
				
			}
		}
	}
}
