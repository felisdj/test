//
//  ChartViewController.swift
//  testSwift
//
//  Created by JJsobtid on 22/2/2561 BE.
//  Copyright © 2561 JJsobtid. All rights reserved.
//

import UIKit
import Charts
import Spruce

class ChartViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
	
	var chartView: LineChartView!
	var chartData = [ChartDataEntry]()
	var ranNumButton = UIButton()
	
	var testCollectionView: UICollectionView!
	var someString: String = "test"
	var customVC: CustomViewController!
	
	let animations: [StockAnimation] = [.slide(.left, .severely), .fadeIn]
	var sortFunction: SortFunction? = LinearSortFunction(direction: .topToBottom, interObjectDelay: 0.05)
	var animationView: UIView?
	var timer: Timer?
	
	override var prefersStatusBarHidden: Bool {
		return true
	}
	
	override func viewWillAppear(_ animated: Bool) {
		navigationController?.isNavigationBarHidden = true
	}
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		guard var vc = customVC else {
			return
		}
		customVC = nil
	}
    override func viewDidLoad() {
        super.viewDidLoad()
		
		prepareAnimation()
		
		ranNumButton.setTitle("Random Number", for: .normal)
		ranNumButton.addTarget(self, action: #selector(addRandomNum), for: .touchUpInside)
		ranNumButton.backgroundColor = .brandcolor2
		
		view.addSubview(ranNumButton)
		ranNumButton.snp.makeConstraints { (make) in
			make.leading.equalTo(view.snp.leading)
			make.trailing.equalTo(view.snp.trailing)
			make.top.equalTo(view.snp.top)
			make.bottom.equalTo(view.snp.top).offset(44)
		}
		
		chartView = LineChartView()
		chartView.backgroundColor = .white
		chartView.doubleTapToZoomEnabled = false
		chartView.xAxis.labelPosition = .bottom
		chartView.xAxis.drawGridLinesEnabled = false
		chartView.rightAxis.enabled = false
		chartView.highlighter = nil
		chartView.scaleYEnabled = false
		chartView.legend.enabled = false
		
		chartView.leftAxis.axisMinimum = 0
		chartView.leftAxis.axisMaximum = 100
	
		view.addSubview(chartView)
		chartView.snp.makeConstraints { (make) in
			make.leading.equalTo(view.snp.leading)
			make.trailing.equalTo(view.snp.trailing)
			make.top.equalTo(view.snp.top).offset(44)
			make.bottom.equalTo(view.snp.bottom).offset(-49)
		}
		
		let collectionLayout = UICollectionViewFlowLayout()
		
		testCollectionView = UICollectionView(frame: CGRect(), collectionViewLayout: collectionLayout)

		animationView = testCollectionView

		testCollectionView.backgroundColor = .clear
		testCollectionView.delegate = self
		testCollectionView.dataSource = self
		testCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
		testCollectionView.register(HeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "header")
		testCollectionView.alwaysBounceVertical = true
		
		
		view.addSubview(testCollectionView)
		
		testCollectionView.snp.makeConstraints { (make) in
			make.top.equalTo(view.snp.top).offset(64)
			make.leading.equalTo(view.snp.leading)
			make.bottom.equalTo(view.snp.bottom)
			make.trailing.equalTo(view.snp.trailing)
		}
		view.layoutIfNeeded()
        // Do any additional setup after loading the view.
    }
	
	func prepareAnimation() {
		animationView?.spruce.prepare(with: animations)
		
		timer?.invalidate()
		timer = Timer.scheduledTimer(timeInterval: 0, target: self, selector: #selector(callAnimation), userInfo: nil, repeats: false)
	}
	
	@objc func callAnimation() {
		guard let sortFunction = sortFunction else {
			return
		}
		let animation = SpringAnimation(duration: 1.0)
		DispatchQueue.main.async {
			self.animationView?.spruce.animate(self.animations, animationType: animation, sortFunction: sortFunction)
		}
	}
	
	// MARK: - Collection
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		switch indexPath.section {
			case 1:
				return CGSize(width: (collectionView.frame.width - 2*16), height: 140)
			case 2:
				return CGSize(width: (collectionView.frame.width - 2*16)/2, height: 70)
			default:
				return CGSize(width: (collectionView.frame.width - 2*16)/2, height: 140)
		}

	}
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
		return CGSize(width: collectionView.frame.width - 2*16, height: 60)
	}
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 4
	}
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		switch section {
		case 0:
			return 0
		case 1:
			return 1
		case 2:
			return 1
		default:
			return 2
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)

		cell.backgroundView = UIView(frame: cell.frame)
		cell.backgroundView?.backgroundColor = .green
		cell.backgroundView?.addSubview(UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10)))
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		print(indexPath)
		
//		let vc = StepOneViewController()
//		vc.navigationController?.hero.isEnabled = true
		customVC = CustomViewController(withsome: someString)
		customVC.id = "TestNav\(indexPath)"
		customVC.isPresent = false
		customVC.navigationController?.hero.isEnabled = true

		if let cell = collectionView.cellForItem(at: indexPath) {
			cell.backgroundView!.hero.id = "TestNav\(indexPath)"
		}
		navigationController?.hero.isEnabled = true
		navigationController?.pushViewController(customVC, animated: true)
//		present(vc, animated: true, completion: nil)
	}
	
	func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
		
		switch kind {
		case UICollectionView.elementKindSectionHeader:
			let reusableview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "header", for: indexPath) as! HeaderView
			
			reusableview.setTitle("hhh")
			
			return reusableview
		default:
			fatalError("Unexpected element kind")
		}
	}
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
	@objc func addRandomNum() {
		let ran = Double(arc4random() % 100)
		chartData.append(ChartDataEntry(x: Double(chartData.count), y: ran))
		
		let line = LineChartDataSet(entries: chartData, label: nil)
		line.colors = [NSUIColor.brandcolor1]
		line.mode = .cubicBezier
		line.circleHoleRadius = 0.5
		line.lineDashPhase = 0.5
		line.circleRadius = 0.5
		
		let data = LineChartData()
		data.addDataSet(line)
		
		chartView.data = data
		chartView.chartDescription?.text = "test line chart"
		
	}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class HeaderView: UICollectionReusableView {
	
	var titleLabel: UILabel!
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		titleLabel = UILabel()
		titleLabel.font = UIFont.systemFont(ofSize: 14)
		titleLabel.textColor = .black
		titleLabel.textAlignment = .center
		titleLabel.backgroundColor = .popoverbg
		
		addSubview(titleLabel)
		titleLabel.snp.makeConstraints { (make) in
			make.centerX.equalTo(snp.centerX)
			make.centerY.equalTo(snp.centerY).offset(2)
			make.height.equalTo(30)
			make.width.equalTo(150)
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func setTitle(_ title: String) {
		titleLabel.text = title
	}
}
/*
if([self.questionType isEqualToString:@"FILLIN"]) {
	self.numberAnswers = @[[[UIOneNumberTextField alloc] initWithFrame:CGRectMake(0, 0, 40, 40)],
	[[UIOneNumberTextField alloc] initWithFrame:CGRectMake(0, 0, 40, 40)],
	[[UIOneNumberTextField alloc] initWithFrame:CGRectMake(0, 0, 40, 40)],
	[[UIOneNumberTextField alloc] initWithFrame:CGRectMake(0, 0, 40, 40)],
	[[UIOneNumberTextField alloc] initWithFrame:CGRectMake(0, 0, 40, 40)],
	[[UIOneNumberTextField alloc] initWithFrame:CGRectMake(0, 0, 40, 40)],
	[[UIOneNumberTextField alloc] initWithFrame:CGRectMake(0, 0, 40, 40)]];
	UITextField *beforeField;
	CGFloat space = (self.frame.size.width - (7*40))/8;
	NSMutableDictionary *answer = [[NSMutableDictionary alloc] initWithDictionary:[self.parameter valueForKey:@"answer_id"]];
	for(int i = 0 ; i < 7 ; i++) {
		UIOneNumberTextField *numberField = self.numberAnswers[i];
		numberField.delegate = self;
		numberField.text = nil;
		numberField.tag = i;
		numberField.backgroundColor = UIColor.whiteColor;
		numberField.layer.borderColor = [Color.guestcolor CGColor];
		numberField.layer.borderWidth = 1.0;
		numberField.textColor = Color.brandcolor1;
		numberField.font = [Text arun32];
		[numberField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
		numberField.textAlignment = NSTextAlignmentCenter;
		numberField.keyboardType = UIKeyboardTypeNumberPad;
		[self addSubview:numberField];
		if(i == 0) {
			[numberField customLayoutosuperView:HorizontalAlignmentLeft VerticalAlignment:VerticalAlignmentMiddle offsetX:space offsetY:0];
		} else {
			[numberField relativeLayoutcenterwith:beforeField position:RelativeAlignmentRight margin:space];
		}
		if(i == 4) {
			numberField.text = @".";
			numberField.font = [Text arun:52];
			numberField.backgroundColor = Color.guestcolor;
			numberField.textColor = UIColor.whiteColor;
			[numberField setUserInteractionEnabled:NO];
		} else {
			//				NSString *answer_id = @"4132.02";
			//				numberField.text = [answer_id substringWithRange:NSMakeRange(i, 1)];
			//				numberField.layer.borderColor = [Color.brandcolor1 CGColor];
		}
		if ([answer valueForKey:[NSString stringWithFormat:@"%d", i]]) {
			numberField.text = [answer valueForKey:[NSString stringWithFormat:@"%d", i]];
			numberField.layer.borderColor = [Color.brandcolor1 CGColor];
		}
		beforeField = numberField;
}

// MARK: - UITextFieldDelegate
-(void)textFieldDidBeginEditing:(UITextField *)textField {

}

-(void)textFieldDidChange:(UITextField *)textField {
NSString *tempNum = textField.text;
if([textField.text length] >= 1) {
tempNum = [tempNum substringFromIndex:[tempNum length] - 1];
textField.text = [(UIOneNumberTextField *)textField replacingThaiNumber:tempNum];
textField.layer.borderColor = [Color.brandcolor1 CGColor];
textField.textColor = Color.brandcolor1;
if(![Validation numericOnly:tempNum]) {
textField.layer.borderColor = [Color.errorRed CGColor];
textField.textColor = Color.errorRed;
} else {
[self.delegate answerFillNumber:textField.tag value:textField.text];
UITextField *nextNum;
if(textField.tag != 3 && textField.tag != 6) {
nextNum = [self.numberAnswers objectAtIndex:textField.tag + 1];
[nextNum becomeFirstResponder];
} else if(textField.tag == 3) {
nextNum = [self.numberAnswers objectAtIndex:textField.tag + 2];
[nextNum becomeFirstResponder];
} else if(textField.tag == 6) {
[self endEditing:YES];
}
}
} else {
textField.layer.borderColor = [Color.errorRed CGColor];
}
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
if(range.location != 0 && range.length == 1) {
return NO;
}
return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
if([textField.text isEqualToString:@""] || [textField.text isKindOfClass:[NSNull class]]) {
textField.layer.borderColor = [Color.errorRed CGColor];
}
}

*/
